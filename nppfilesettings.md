---
layout: default
title: NppFileSettings
description: NppFileSettings tries to recognize VIM modelines and adjust settings for that file accordingly
---

# NppFileSettings plug-in for Notepad++


## Introduction

NppFileSettings is a plug-in for [Notepad++](https://notepad-plus-plus.org/) that tries to recognize VIM modelines and adjust settings for that file accordingly.
It can be very useful for people who work on cross platform projects.


## Download

To download version 2.0.0 (2022-10-21), go to [download page on GitHub](https://github.com/ffes/nppfilesettings/releases/tag/v2.0.0).


## How to install

The easiest way to install is to download it through the `Plugins Admin` in Notepad++.

To install manually, copy `NppFileSettings.dll` to the `plugins/NppFileSettings` directory and (re)start Notepad++.


## Currently supported

Support for [VIM modelines](http://vim.wikia.com/wiki/Modeline_magic) to override default settings

- `tabstop`, `ts`: set the tab width
- `expandtab`, `et`: tab key produces spaces
- `noexpandtab`, `noet`: tab key produces tabs
- `filetype`, `ft`, `syntax`, `syn`: specify the syntax highlighting used for the file


## Known issues

- This plug-in can cause unexpected results if you use it together with the [EditorConfig](https://editorconfig.org/) plug-in. When both plug-ins are installed and a `.editorconfig` file exists and sets tabs and a VIM modeline with tab settings is opened, it depends on the order the are found in the Plugins menu of Notepad++ which plug-in does its thing first. To fix this these two plugins need to become aware of each other most likely with the message `NPPM_MSGTOPLUGIN`.
- Every time a file is activated (like when switching tabs), the plug-in will do its thing, not just when the file is opened. When a file is saved the plug-in is not activated (yet).


## License

This plugin is released under the [GPL2-license](http://www.gnu.org/licenses/gpl-2.0.html).
The source code can be found at [GitHub](https://github.com/ffes/nppfilesettings).


## Release history

**Version 2.0.0** (2022-10-21)

- Removed first line recognition, because that is in Notepad++ itself and renamed therefore the plugin to NppFileSettings
- Add 64-bit build
- Add support for vim options `filetype` and `syntax`
- Internal rewrite a lot of code

**Version 1.0.0** (2015-09-23)

- Initial release as NppFileMagic
