---
title: Vim, not as hard as it seems
name: Getting started
layout: tutorial
lang: en
---

We need a file we can safely edit without breaking anything important, so we create one by typing at the command line:

```sh
ls -l /bin > learning-vim.txt
```

Now we can edit this file by typing:

```sh
vi learning-vim.txt
```

Although you typed `vi`, you will start `vim`.
If for some reason it doesn't work, you might want to add an `alias`.


## Moving around a file

Vim always starts in normal or command mode.
In this mode you can easily move around in a file with the normal cursor keys, including the `Home`/`End` and `PgUp`/`PgDn` keys.
The `Del` key also works as expected.
Other keys to move around the file are `Ctrl+D` and `Ctrl+U`.
Have you noticed the difference between `PgUp` and `Ctrl+U`?
Know that `Ctrl+F` is the same as `PgUp` and `Ctrl+B` as `PgDn`.
What do think the `U`, `D`, `F` and `B` stand for?
Many keystrokes and commands are mnemonic and this makes them easier to remember.


## Basic editing

Move the cursor to a position halfway any line of your choice.
Now press the `x` and see what happens.
After that press `X` and see the difference.
Note that the first one is a **lowercase x** and the second one is an **uppercase X**.
Press the `Del`-key as well and see that `Del` does the same as the lowercase `x`.

Now we can start to enter text.
To enter text we need to go into the insert mode.
There are many ways to enter the insert mode, but let's start with the two basic ones.
Press `i`, type some text and leave the insert mode by pressing `Esc`.
You have seen that pressing the `i` enters the insert mode at the position where the cursor is.

When back in normal mode move the cursor somewhere else and press `o`, type some text and again leave the insert mode by pressing `Esc`.
As you must have noticed, a new line under the cursor was opened to type your text.

Have you tried to use the cursor keys while you were in insert mode?
Did you know that `Ctrl+Left` and `Ctrl+Right` often works as well?


## Saving the file and closing vim

To do operations like saving the file and closing vim, you have to be in normal mode.
If you are not sure, you can safely press `Esc` to make sure you are in normal mode.

To save the current file and stay in vim, type `:w`.
To close without saving the file, type `:q!`.
To save the current file and leave vim, type `:x`. You can also use `ZZ`, or type `:wq`.
The result of these three commands is the same.
Note that there is no `Are you sure?` question, so be careful.


## More edit commands

Now that you know the basics of editing with vim, let's look at some other edit commands.

Deleting text with the `x` command works fine for a few characters, but in a lot of situations it is not efficient.
Luckily the `d` command exists and it always works with 2 characters: `dd` will delete the current line and `dw` will delete the current word.

Similar to the `d` command is the `c` command, but after deleting the stuff it will put vim in edit mode and thus act as a replace or change command.
So with `cc` you can change the current line and with `cw` you change the current word.
Know that you have to press `Esc` to leave the edit mode and you can change one word in a complete block of text if you like.

As mentioned before, vim has some very powerful options.
Almost any command in vim can be prefixed with a number.
You just learned the `cw` command.
To change the next 3 words you can use `3cw`.
Now see what happens when you type `5dd`.
