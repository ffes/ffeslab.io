---
title: Vim, not as hard as it seems
name: Introduction
layout: tutorial
lang: en
---

There are many different text-editors for Linux.
One of them is [vi](https://en.wikipedia.org/wiki/Vi) (pronounce as vee-eye) which is found on all major distributions.

The first version of vi is written in 1976 by [Bill Joy](https://en.wikipedia.org/wiki/Bill_Joy), who later became one of the co-founders of [Sun Microsystems](https://en.wikipedia.org/wiki/Sun_Microsystems).
Since then a lot of changed, but vi is still around and the basic concept is still the same.
Vi is a bit different from other editors.
It has two mode, an insert and an edit or normal mode.
It is important to know in which mode you are.
Because of those modes vi has a learning curve, but once you understand the commands, it proves itself as a very powerful editor.

Learning the many commands vi has takes a bit of getting used to and you just have to do it.

The original vi (from the 1970s) is not used anymore, but several reimplementation of vi exist.
The most widely used is [vim](https://www.vim.org/).
This tutorial will focus on the basics that should be the same for all of them.
