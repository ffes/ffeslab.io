---
layout: default
title: RebootAt
description: RebootAt is a tool reboot your Windows machine at a given time
---

# RebootAt

## Introduction

RebootAt is a command line tool to reboot your Windows machine at a given time.


### Usage

The tool takes just one argument, a timestamp when the computer needs to reboot.

The word `now` (case insensitive) means just that, reboot immediately.

```sh
# Reboot immediately
RebootAt now
```

A timestamp means to reboot at the first time this will occur.
The timestamp has to be in 24-hours notation.

```sh
# Reboot today at 23:30
RebootAt 23:30
```

```sh
# Reboot tomorrow at 2:00
RebootAt 2:00
```

An argument starting with `+` means the number following is in minutes.

```sh
# Reboot in 60 minutes
RebootAt +60
```

## Download

At the moment the command line parsing is not robust enough to call the tool production ready.
Therefore there is no official release yet.

Since I know these limitations, I use it on our production servers.

The sources can be found at [GitHub](https://github.com/ffes/rebootat).

If you want to use it at this moment you need to build it yourself.
For the project I use [Visual Studio](https://visualstudio.microsoft.com/), so you need that to build it.
