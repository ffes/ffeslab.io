---
title: Genealogie van de familie Fesevur
layout: default
lang: nl
---

# Genealogie van de familie Fesevur

Op deze pagina's vind je informatie over [mijn zoektocht]({% link gen/werkwijze.md %}) naar de stamboom van de familie Fesevur.

There is also a [short English summary]({% link gen/summary.md %}) about the genealogy of the Fesevur Family.

De oudste Fesevur die tot nu toe is gevonden is Jan Fesevur, getrouwd met Anna Catharina Raaijmakers. Zij kregen in 1792 een zoon Jan Mathias Fesevur. Hij trouwde met Anne Marie Catharina Vroemen. Uit dit huwelijk werden drie kinderen geboren: Jean Philippe, Henri Jean en Maria Anna Catharina Sibilla. Een groot deel van de mij bekende Fesevuren zijn afstammelingen van deze drie kinderen.

Naast deze afstammelingen is er ook wat ik voor het gemak de Amsterdamse tak ben gaan noemen. Deze leefden tussen 1850 en 1900 in Delft en zijn daarna naar Amsterdam vertrokken. Dit zijn zeker drie verschillende gezinnen en van een deel van hen heb ik nog geen relatie kunnen leggen tussen hen en de eerder genoemde Jan Fesevur.

Ook is er een gezin dat enige jaren in het toenmalige Nederlands-Indië heeft gewoond. Ook van hen is de relatie met de rest van de stamboom mij nog onbekend.

## Betekenis van de naam Fesevur

Er binnen de familie gaan meerdere verhalen, maar de meeste logisch verklaring volgens mij is de volgende. 'Fese' schijnt oude Duits te zijn voor 'turf' of 'plaggen' en 'vur' is gewoon 'vuur'. Dus zou de betekenis turfstokers kunnen zijn.

Andere verhalen binnen de familie zijn dat de naam afkomstig is uit Scandinavië. Binnen een andere tak van de familie ging het verhaal dat de naam uit Frankrijk komt, aangezien er veel Franse voornamen zijn. Met name Jean Philippe was erg populair. Ook hoor ik nog wel eens de vraag of de naam uit Portugal komt, misschien wel als [Sefardische Joden](https://nl.wikipedia.org/wiki/Sefardische_Joden). Voor al deze verhalen heb ik tot nu toe geen concrete aanwijzingen gevonden.

De Franse voornamen laten zich goed verklaren. Jan Mathias heeft in het leger gezeten en heeft daarom veel in het Franstalige deel van wat nu België is vertoefd. Hierdoor zijn waarschijnlijk de Franse voornamen in de familie terecht gekomen.

## Verschillende spelling

In officiële stukken zijn er twee voorkomende spellingen: Fesevur en Fesevûr. Waar het ^ vandaan komt is mij op dit ogenblik nog onduidelijk, maar in diverse archiefstukken van rond 1900 kom ik dit al tegen. Mogelijk is het om de uitspraak van "uh" of "uu" duidelijk te maken. Ook kan het zijn dat men het met de ^ er chiquer uit vond zien.

## Verspreiding over de wereld

Het overgrote deel van de mensen die ik heb gevonden wonen in Nederland. Een klein aantal in België. Ook wonen er een aantal in de Verenigde Staten. Ze emigreerden in de 60-er jaren. Een tijdje geleden, kreeg ik een e-mail van iemand uit Nieuw-Zeeland, maar dit is iemand die niet zo lang geleden vanuit Nederland is geëmigreerd.

## Stamboom online

Ik heb nog geen stamboom online staan. Ik twijfel nog over het plaatsen op een site als [MyHeritage](https://www.myheritage.nl/) of om de huidige informatie rechtstreeks op deze site te plaatsen. Maar mocht je interesse hebben in een [GEDCOM bestand](https://nl.wikipedia.org/wiki/GEDCOM), neem dan [contact met me op]({% link about.html %}).
