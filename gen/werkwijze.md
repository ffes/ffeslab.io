---
title: Genealogie van de Familie Fesevur
layout: default
lang: nl
---

# Werkwijze

Hoe ben ik tot deze stamboom gekomen? Eigenlijk is dat een lang verhaal.

Regelmatig krijg ik de vraag: "Fesevur, wat een aparte naam. Waar komt die vandaan?". En ik moest dan altijd zeggen: "Ik weet het niet". Zo ging in de familie het verhaal dat de naam uit Scandinavië zou komen, maar enig bewijs (of andere concrete aanwijzingen) heb ik hiervoor nooit gezien. Toch intrigeerde deze vraag mij al geruime tijd.

Ook hoorde ik - vooral vroeger - van voornamelijk Hagenaars regelmatig de vraag: "Ben je familie van de oogarts of internist". Ook op deze vraag moest ik altijd antwoorden: "Ik weet het niet".

In het voorjaar van 2003 kreeg ik van mijn schoonmoeder een rouwadvertentie uit de Haagsche Courant van een Fesevur. Op dat ogenblik zei het mij nog niets, maar wel realiseerde ik me dat als ik informatie over de familie Fesevur wilde, ik niet te lang moest wachten. Mijn vader (1927-1985) was toen inmiddels 76 geweest, dus de rest van de familie werd er niet jonger op. Ik ben gaan rondvragen in de familie en mijn tante verwees me naar een neef van haar. Hij zou al redelijk wat onderzoek gedaan hebben.

Ik heb toen contact met hem opgenomen en inderdaad kon hij mij het een en ander vertellen. Hij was vanaf zijn opa (mijn overgrootvader) naar boven gaan speuren en was uiteindelijk bij Jan Fesevur terecht gekomen. Ook was hij diverse andere Fesevuren tegengekomen, maar had die tak verder nooit onderzocht. Hij had al jaren niets meer aan dit onderzoek gedaan, maar was nog steeds nieuwsgierig. Daarom hebben we afgesproken dat ik zou proberen contact op te nemen met de Fesevuren die op dat ogenblik in het telefoonboek te vinden waren.

Ik ben gaan bellen en heb met diverse mensen uitgebreid gesproken. Zo kwam ik in contact met Guus Kool. Hij is getrouwd met een Fesevur en had wat onderzoek gedaan in Amsterdam. Ook heb ik gesproken met Jan Fesevur en hij is een zoon van de artsen die jarenlang in Den Haag werkzaam zijn geweest.

Wat ik tot nu toe heb gedaan is alles wat anderen al gevonden hadden en wat ik zelf op internet heb kunnen vinden gecombineerd tot een stamboom. Hiervoor maak ik gebruik van het programma [Aldfaer](http://www.aldfaer.net/). Zelf ben ik nog nooit in een archief ben geweest, maar dat gaat er zeker een keer van komen. Er zijn nog genoeg punten waar verder onderzoek noodzakelijk is.

Ik heb ervoor gekozen om te proberen een zo compleet beeld te krijgen van iedereen die naam Fesevur heeft. Als je genealogisch interessante informatie hebt over een Fesevur die hier nog niet vermeld is, [laat het dan weten]({% link about.html %}).
