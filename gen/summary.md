---
title: Genealogy of the Fesevur Family
layout: default
---

# Genealogy of the Fesevur Family

The oldest Fesevur that has been found so far is Jan Fesevur, married to Anna Catharina Raaijmakers. In 1792 they got a son Jan Mathias Fesevur. He was married to Anne Marie Catharina Vroemen. From this marriage three children were born: Jean Philippe, Henri Jean and Maria Anna Catharina Sibilla. A large part of the Fesevurs known to me are descendants of this three children.

Apart from them there is also a family that lived in Delft between 1850 and 1900. After that they moved to Amsterdam. This are three different families en with a part of them I have not been able to connect them to the Jan Fesevur mentioned above.

There is also a family that lived in the then Dutch Indies, now Indonesia. I haven't been able to related them to the family tree as well.

## The meaning of the name Fesevur

In the family go several story about the meaning of name. The most logical explanation, according to me, is this one. 'Fese' seems to be old German for 'peat' or 'sod' and 'vur' is from 'fire'. So the meaning could be peat burners.

Other stories are that the name comes from Scandinavia. There also is the story that the name comes from France, since there are a lot of French first names. Especially Jean Philippe was very popular. For both stories I couldn't find any concrete indications.

The French first names are very explicable. Jan Mathias has served in the army and therefore spend a lot of time in the French speaking part of what now is Belgium. This is probably how the French first names came in the family.

## Spelling

The official documents have two spellings: Fesevur and Fesevûr. Where the ^ comes from is not clear yet, but I found it in documents from around 1900.

## Spread over the world

The majority of the people I have found live in the Netherlands. A small number of the Fesevurs live in Belgium. A number of people live in the States. They emigrated in the 1960's. I also got an email from a Fesevur in New Zealand, but this is someone who emigrated from the Netherlands not too long ago.
