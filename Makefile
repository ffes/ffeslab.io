error:
	@echo "No target given"

livereload:
	bundle exec jekyll serve --config _config.yml,_config-dev.yml --livereload

serve:
	bundle exec jekyll serve --config _config.yml,_config-dev.yml

drafts:
	bundle exec jekyll serve --config _config.yml,_config-dev.yml --drafts --future --livereload

build:
	JEKYLL_ENV=production bundle exec jekyll build

rebuild: veryclean prepare build

prepare:
	if [ -f /.dockerenv ]; then npm install --unsafe-perm; else npm install; fi
	bundle config --local path _vendor
	bundle install
	[ -f _config-dev.yml ] || touch _config-dev.yml

clean:
	rm -rf .sass-cache/ _site/ .jekyll-cache/ .jekyll-metadata

veryclean: clean
	rm -rf .bundle/ lib/ node_modules/ _vendor/ Gemfile.lock package-lock.json
