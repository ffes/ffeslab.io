---
layout: default
title: IndentByFold
description: "IndentByFold is an auto indent plug-in for Notepad++ based on the language folding points"
---

# IndentByFold plug-in for Notepad++

## Introduction

IndentByFold is an auto indent plug-in for [Notepad++](https://notepad-plus-plus.org/) based on the language folding points.

## Download

To download version 0.7.3 (2019-11-10) go to the [release page](https://github.com/ffes/indentbyfold/releases/tag/v0.7.3).

The source code of IndentByFold can be found at [GitHub](https://github.com/ffes/indentbyfold/).

## How to install

The easiest way to install is to download it through the `Plugins Admin` in Notepad++.

To manually install the plug-in, copy `IndentByFold.dll` to the `plugins\IndentByFold` directory. Then (re)start Notepad++.

## Documentation

The documentation of IndentByFold can be found at [Read the Docs](https://indentbyfold.readthedocs.io/). This includes:

* A complete [changelog](https://indentbyfold.readthedocs.io/en/latest/changelog.html)
* Instructions [how to build](https://indentbyfold.readthedocs.io/en/latest/building.html)
