---
layout: default
---

# Welcome

Welcome to the website of Frank Fesevur.

On this website you can find information about the various open source [projects]({% link projects/index.html %}) I work on or have worked on.  
I have written a tutorial on how to get started using [vim]({% link vim.md %}).

If you are interested in the (Dutch) [genealogy of the Fesevur family]({% link gen/index.md %}), you can find that here are well.

<p>&nbsp;</p>

### Most recent blog posts

{% include blog-recent.html items=2 %}

View all posts on the [blog]({% link blog/index.html %}).
