# Website Frank Fesevur

This is the source of the website https://www.fesevur.com/ and https://ffes.dev

The staging/test site is available at https://ffes.gitlab.io

The site built using [Jekyll](http://jekyllrb.com/) static site generator.

## Developing

Make sure you have Docker installed.

If you use VS Code, use the devcontainer.

If you don't use VS Code, run this command.

```sh
docker run --rm -ti \
	-p 4000:4000 -p 35729:35729 \
	--volume $PWD:/jekyll \
	--workdir /jekyll \
	--user $(id -u):$(id -g) \
	jekyll/jekyll /bin/bash
```

Within the running docker container:

```sh
make prepare
make livereload
```

## Publish

To push to the site to production create a tag in the form `YYYY-MM-DD` and push that to GitLab and the CI will do its thing.
If you need to deploy more then once on a day, use `YYYY-MM-DD-HHhMM`.
Note the lowercase `h` to separate the hours and minutes.
