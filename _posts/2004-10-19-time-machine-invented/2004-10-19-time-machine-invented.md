---
title: Time Machine Invented!
date: 2004-10-19
lang: en
comments:
  - comment:
    author: Erik
    text:
      - Professor Barabas is so cool....
  - comment:
    author: Michael
    text:
      - hahahahahahah finally they've spent tax money in a good way!!
---

The [City of The Hague](https://www.denhaag.nl) has done a major invention:

**a time machine!**

![A sign about road work, but with an error in the dates](escamplaan.jpg)

If you are familiar with the situation there, they can not even spell the name of the street!

For those of you who can't read Dutch, this is a sign to tell us
about the work on the road at a crossing near my house.
