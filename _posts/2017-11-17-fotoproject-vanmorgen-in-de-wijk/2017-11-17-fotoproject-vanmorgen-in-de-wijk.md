﻿---
layout: post
title: "Fotoproject \"Vanmorgen in de wijk\""
lang: nl
date: 2017-11-17 09:37:56 +0100
category: fotografie
tags:
- fotografie
- weekproject
---

Afgelopen week heb ik voor mezelf een fotoproject gedaan. Het thema was vrij overzichtelijk: "Vanmorgen in de wijk". Dat kwam er op neer dat wanneer ik thuis vertrok ik een foto in onze wijk heb gemaakt en die plaats op [Instagram](https://instagram.com/frankfesevur). En aangezien al mijn foto's vanaf Instagram automatisch ook op [Facebook](https://www.facebook.com/frank.fesevur.3) worden geplaatst, zijn ze daar ook te zien.

Een leuk en bovenal haalbaar project. Geen Project 365 waar je een heel jaar mee bezig moet zijn. Dat heb ik twee keer geprobeerd en nooit verder dan half februari gekomen. Een week is een periode die te overzien is. Een thema gekozen waar je een week mee vooruit kunt. Omdat het 's morgens nog vrij donker is leverde dat mooie plaatjes op. Kortom een geslaagd project.

En de foto's vielen helemaal niet tegen. Als ik ze zelf beoordeel, dan vind ik de foto van [woensdag](https://www.instagram.com/p/BbgcLxSDNsr/) de mooiste, gevolgd door [maandag](https://www.instagram.com/p/BbcSNoMjmjQ/), [vrijdag](https://www.instagram.com/p/BblvqbHDu6T/), [donderdag](https://www.instagram.com/p/BbjBxfYjxZ8/), en [dinsdag](https://www.instagram.com/p/Bbd3Z1pDoRM/).

Het leuke van zo'n project is dat jezelf dwingt om te fotograferen en dat je na moet gaan denken over je foto. Zo wist ik op dinsdag al waar ik de foto van woensdag wilde maken. Dat precies op dat moment die tram aan kwam rijden had ik van te voren niet bedacht, maar maakte het wel de beste foto van de week.

![Woensdag](woensdag.jpg)

Inmiddels heeft het thema van het volgende project zich al weer aangediend. Ik ben op Facebook uitgedaagd:

> Zeven dagen, zeven zwart-wit foto's uit je leven. Geen mensen, geen uitleg.

Nog even kijken wanneer ik dat ga doen, maar er borrelen al diverse ideeën naar boven. En zwart-wit heb ik me nog nooit aan gewaagd. Ik kijk er nu al naar uit!
