---
layout: post
title: NppSnippets 1.0.0 on it's way
date: 2011-08-31 17:07:00 +02:00
lang: en
category: software
tags:
  - nppsnippets
  - notepad++
---

After the release of NppSnippets version 0.7.1 development has continued and I have implemented the most important feature that was on the wish-list from the beginning.

After inserting a snippet the cursor is now at the right position and the selection is restored. It turned out to be an easy fix, just 4 lines of code. This was the last feature that was missing before I felt I could release it as version 1.0. So now that this is done, I will release this next version quite soon. No other new features are planned, just some final testing needs to be done.
