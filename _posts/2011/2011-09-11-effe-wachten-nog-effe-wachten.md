---
layout: post
title: Effe wachten... Nog effe wachten...
date: 2011-09-11 23:32:00 02:00
lang: nl
---

Deze legendarische woorden, [uit een bekende Nederlandstalig lied](https://www.youtube.com/watch?v=5Yz_KpJj3y8), waren deze week zeer van toepassing. Menno had na een ongelukkig balcontact tijdens gym, flink veel last van zijn schouder en dus zijn we toch maar langs de huisartsenpost gegaan.

Maar we konden pas om [tien over negen](https://twitter.com/Carolegend/status/110794517795450880) terecht bij de huisarts. Daar ging het redelijk vlot, maar die stuurde ons toch "even" door naar de SEH.

En daar begon het wachten pas echt. Om half tien binnen, om kwart over tien het intakegesprek, wachten en [wachten](https://twitter.com/FraCaMen/status/110822884024401920).

Aangezien je toch niets beters hebt te doen ga je dan maar het gedrag van een ieder te observeren. Een moeder met tienerdochter met iets aan haar arm. Iemand met de voet omhoog. Er zitten drie jonge knullen die het beste van de wachttijd maken.

Bijna iedereen een smartphone en daar werd driftig gebruik van gemaakt. Af en toe liep er iemand naar buiten om even te bellen. Toen kwam er een oudere man de wachtkamer binnen vanuit de verpleegafdeling. Uit zijn zak haalde hij een iPhone en begon vrij luidruchtig in de wachtkamer zijn zoon en kleinzoon te bellen. Alle aanwezigen wisten nu hoe het zijn vrouw ging. Blijft toch opvallend dat alle jongeren steeds naar buiten liepen en hij speciaal in de wachtkamer ging zitten om te bellen.

Om half twaalf mochten we uiteindelijk naar de behandelkamer. Dan gaat het snel hoop je, maar uiteindelijk ook daar nog een [half uur moeten wachten](https://twitter.com/Carolegend/status/110835019387387905). Om tien over twaalf stonden we weer buiten met de diagnose gekneusd. Dus thuis gekomen heeft Menno een paracetamol ingenomen en is heerlijk in slaap gevallen.

De volgende dag was het al weer bijna over, dus gelukkig was er inderdaad niets ernstigs aan de hand.
