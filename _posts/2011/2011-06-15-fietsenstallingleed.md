---
layout: post
title: Fietsenstallingleed
date: 2011-06-15 21:17:00 +02:00
lang: nl
tags:
- den haag centraal
- fietsenstalling
---

Sinds een paar weken is de fietsenstalling op station [Den Haag Centraal](https://nl.wikipedia.org/wiki/Station_Den_Haag_Centraal) voor abonnementhouders verplaatst van de kelder naar "buiten". Dat buiten is betrekkelijk want ze hebben voor tijdens de verbouwing van het station een tijdelijke stalling gemaakt, netjes met een dak erop.

Tot zover geen probleem. Alleen is er in die stalling niet genoeg plek, in vergelijking met de kelder is het echt een stuk minder. In de kelder had je twee redelijk diepe gangen met twee etages fietsenrekken. Dit aantal wordt in de tijdelijke stalling niet gehaald. Dus een paar weken na de in gebruik name van deze tijdelijke stalling, moeten er dus plaatsen bijgemaakt worden. Daarom is nu 1/5 deel van de toch al te kleine stalling afgesloten om er nieuwe fietsenrekken met twee etages te plaatsen. Hadden ze dit niet eerder kunnen bedenken?

Dus wordt het nog een paar weken behelpen, vrees ik. Nu maar hopen het daarmee echt beter wordt. Ik koop niet voor niets iedere maand een abonnement. Ik verwacht wel enige waar voor mijn geld. Ze zullen het moeten afwachten.
