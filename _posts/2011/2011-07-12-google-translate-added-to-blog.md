---
layout: post
title: Google Translate added to blog
date: 2011-07-12 13:38:00 +02:00
lang: en
category: website
---

Since most of my blog posts are in Dutch, my native tongue, and people from all over the world come to my blog because of my Notepad++ plugin, I added a Google Translate gadget. So all non-Dutch speakers out there can now read all my blog posts. Enjoy!

Know that posts regarding NppSnippets will be written in English, or at least what I think/hope is English.
