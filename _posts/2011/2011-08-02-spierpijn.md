---
layout: post
title: Spierpijn???
date: 2011-08-02 22:48:00 +02:00
lang: nl
category: hardlopen
tags:
  - hardlopen
comments:
  - comment:
    author: Moois &amp; meer
    date: 2011-08-02 22:56:16 02:00
    text:
      - Knap hoor! X Suus
---

Vanavond in Kijkduin hardgelopen en Patty had een pittige training in petto, in ieder geval voor mijn huidige conditieniveau.

Het was een piramide-loop in tempo van 4-6-6-4 minuten, met iedere keer 2 minuten rust. Ik heb de rondjes best aardig vol kunnen houden, maar als ik voel hoe mijn spieren protesteerden het uitlopen en het rekken en strekken na afloop vrees ik dat ik morgen wakker wordt met spierpijn.

Ik zal het moeten afwachten.
