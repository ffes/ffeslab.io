---
layout: post
title: '"Modemissers (volgens mannen)"'
date: 2011-11-01 12:48:00 +01:00
lang: nl
---

Op het <a href="http://www.hln.be/">Belgische HLN</a> kwam ik dit weekend een <a href="http://www.hln.be/hln/nl/40/Mode/article/detail/1340771/2011/10/28/Dit-zijn-de-tien-grootste-modemissers-volgens-mannen.dhtml">artikel</a> tegen over de tien grootste modemissers volgens mannen.

Ik kan me wel vinden in een aantal punten van in het artikel. Inderdaad Uggs zouden verboden moeten worden, net als Crocs trouwens. Ook zo'n harembroek is een regelrechte afknapper, of een weide soepjurk.

Maar andere dingen zijn ook wel wat overdreven en/of erg conservatief. Wat is met mis met felle kleuren? Of rode lingerie? Er kan sowieso weinig fout gaan met mooie lingerie.

Gelukkig heb ik thuis niet veel te klagen. Ik denk dat Caroline van een vuilniszak nog wat weet te maken en zelfs daarmee er prima uit kan zien.
