---
layout: post
title: Zevenheuvelenloop 2011
date: 2011-11-21 23:11:00 +01:00
lang: nl
category: hardlopen
tags:
  - wedstrijd
  - hardlopen
  - zevenheuvelenloop
  - verslag
comments:
  - comment:
    author: TeleRoel
    date: 2011-11-23 09:40:28 01:00
    text:
      - Goed gedaan Frank en een leuk verslag.
      - De Meeuwen Makrelenloop zit al vol, dus daar hoef je je niet op te richten [:)]
      - In maart de CPC?
---

Gisteren was het zover, de <a href="http://www.zevenheuvelenloop.nl/">Zevenheuvelenloop</a> in Nijmegen. Een lange maar geslaagde dag.

Na eerste nog wat pakjes drinken uit de auto gehaald te hebben, <a href="http://twitter.com/FraCaMen/status/138181021752504320">vertrok ik</a> rond 10:00 uur met een <a href="http://twitter.com/FraCaMen/status/137982406249558016">ingepakte rugzak</a> op de fiets naar het station om de trein van 10:38 uur te halen. Ik was ruim op tijd op het perron en daar was het al direct duidelijk dat het een drukke rit zou worden. Er stonden <a href="http://twitter.com/FraCaMen/status/138188313365458945">best veel mensen</a> op het perron in trainingspak en met een <a href="http://nl.mylaps.com/">chip</a> op hun schoen. Allemaal onderweg naar Nijmegen. Gelukkig loonde het ook nu weer om vooraan in te stappen, want daar waren nog zitplaatsen in overvloed. Een voordeeltje voor de ervaren treinreiziger.

Onderweg naar Utrecht al telefonisch contact met André gehad maar op Utrecht CS was het perron voor de trein naar Nijmegen zo vol dat we elkaar daar niet gezien hebben. Ook hier kwam de ervaring weer van pas, want iedereen haastte zich om de coupés in te gaan maar direct naast de deur zijn ook altijd een aantal zitplaatsen. En daar was nog één plek, dus ik ben <a href="http://twitter.com/FraCaMen/status/138201705539047424">gelijk gaan zitten</a>. De meesten hebben moeten staan in de overvolle trein.

Op <a href="http://twitter.com/FraCaMen/status/138217079466762240">station Nijmegen</a> was het een drukte van je welste. André en Sylvia had ik vlot gevonden en samen richting het Keizer Karelplein om daar op zoek te gaan naar de parkeergarage waar we ons konden omkleden. Gelukkig was alles duidelijk aangegeven en was de ING snel gevonden. Toen kwam de "belangrijkste keuze van de dag". Wat doe je nu precies aan? De hele dag was het mistig geweest, maar het leek er toch op dat het zonnetje door zou breken. Eén laagje? Als één laagje, dan lange of korte mouwen? Toch twee laagjes? Na wat getwijfel ervoor gekozen om één laagje aan te doen, maar wel met lange mouwen.

Toen konden we op weg naar de start. Ook dit was weer prima aangegeven en het paarse (ja, het langzaamste) startvak was snel gevonden. Daar wilde André nog even wachten op twee neven van Sylvia die ook vanuit dit startvak zouden vertrekken. En daardoor werd ik ineens opgemerkt door <a href="http://www.huizeherlaar.nl/?p=763">Karin</a>, de vrouw van een oud-collega. We hebben nog even staan kletsen en rond kwart voor twee kwam er beweging in de menigte en konden we langzaamaan richting de start. En minstens net zo belangrijk, het zonnetje brak door.

Dus ik zet mijn Garmin aan, en wat gebeurt er... helemaal niets! Ik had hem de zaterdag ervoor nog gebruikt, 's avonds opgeladen en de gegevens op mijn pc gezet, dus een lege batterij zou het niet kunnen zijn. Maar toch deed hij helemaal niets. Nog geprobeerd om de "magic button" te vinden om hem een reset te geven, maar ook dat lukte niet. Dat was balen!!! Hij had me tot nu toe nog nooit in de steek gelaten! Dan maar lopen zonder de informatie van mijn horloge. Ik had niet veel keuze.

De wedstrijd ging heel goed. De eerste vijf kilometer gingen langzaam omhoog, maar daar merkte ik niet zo veel van. Bij de start had ik geen tijd gezien, dus bij het vijf-kilometer-punt had ik geen idee hoe het ging qua tijd. Het lopen ging makkelijk, maar de echte wedstrijd moest nog beginnen.

Na de lege drinkpost (erg jammer) gingen we na 7 kilometer de Zevenheuvelenweg op. Het echte klimmen! Maar dat viel eigenlijk best mee. Ja, het zijn flinke heuvels waar ik best wat mensen stuk heb zien gaan, maar de vele kilometers door de duinen bleken zich uit te betalen. Ik kon op het tweede stuk mijn tempo prima vasthouden.

Bij de tweede drinkpost was er gelukkig nog wel voorraad dus een paar passen wandelen om een paar slokken te nemen en weer verder. Op weg naar de elfde en zwaarste kilometer. Dat was inderdaad een pittige kilometer, maar ook die kwam ik prima door. En zo kon ik op weg naar de finish. Eigenlijk alleen nog maar afdalen. Bij de finish zag ik dat de laatste tien kilometer rond het uur was geweest, maar mijn echte tijd wist ik niet.

In het uitloopvak een flesje drinken en de medaille opgehaald en vervolgens gewacht op André. Die was niet echt tevreden met zijn 1:43. Het zonnetje had inmiddels plaats gemaakt voor de teruggekeerde mist, dus het koelde vrij snel weer af. Een extra reden om snel weer terug te gaan naar de parkeergarage om om te kleden. Het mobiele internet in Nijmegen was ten tijde van mijn finish zeer slecht, dus even een twitterstilte.

Nadat we omgekleed waren zijn we naar een Grieks restaurant gegaan. De treinen zouden ook op de terugweg wel uitpuilen dus wat later vertrekken kon geen kwaad. Daarom was even zitten met een hapje en een drankje een prima uitkomst om het vertrekmoment te verleggen.

Na bijna twee uur was er weer mobiel internet en was het <a href="http://twitter.com/FraCaMen/status/138293577292845056">fijn</a> en grappig om te zien dat diverse mensen gereageerd hadden op <a href="http://twitter.com/FraCaMen/status/138262655545774080">mijn tijd</a> terwijl ik die zelf nog niet eens wist. Ik bleek er dus 1:31:46 over gedaan te hebben. Een tijd waar ik <a href="http://twitter.com/FraCaMen/status/138292756794716160">best tevreden</a> mee was. Ook de tussentijden gaven aan dat ik een mooi vlak schema had gelopen. De eerste 5 km hadden wellicht iets sneller gekund.

Op de terugweg was een stuk rustiger <a href="http://twitter.com/FraCaMen/status/138294378555916288">in de trein</a>. We konden gewoon zitten en samen met diverse andere lopers was het best gezellig. Zeker als degene naast ons even ge<a href="http://twitter.com/FraCaMen/status/138309185036230656">mist</a> hadden dat ze in de Intercity zaten en niet in de stoptrein naar Arnhem en dat die dus niet in Elst stopte.

In Utrecht afscheid genomen van André en Sylvia en <a href="http://twitter.com/FraCaMen/status/138317140892729344">rond half acht</a> was ik weer thuis. Een leuke dag, een erg leuke en mooie loop. Een resultaat waarmee ik wat kan, gegeven de ietwat krappe voorbereiding. Nu weer een doel stellen voor een volgende loop!
