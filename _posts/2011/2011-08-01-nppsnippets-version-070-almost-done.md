---
layout: post
title: NppSnippets version 0.7.0 almost done
date: 2011-08-01 14:21:00 +02:00
lang: en
category: software
tags:
  - nppsnippets
  - notepad++
---

During our vacation [Notepad++ 5.93](http://notepad-plus-plus.org/download/) was released which includes the new messages `NPPM_GETLANGUAGENAME` and `NPPM_GETLANGUAGEDESC`. As promised, I'm busy with the next version of [NppSnippets]({% link nppsnippets.html %}) that uses these messages.

I have completed the last feature for this release and I will do the final testing in the next days. I hope to release version 0.7.0 within a couple of days. So stay tuned to [my blog]({% link blog/index.html %}) or follow the [N++ plug-in forum](https://sourceforge.net/projects/notepad-plus/forums/forum/482781/).

To take full advantage of the new features in NppSnippets you need to upgrade to N++ 5.93 although with an older version the plug-in will still work, but you can not use all the features.
