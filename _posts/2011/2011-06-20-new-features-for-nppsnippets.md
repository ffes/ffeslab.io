---
layout: post
title: New features for NppSnippets
date: 2011-06-20 11:17:00 +02:00
lang: en
category: software
tags:
  - nppsnippets
  - notepad++
comments:
  - comment:
    author: Inomoz
    date: 2011-07-12 12:38:58 +02:00
    text:
      - Been using your plugin with zencoding mostly to create code templates (no individual tags).
      - The new version will not be accidentally support hot keys? And the grouping tags ... It would be nice.
      - P/S google translate
---

This weekend I implemented two major new features for [NppSnippets]({% link nppsnippets.html %}).

There is now a dialog to edit which languages can be used for a certain library. For this feature I need to wait for the next version of [Notepad++](http://notepad-plus-plus.org/) to be released, since I'm using the [new plug-in messages](http://sourceforge.net/projects/notepad-plus/forums/forum/482781/topic/4574403) that I asked Don for recently. These messages give me a reliable way to get the names of the supported languages in Notepad++.

Another feature I added is an import for libraries from another NppSnippets database. This way distributing libraries between users become more easy. Send another user a database and (s)he can import the wanted library. I'm also planning to put up a special section on my website for redistributing user provided libraries. So if you have created a library and you think it is useful for other users, drop me a line and I will put it on my website.

So as soon as the next version of Notepad++ has been released, I will give the new edit language feature a final test and release the next version of NppSnippets.
