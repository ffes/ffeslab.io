---
layout: post
title: NppSnippets 1.1.0 just around the corner
date: 2011-11-08 13:43:00 +01:00
lang: en
category: software
tags:
  - nppsnippets
  - notepad++
---

Just a short message to inform you that NppSnippets 1.1.0 is just around the corner.

To plugin itself is feature complete. I just need to rewrite some sections of the documentation to reflect these changes and do some last testing. I hope to release within a week.

The first new feature is the possibility to create a snippets from the clipboard or the current selection.

This next version will become available through the Plugin Manager as well. To prevent the Plugin Manager from overriding your database I had to improved the detection and installtion of the snippets database. This is the second important improvement to the plugin.
