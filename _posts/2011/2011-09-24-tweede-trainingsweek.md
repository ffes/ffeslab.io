---
layout: post
title: Tweede trainingsweek
date: 2011-09-24 13:16:00 +02:00
lang: nl
category: hardlopen
tags:
- hardlopen
- zevenheuvelenloop
---

Deze week weer volop in training voor de Zevenheuvelenloop. Maandag stond er weer een lange duurloop op het programma. Na de problemen van vorige week, was ik toch wel erg benieuwd hoe het zou gaan.

Ook nu was het weer een duurloop van 45 minuten. De training van afgelopen zaterdag gaven me vertrouwen dat het deze keer goed zou gaan. De eerste kilometers ging ik iets te hard van start en om de een of andere reden had ik bij vrijwel iedere kruising last van het verkeer. Erg vervelend. Een aantal keren moest ik echt even wachten. Maar ondanks dat (of misschien wel juist dankzij, wie zal het zeggen) liep ik de ruim 45 minuten vrij eenvoudig uit.

Voor de woensdag stond er eigenlijk een interval op het schema en voor zaterdag weer een duurloop. Aangezien we op zaterdag bij <a href="http://www.kekwek.nl/">Kekwek</a> bijna altijd intervaltrainingen doen, had ik het programma van deze twee dagen omgedraaid en dus stond er weer een lange duurloop op het programma.

Rustig aan, was het devies. "Onze zomer qua weer" - het voorjaar dus - was de laatste keer dat ik een uur had volgemaakt. De eerste kilometers vielen wat tegen, maar toen ik eenmaal 15 tot 20 minuten bezig ging het eigenlijk steeds makkelijker. Het rustige tempo van rond de 9 km/u was prima vol te houden en op een minuutje na heb ik het uur voltooid.

Donderdag werd mij gevraagd of ik zaterdag de training wilde geven. In overleg met de trainer hebben we een redelijk stevig programma gemaakt, dus kan ik zelf ook goed aan de bak.

Zaterdag dus de training bij Kekwek. We hebben eerst rustig inlopen en wat oefeningen gedaan. Daarna kwam "the main event": een piramide van 2-4-6-4-2 minuten, iedere keer gevolgd door 50% rust. Om de training af te sluiten heb ik nog wat korte tempo's gedaan en toen zat het er al weer op.

En zo kwam de teller voor deze week op 25,5 kilometer te staan. Een lekkere trainingsweek, zonder problemen. Wel veel kilometers, maar wat wil je met twee keer een duurloop.
