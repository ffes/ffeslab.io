---
layout: post
title: NppSnippets 1.0.0 released
date: 2011-09-08 09:54:00 +02:00
lang: en
category: software
tags:
  - nppsnippets
  - notepad++
---

Last Tuesday I released version 1.0.0 of [NppSnippets]({% link nppsnippets.html %}), my snippets plug-in for Notepad++.

I implemented the last feature needed to bump the version out of the 0.x series and make it a 1.0.0. The selection or cursor position are now restored after inserting a snippet. More information and downloads can be found at [my website]({% link nppsnippets.html %}).

I will look at the new [Plugin Manager interface](https://sourceforge.net/projects/notepad-plus/forums/forum/482781/topic/4695234) as soon as possible, so people will get an automatic update from the Plugin Manager.
