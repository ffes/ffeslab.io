---
layout: post
title: Laan van Meerdervoortloop 2011
date: 2011-11-07 09:12:00 +01:00
lang: nl
category: hardlopen
tags:
- wedstrijd
- den haag
- laan van meerdervoortloop
- hardlopen
- verslag
---

Gisteren was het de dag van de <a href="http://www.lvml.nl/">Laan van Meerdervoortloop</a>. Aangezien ik van de week best veel last van mijn hamstring had was ik een beetje bang dat ik toch een blessure had opgelopen.

Maandag had ik bijna 13 kilometer gelopen, maar had ik een pijntje in/bij mijn hamstring. Dus de rest van de week niets gedaan om de spier rust te geven. Deze Laan van Meerdervoortloop was dus een zeer goede test. Ik wilde hem als een echte wedstrijd lopen, maar ik wilde vooral geen blessure oplopen in aanloop naar de <a href="http://www.zevenheuvelenloop.nl/">Zevenheuvelenloop</a>, die al over twee weken is. Een duidelijke opdracht dus: geen blessures!

Ik was rond 12:15 uur in Kijkduin om eerst mijn startnummer op te halen. De rij was langer toen ik hem had opgehaald dan toen ik zelf in de rij ging staan. Dat was weer een meevallertje. Daarna naar <a href="http://www.restaurant-lafontaine.nl/">La Fontaine</a>. Daar hadden we met de anderen van <a href="http://www.kekwek.nl/">Kekwek</a> afgesproken als verzamelpunt. Uiteindelijk met ons zessen gezamenlijk de voorbereiding gedaan. Dat waren Roel, Kees, Patricia en ik voor de 10 en Sandra en Digna die de 5 gingen doen. Tijdens het inlopen had ik gelukkig geen last van mijn hamstring. Dat gaf goede moed voor de wedstrijd.

Direct na de start was het best smal, en daardoor moest ik direct Patricia en Roel laten gaan en liet ik Kees achter me. Het eerste deel van de wedstrijd doet zijn naam eer aan. Over de Laan van Meerdervoort tot aan het De Savornin Lohmanplein en daarna richting zee. Op de atletiekbaan van Haag was de drinkpost en daar even 10 passen gewandeld om een paar slokken te nemen.

Vlak na de atletiekbaan kwam ik <a href="https://www.facebook.com/willem.wissink">Willem Wissink</a> tegen, mijn oud-handbaltrainer, die ik met enige regelmaat tegenkom bij diverse hardloopwedstrijden in de regio. Dat was wel prettig, want hij liep met een clubje en hun tempo was ook mijn tempo. Het is altijd fijn om een stukje samen op te lopen.

Daarna kwam de zware tweede helft van de race. De lange klim de duinen in vanaf de Laan van Poot richting het strand. Wat een end blijft dat toch! Daarna nog een stuk door de duinen richting Duindorp, om bij strandopgang 10 het strand op te gaan.

Dat betekent dus dat je door het mulle zand richting de zee loopt om naar het "harde" zand te lopen. Dat was afzien en daar moest ik het groepje van Willem laten gaan. Langs de vloedlijn was het smal, want het was net vloed geweest. De ruime twee kilometer over het strand hadden we wind mee en dat was wel zo prettig. Maar dan weet je al dat je ook het strand weer af moet en omhoog bij Kijkduin. Dat was inderdaad het zwaarste stuk van de hele wedstrijd.

Onderweg op diverse plekken een hoop bekenden in het publiek tegengekomen. Dat was erg prettig om iedereen langs het parkoers te zien staan!

De finish gehaald in 58:59 (handgeklokt) maar de officiële eindtijd was 59:05. Gezien het parkoers ben ik daar best tevreden over! Na de finish kwam ik de rest van de groep al snel tegen. Iedereen was tevreden en na een gezamenlijk drankje bij La Fontaine allemaal weer naar huis.

Zoals je uit dit verslag kunt afleiden dat ik helemaal geen last van mijn hamstring heb gehad en dat was de grootste winst van vandaag! Heerlijk gelopen over een uitdagend parkoers. Een leuke wedstrijd en ook gelijk een prima training en test voor de Zevenheuvelenloop.
