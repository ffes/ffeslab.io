---
layout: post
title: Nieuwe poging
date: 2011-06-14 16:31:00 +02:00
lang: nl
category: website
tags:
- hardlopen
---

Ik ga **nogmaals** proberen een weblog bij te houden, met name over mijn hardloop-vorderingen dit keer.

En wat heb ik dan allemaal te melden? Aangezien mijn hardloopinspanningen de laatste tijd in het algemeen wat aan de lage kant zijn, kan het geen kwaad daar wat extra energie in te stoppen. Daarom hoop ik dat het bij gaan houden van een weblog hier bij zal helpen.

Dus het is, ook voor mij, even afwachten hoe het gaat lopen [;-)], maar een begin is gemaakt.
