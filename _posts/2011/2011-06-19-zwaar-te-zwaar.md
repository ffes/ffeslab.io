---
layout: post
title: Zwaar, te zwaar
date: 2011-06-19 15:12:00 +02:00
lang: nl
category: hardlopen
tags:
- hardlopen
---

Gisterochtend heb ik voor het eerst sinds drie weken weer op [de club](http://www.kekwek.nl/) getraind. De laatste keer dat ik had hardgelopen was tijdens de zesuursestafette. Drukke weekenden en geen tijd maken door de weeks waren er het gevolg van dat ik het gisteren zwaar had.

En dat is dan nog voorzichtig gezegd. Normaal gesproken loop ik redelijk vooraan mee, nooit helemaal op kop maar toch wel bij de snellere van de groep. Gisteren was dat echter duidelijk niet het geval. Drie weken niets doen eist zijn tol. Ik liep achteraan te hijgen en te puffen. Dat was lang geleden dat het zoveel moeite had gekost.

Maar te gelijk is dit een mooie wakeup-call. Er moet weer wat gaan gebeuren! Komende week een avond in de agenda zetten om zelf weer een (binnen)rondje Zuiderpark te doen en dan vanaf daar de draad weer oppakken. Een paar kilo kwijt zal trouwens ook helpen.

Dus tot zover het goede voornemen van dit weekend. Nu me er nog aan houden!
