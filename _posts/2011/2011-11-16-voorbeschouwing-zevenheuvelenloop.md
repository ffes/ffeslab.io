---
layout: post
title: Voorbeschouwing Zevenheuvelenloop
date: 2011-11-16 10:12:00 +01:00
lang: nl
category: hardlopen
tags:
- hardlopen
- training
- zevenheuvelenloop
---

Aanstaande zondag is het zover: mijn eerste deelname aan de <a href="http://www.zevenheuvelenloop.nl/">Zevenheuvelenloop</a> in Nijmegen. Wat kan ik ervan verwachten?

De afgelopen maanden heb ik hard gewerkt aan de voorbereiding op de Zevenheuvelloop van aanstaande zondag. Mijn broer <a href="{% post_url 2011/2011-09-08-zevenheuvenloop-2011 %}">legde de uitdaging neer</a> om daar samen aan de start te verschijnen en die uitdaging ben ik aangegaan. Daarvoor waren meerdere redenen.

Ten eerste is het altijd leuk om samen met André aan de start van een wedstrijd te staan. Ten tweede was ik conditioneel niet top. Ik had op dat ogenblik al moeite met 5 kilometer en had ik geen echt doel om naar toe te werken om weer in conditie te komen. Dat was er nu wel! Ten derde had ik veel goede verhalen gehoord over de Zevenheuvelenloop en aan zo'n grote wedstrijd wilde ik toch wel een keer deelnemen. En ten vierde was het voor de weegschaal ook wel prettig als ik weer wat ging doen. Die heeft nu ook iets minder moeite om mij te dragen.

Die uitdaging ben ik dus om meerdere redenen aangegaan. Het was begin september en ik had nog zo'n drie maanden aan voorbereidingstijd. Een beetje krap gezien mijn conditie op dat moment, maar het zou moeten kunnen. Ondanks wat kleine tegenslagen met wat protesterende spieren ging de voorbereiding vrij voorspoedig.

Toch was de aanloop echt wel wat aan de korte kant en ga ik zondag naar Nijmegen vooral om de wedstrijd uit te lopen. Een verwachting voor een eindtijd heb ik niet echt. Ik heb als verwachte eindtijd bij inschrijving 1:40 opgegeven. Dat was vooral om er zeker van te zijn dat ik samen met André in het startvak kwam te staan. Zelf hoop ik wel wat sneller dan dat te zijn en ergens laag in de 1:30 uit te komen, maar het voornaamste is toch echt de finish halen. Gezien de voorbereiding en de lange duurlopen van de afgelopen tijd zie ik geen reden waarom ik de finish niet zou halen, en dan zien we wel wat de uiteindelijke tijd zal worden.

Zondag om 10:38 uur vertrekt mijn trein vanaf Den Haag Centraal naar Utrecht. In Utrecht probeer ik André te vinden en stappen we over op de trein naar Nijmegen. Daar hebben we dan nog bijna een uur om in het paarse startvak te komen, waar we tegen twee uur van start kunnen gaan. Dan is de winnaar normaal gesproken al binnen! Ondanks dat kleine details, heb ik er zin in!
