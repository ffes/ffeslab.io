---
layout: post
title: Zevenheuvenloop 2011
date: 2011-09-08 12:36:00 +02:00
lang: nl
category: hardlopen
tags:
- hardlopen
---

Ik werd gisteren door mijn broer uitgedaagd om dit jaar mee te doen aan de [Zevenheuvelenloop](http://www.zevenheuvelenloop.nl/). Een wedstrijd in de omgeving van Nijmegen van 15 km over een heuvelachtig parcours.

En aangezien ik dit jaar nog niet heel veel duurtraining heb gedaan en ik behalve de halve marathon van ruim twee jaar geleden bij de [City-Pier-City-loop](http://www.cpcloopdenhaag.nl/) nog nooit een wedstrijd gelopen van meer dan 10 km heb gelopen, is het zeker een uitdaging.

Ik heb me nog niet ingeschreven en laat het nog even bezinken, maar ik denk dat ik de uitdaging aanga. Jezelf weer een doel stellen kan nooit kwaad. We zien elkaar zaterdag en dan zullen we het er zeker nog over hebben. Aangezien er het aantal lopers gelimiteerd is moeten we niet te lang wachten met het nemen van het besluit.

Maar dat betekent wel dat ik serieus aan de bak moet. Gisteravond heb ik voor het eerst sinds lange tijd weer een buitenrondje Zuiderpark (4 km) gelopen en dat viel helemaal niet tegen. Met een tevreden gevoel kon het rondje vrij eenvoudig afmaken. Dus vanaf volgende week het rondje weer gaan verlengen en weer duurinhoud opbouwen.

En als de Zevenheuvelenloop lukt, zijn er weinig redenen om me niet in te schrijven voor de CPC of de 20 van Alphen.
