---
layout: post
title: Training voor de Zevenheuvelenloop
date: 2011-10-22 22:09:00 +02:00
lang: nl
category: hardlopen
tags:
- laan van meerdervoortloop
- hardlopen
- training
- zevenheuvelenloop
---

De afgelopen weken zijn mijn trainingen in voorbereiding op de Zevenheuvelloop uiteraard gewoon doorgegaan, al heb ik mijn blog daarover een beetje verwaarloost. Alleen zat het vorige week niet echt mee.

Het schema heb ik netjes aan kunnen houden en de lange duurlopen gaan steeds makkelijker. Twee weken geleden op zondag ruim een uur bezig geweest en ruim 10 km gelopen van en naar de duinen en Kijkduin. De duinen door, dus dan heb je gelijk wat heuveltraining erbij. Met de Zevenheuvelenloop in aantocht is dat toch wel nodig. Ik had een paar paadjes genomen die best stevig waren, mul zand en vrij stijl omhoog en dat viel toch wel tegen.

De daarop volgende week zat het allemaal even tegen. Ik voelde me zelf niet top, dus moest daardoor een training laten schieten en de andere training kwam door een overvolle agenda ook in het gedrang. Dus onverwacht en ongepland een weekje wat rustiger aan gedaan. Maar de zaterdagtraining daarop ging wel gewoon door en om de een of andere reden (of misschien wel juist door de rust) ging dat erg lekker.

Ook deze week was er weer een druk schema. De lange duurloop op maandag ging prima en de lange intervallen op woensdag worden steeds langer, maar gingen ook prima. Ik loop inmiddels 9 minuten op een DL3 tempo. Vandaag hadden we op de club ook een stevig interval-programma. En ik merk dat de trainingen steeds makkelijker gaan, dat ik het langer volhoud en dat de snelheid ook omhoog gaat.

Dit weekend ga ik me inschrijven voor de <a href="http://www.lvml.nl/">Laan van Meerdervoortloop</a> op 6 november, dus twee weken voor de trip naar Nijmegen. Het parkoers is een "ware uitdaging" volgens de site. Vorige jaar was het de eerste keer op dit vernieuwde parkoers, maar moest ik deze loop laten lopen omdat ik totaal niet getraind was. Maar dit jaar is dat anders en moet het gaan lukken. Volgens de verhalen van leden van <a href="http://www.kekwek.nl/">Kekwek</a> was het inderdaad een zwaar parkoers en moet ik absoluut niet rekenen op een snelle tijd. Een flink stuk duinen en ruim 2,5 km strand. Dus eigenlijk een prima in de aanloop naar de Zevenheuvelenloop.
