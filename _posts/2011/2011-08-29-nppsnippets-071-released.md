---
layout: post
title: NppSnippets 0.7.1 released
date: 2011-08-29 10:18:00 +02:00
lang: en
category: software
tags:
  - nppsnippets
  - notepad++
---

Yesterday I released a new version of [NppSnippets]({% link nppsnippets.html %}). This version fixes a nasty bug in the edit languages dialog that could cause Notepad++ to crash. Because of this bug version 0.7.0 was released, but never announced.

The most noticeable new feature for version 0.7.x are:

- A user interface for editing the language selection for libraries has been added. You need at least Notepad++ version 5.93 for this feature.
- You can import a library from another NppSnippet database.
- Start a new document for a certain snippets, and allow that snippet to set the language of that new document. There were already fields in the database for this. It can be very useful to start a new CSS-file or JavaScript-file from HTML, etc.
- For the next version I planned to finally build the longstanding wish to keep or restore the selection when you insert a snippet that surrounds a selection. When this is done, it could become version 1.0!
