---
layout: post
title: Meedoen met de Glazen Huis Loop 2011?
date: 2011-12-02 09:03:00 +01:00
lang: nl
category: hardlopen
tags:
- serious request
- hardlopen
---

Zondag 18 december is in Leiden de <a href="http://www.glazenhuisloop.nl/">Glazen Huis Loop</a>. De opbrengst van deze loop gaat naar de 3FM-actie <a href="http://seriousrequest.3fm.nl/">Serious Request</a>.

Het wordt een loopje van 5.5 km, dus voor velen te doen. En gezien de achterliggende gedachte bij deze loop geldt: **Meedoen is belangrijker dan winnen!!!**

Wij zijn al jaren fan van de Serious Request actie en tijdens die periode wordt er niet veel anders gekeken op tv. En aangezien de kans groot is dat Menno die zaterdag op zondag in Leiden logeert, is het een logische combinatie om bij het Glazen Huis te gaan kijken en dan ook gelijk mee te doen aan de Glazen Huis Loop.

We moeten dus nog even bekijken of het bij ons past qua planning, want het lijkt mij er leuk om mee te doen. En in die planning zit nog wel een kritisch pad, dus ik hoop dat we daar uitkomen.
