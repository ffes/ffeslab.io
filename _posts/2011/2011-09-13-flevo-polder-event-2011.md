---
layout: post
title: Flevo Polder Event 2011
date: 2011-09-13 22:06:00 +02:00
lang: nl
tags:
- geocaching
---

Afgelopen weekend hebben wij voor de eerste keer deelgenomen aan het FPE, oftewel het <a href="https://coord.info/GC2M2KX">Flevo Polder Event</a> in Almere. Dus niet te laat opstaan, spullen inpakken, de auto in en die kant op.

Onderweg, bij Almere Buiten, eerst een <a href="https://coord.info/GC2ET0R">carpoolcache</a> met 33 favorites points gedaan. Met zoveel punten moet er iets bijzonders mee aan de hand zijn. Ter plekke aangekomen wees de pijl al snel de juiste kant op en was de cache eenvoudig te vinden. Het was zeker een creatieve carpoolcache. Een stuk leuker dan de meesten. Op die parkeerplaats kwamen nog een stel andere cachers tegen, maar die lieten deze cache aan zich voorbij gaan.

Toen konden we door naar het event zelf dat werd georganiseerd door of bij <a href="http://www.scoutingdemeridiaan.nl/">Scouting De Meridiaan</a>. Er waren veel nieuwe caches gemaakt, maar aangezien we tot vandaag geen enkele cache in Flevoland hadden gevonden, was dat voor ons geen vereiste. Er bleek <a href="https://coord.info/GC2X241">één nieuwe cache</a> vanaf het terrein te vertrekken, dus die als eerste gedaan.

Bij het ophalen van de elektronische gegevens voor in de gps, konden alle aanwezigen grabbelen in een bak met hele kleine flesjes. Na wat gepiel om het blaadje uit het kleine flesje te halen bleek dat ik één van de gelukkige winnaars was van een <a href="https://coord.info/TB39PKJ">geocoin</a>. Een erg leuke idee en als je dan ook nog wat wint...

Dus konden we op weg voor de cache over "Almera en Achtbaangek". Al tijdens de wandeling naar het eerste waypoint vormden zich enkele groepjes. En zo kwamen we met de cachers die we bij de carpoolcache waren tegenkomen in een groep terecht. Dit gezellig groepje bleef de hele cache in takt en als groep hebben we ook de cache gevonden. Een leuke wandeling, we wisten niet dat er in Almere zulke leuke stukjes natuur te vinden waren.

Teruggekomen op het eventterrein een heerlijke hamburger gegeten en wat gedronken en toen op weg naar het <a href="http://www.kemphaan.nl/">Stadslandgoed De Kemphaan</a>. Daar waren 5 nieuwe caches en een challenge geplaatst. Maar aangezien onze TomTom Start niet de mogelijkheid heeft om coördinaat in te voeren was het nog een uitdaging om de weg te vinden. De Map60CSx had het moeilijk en stuurde ons onlogische kanten op. Dus het coördinaat ingevoerd in de Google Maps apps op mijn telefoon. Maar mijn telefoon heeft de laatste tijd wat kuren, en die resette zichzelf weer eens. Gelukkig had de app wel het coördinaat omgezet naar een adres, dus konden we dat adres invoeren in de TomTom en waren we alsnog snel ter plaatse.

We wilden als eerste de <a href="https://coord.info/GC2Z5B5">Wherigo-cache</a> gaan doen, maar toen de telefoon voor de derde keer die dag protesteerde besloten we deze maar te laten voor wat het was en de andere caches te proberen. Dan maar eerst de <a href="https://coord.info/GC2Z5B4">Letterbox</a> doen. Onderweg daarheen kwamen we diverse cachers tegen, maar we vonden deze cache zonder dat er anderen bij waren. Dat maak je niet vaak mee op zo'n dag! Toen zijn we doorgelopen naar de <a href="https://coord.info/GC2Z5AZ">Traditional</a>. Ook deze was snel gevonden en weer was het geen file-loggen.

Na deze cache gingen we het richting het startpunt van de <a href="https://coord.info/GC2Z5AQ">Multi</a>. Toen kwamen een groep tegen die hier net mee begonnen waren. We sloten ons aan bij het team <a href="http://www.geocaching.com/profile/?guid=b87e883e-3a6b-441c-bf9a-cd6e9aaccdb0">Leandraakje</a>. Die waren we op het eventterrein al twee keer tegengekomen, dus gingen we met zijn zessen onderweg. De multi was wat lastig, maar uiteindelijk werd hij wel gevonden. Aangezien zij de Wherigo wel, maar de andere caches van de challenge nog niet gedaan hadden zijn we samen verder opgetrokken. Daardoor konden we ook de <a href="https://coord.info/GC2Z5BA">Mystery</a> loggen.

En zo eindigde een zeer geslaagde dag in Almere. Een leuk event, leuke creatieve caches gedaan, leuke cachers ontmoet. Het weer werkte ook mee, dus het Flevo Polder Event is zeker voor herhaling vatbaar. Dus wie zijn we volgend jaar weer aanwezig.
