---
layout: post
title: Gefopt door een kat
date: 2011-07-03 22:15:00 +02:00
lang: nl
tags:
- bieslandse bos
---

Als kampstaf heb je altijd bezoekers op je [terrein](http://www.bieslandsebos.nl/). Zo ook gisteren, maar nu was er ook een zwart met witte kat.

Op zich niet raar zal je denken, maar tot nu toe hadden wij nog nooit een kat op het terrein gezien. En deze kat was duidelijk geen zwerver. Erg aanhankelijk, kopjes geven, aanhalen en optillen was geen probleem. Toen het beestje er 's avonds nog was, vroegen wij ons toch af wat we ermee aanmoesten.

Wij op internet gezocht of er een zwart met witte kat vermist was in de buurt. En ja, wat schetst onze verbazing, in Berkel en Roodenrijs was er inderdaad een vermist en die leek sprekend op de kopjes-geef-machine die wij hadden rondlopen.

Dus na veel vijven en zessen, besloten toch een poging te doen om met de eigenaar in contact te komen. Na wat speurwerk een mobiel nummer kunnen achterhalen en een sms gestuurd. Binnen twee minuten teruggebeld door een mevrouw die duidelijk moeite had om haar emoties in bedwang te houden. Wij hebben uiteraard erbij gezegd dat wij niet zeker weten of het haar kat was, maar heel begrijpelijk sprong ze gelijk in de auto en kwam naar ons toe.

De mevrouw op de parkeerplaats opgewacht. Een aardige jonge dame, die vertelde dat ze haar kat al bijna een maand kwijt was en ik nogmaals gezegd dat wij hoopten dat het inderdaad haar kat was en dat haar bezoek geen teleurstelling zou zijn, maar wij wisten eigenlijk zeker dat het goed zat.

Bij het huisje aangekomen, zag ze het direct! Het was **niet** haar kat! Sterker nog, haar vermiste lieveling was een poes en dit was een gecastreerde kater. Om dat zeker te weten, tilde ze de staart van onze gast op om het nog even te controleren. Die was daar niet blij mee en beet haar in haar hand (niet hard, maar toch). Ze vond het toch erg fijn dat we haar gebeld hadden want, zo zei ze, alleen al het feit dat zij wist dat er mensen waren die dit voor een kat overhadden deed haar goed. Maar toch moest ze onverricht ter zake weer naar huis.

Maar wat nu? In het huisje houden was geen optie. Wij willen hem niet houden. En er was nog een praktisch punt. Zo'n beest wil je zonder kattenbak niet binnen hebben. Daarnaast wil je niet dat hij hier op het terrein blijft zwerven en er een volgende kampstaf mee opzadelen. Het snoepje toch maar naar buiten gezet en als hij er morgen nog zou zijn zouden we proberen het asiel te bellen.

Vanmorgen was de kat er niet meer. Hij was blijkbaar toch gewoon van de buurman of hij was op doorreis en nu ging hij nu op weg naar zijn volgende slachtoffer om die weer een niets vermoedende katteneigenaar blij te maken met een dode mus.
