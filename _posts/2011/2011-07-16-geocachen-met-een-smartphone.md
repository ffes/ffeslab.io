---
layout: post
title: Geocachen met een smartphone
date: 2011-07-16 21:01:00 +02:00
lang: nl
tags:
- geocaching
- garmin
- android
- smartphone
- paperless
---

Recent kreeg ik van een geocacher de vraag naar onze ervaring over cachen met een smartphone. Wij geocachen nu ruim drie jaar paperless zoals dat heet. Pocket Queries, samen met de bookmark-lists, zijn voor mij de belangrijkste redenen om premium member van [geocaching.com](https://www.geocaching.com/) te zijn.

Ik heb zelf nu ruim een jaar een HTC Desire, dus Android gebaseerde telefoon en had daarvoor twee jaar een Windows Mobile gebaseerde telefoon. Die gebruik ik vrijwel altijd in combinatie met mijn [eTrex Venture HC](http://www.garmin.com/products/etrexventurehc). Ik zoek en vind af en toe wel een cache met alleen de telefoon, maar dat is meer voor zomaar even een cache meepakken als we ergens zijn. Maar de echte gps heeft duidelijk voordelen.

Het grootste voordeel van de Venture is de robuustheid. Als ik die laat vallen, of wordt overvallen door een regenbui, dan is dat totaal geen probleem, terwijl mijn telefoon daar niet tegen kan. Dat is en blijft een redelijk kwetsbaar ding.

De batterijen in mijn gps gaan veel langer mee dan de accu van mijn telefoon en daarnaast gebruikt de Garmin AA-batterijen, dus verwisselbaar.

Een ander voordeel is de leesbaarheid van het scherm. Het scherm van mijn telefoon is niet optimaal leesbaar in de volle zon, daar heb ik bij mijn Garmin veel minder last van.

En het is zo dat de gps-ontvanger in de telefoon minder nauwkeurig is dan die in mijn Garmin. Maar op het laatste stukje komt het toch aan op het doosje zoeken en niet zo zeer op de gps. Dus dat probleem is volgens mij dus niet zo groot.

Maar de voordelen van een smartphone met de gpx-files en met name de [geocaching-app](https://market.android.com/details?id=com.groundspeak.geocaching) zijn overduidelijk. Altijd live hun database kunnen raadplegen met de app is ideaal. Die luxe van altijd en overal de cachegegevens kunnen opvragen went erg snel.

Zo plannen wij voor een vakantie niet echt van te voren welke caches we gaan doen. We kijken wel een beetje of er caches zijn die we heel graag zouden willen doen, zoals voor een mooie wandelingen of leuke, bijzondere punten, maar echt plannen is het niet. We zorgen voor een ruime pocket query met voldoende caches en zet die zowel in mijn gps (voor de coördinaten) als op mijn telefoon (voor de cache-info), als in mijn TomTom (nuttige plaatsen). En ter plekke kijken we dan wel waar we in de buurt komen en waar we zin in hebben.

Dus voor ons werkt de combinatie het beste. De telefoon is inmiddels wel vrijwel onmisbaar geworden tijdens het geocachen, met name voor het paperless cachen, dus de informatie van de cache-pagina en recente logs, maar kan de robuuste Garmin zeker niet vervangen.
