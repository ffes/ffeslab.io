---
layout: post
title: Eerste trainingen richting Zevenheuvelenloop
date: 2011-09-16 16:37:00 +02:00
lang: nl
category: hardlopen
tags:
- hardlopen
- training
- zevenheuvelenloop
---

Afgelopen week ben ik begonnen met de training voor de Zevenheuvelenloop. Aangezien ik nog niet echt in shape ben, heb ik het schema voor de halve marathon erbij gepakt. Maandag en woensdag was ik van plan te gaan trainen.

Maandagavond niet te veel gegeten en een duurloop van 45 minuten stond op het schema. De laatste keer dat ik zo lang achter elkaar had gelopen was ruim voor de zomervakantie, dus bij voorbaat ging ik er al niet vanuit dat ik het vol zou houden. Omkleden, mijn hardloopschoenen aan en op weg.

Conditioneel viel het helemaal niet tegen, maar na een half uur begonnen mijn benen toch wel moe te worden. Ik ging meer sloffen en voelde een licht spierpijn opkomen. Op een paar honderd meter van huis voelde ik een pijntje aan de voorkant van mijn been! Dus direct gestopt met lopen, want een <a href="http://www.blessure-aanwijzer.nl/DaviWB/Pagina15.html">blessure</a> is het laatste waar ik op zit te wachten. Gaan lopen forceren heeft geen zin, en zo dicht bij huis al helemaal niet.

Ondanks dat, ben ik toch wel met een tevreden gevoel thuis gekomen. Het begin was gemaakt!

De volgende ochtend waren alle spieren gevoelig, maar echt zeer deed het niet. Een beetje extra rekken en strekken en de tweede training van deze week maar uitstellen tot donderdag en niet te hoog inzetten. Een extra dagje rust kan geen kwaad.

Donderdagavond was het weer zover. De tweede training van deze week. De pijntjes in mijn benen waren weg, dus vol goede moed ging ik van start in het Zuiderpark. Door de drukte van de dag was het inmiddels al over achten en dat betekende dat het al <a href="http://www.zonneklok.nl/">donker werd</a>. Dus voor het eerste weer het reflecterende hesje aan. Een aantal wat langere tempo's stonden op het schema. Vol goede moed begonnen, de eerste twee tempo's van 5 minuten gingen prima, maar tijdens de derde voelde ik dat mijn spieren nog niet helemaal hersteld waren. Dus de training afgebroken en in een hele rustige dribbel op huis aan. Die dribbel ging zonder problemen en 's avonds verder geen reactie meer gevoeld.

Eerst morgen de training bij <a href="http://www.kekwek.nl/">Kèkwèk</a> even afwachten, maar heel ernstig voelt het allemaal niet. Dus vooralsnog zeg ik: "Nijmegen, ik kom er aan!"
