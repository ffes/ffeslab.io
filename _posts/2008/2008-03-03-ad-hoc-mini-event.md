---
layout: post
title: Ad hoc mini event
date: 2008-03-03
lang: en
tags:
  - geocaching
---

As [I told you before]({% post_url 2007/2007-09-17-geocaching %}), we [geocache](http://www.geocaching.com/profile/?guid=9c17cffb-7230-4773-a850-6dcd4b0e3c0e).
We only just started, but yesterday we had an ad hoc mini event.
We did a special cache, that needs to done at two places at the same time.
One team in [Zoetermeer](https://coord.info/GCZY13) and the other in [Prague](https://coord.info/GCZXZK).

That requires a bit of organization.
My colleague Petra sent a mail to the maintainer of the cache to ask for assistance and he told her where she should leave a message.
Luckily someone in the Czech Republic reacted and a appointment was made for yesterday afternoon.
A message was posted on the page of the Dutch cache and several teams reacted.
Since this cache is not done that often, a lot of people have put this cache in their watchlist.
At 14:00 eight teams were present at the starting point, a group of about 20 people and one dog.

The cache itself was a pleasant trip through a forest of about 7km.
At every waypoint we had to SMS the answer to the Czechs and they send us the answers to the questions at their waypoints.
With that answer we could calculate our next waypoint.

The weather in Zoetermeer was nice, a bit windy but in the forest we didn't notice much of it.
In Prague the weather was not that pleasant.
It rained when they started, so we were quite pleased that some people were there, because without each other it is not possible to do the cache.

After six waypoints we arrived at the location of the cache and when you search with such a large group, the ammo can was found very fast.
Than began the big exchange.
The traded their goodies and the adults traded the travel bug and geocoin numbers.
That was a total of 21 numbers.
Pictures were taken and the cache was put back in place.

At 16:30 we returned at the cars and it was time to say goodbye.
All participants, thanks for the nice day.
