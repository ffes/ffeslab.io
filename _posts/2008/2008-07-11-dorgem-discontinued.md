---
layout: post
title: Dorgem discontinued!
date: 2008-07-11
lang: en
category: software
tags:
- dorgem
---

I needed to make a decision about my future with [Dorgem](https://dorgem.sourceforge.net/), which implies almost automatically the overall future of Dorgem, since I have always been the only developer.

In September of last year I wrote about my dilemma with Dorgem.
On January 1th, [I wrote]({% post_url 2008/2008-01-01-happy-2008 %}) that would try to make this a great year for Dorgem.
And I did try.
The first half of January I tried a few things (streaming webserver and some UI things), but nothing resulted in very useful stuff then.
And after that I never took the time to start again.

The main problem are that very much work needs to do done to turn Dorgem 3 into a real product.
And another thing is that I lost interest in the webcam thing in general.
There hasn't been any real useful development done since the release of Dorgem 3 pre-alpha 1 in May 2007 and the last release of Dorgem 2 was in May 2005.

The competition didn't sit still during the last years.
They continued to develop their product and Dorgem is way behind on them.

So my only conclusion can be that the Dorgem project is now officially discontinued.
No more new releases are to be expected and the website will not be update anymore.

Know that all the sources, both Dorgem 2 and Dorgem3, are available so if someone wants to take over, feel free to do so.
I gladly give you all the rights you need to have for the SourceForge site.
But I don't think that will happen.
During the lifetime of Dorgem a couple of people submitted patches, but a real developer never came.

I have enjoyed the period I really work on Dorgem as nice, fun and educational.
I look back at it with much satisfaction.
I want to thank everybody who has downloaded and used Dorgem, and probably still using it.
And of course I want to thank the people who actively helped the development of Dorgem.
Your patches really helped Dorgem!
