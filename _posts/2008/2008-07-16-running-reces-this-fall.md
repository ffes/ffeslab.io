---
layout: post
title: Running races this fall
date: 2008-07-16
lang: en
category: hardlopen
tags:
  - hardlopen
---

I still have to decide what races I will be running this fall.
I want to run some 10 km races.

I think I will run in [Haarlemmermeer](http://www.haarlemmermeermarathon.nl).
I ran my very first 10km there last year and some other members of [KekWek](http://www.kekwek.nl) are planning to run there this year as well.

And I also want to the Zen Run 10K (the 10km variant of the World Wide Half, an idea of [Steve Runner](http://www.steverunner.com) of the well known Phedippidations podcast), but then I would need to run a 10km in the weekend of 11 and 12 October.
I will have to search for a 10km race in the neighborhood in that weekend.

The site of the [Laan van Meerdervoortloop](http://www.lvml.nl) is down, but it seems it is being held on Sunday 23 November.

That would be three races.
One every month, so that should not be a problem.
I ran two 10km within one week this March.
But as I said, I still have to decide.
It is all subject to change.
