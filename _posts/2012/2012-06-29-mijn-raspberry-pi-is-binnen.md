---
layout: post
title: Mijn Raspberry PI is binnen
date: 2012-06-29 16:03:00 +02:00
lang: nl
tags:
- kinderen
- programmeren
- raspberry pi
---

Vandaag is eindelijk mijn <a href="http://www.raspberrypi.org/">Raspberry Pi</a> binnengekomen. Het heeft even geduurd want op de release dag, 29 februari, had ik mijn (soort van) pre-order al geplaatst.

De Raspberry Pi is een creditcard-formaat-computer die is voortgekomen uit een project om met behulp van deze zeer betaalbare computer kinderen weer geïnteresseerd te krijgen in computertechniek in het algemeen en schrijven van programma's in het bijzonder. Maar ook voor een ervaren programmeur is het erg leuk. Het board is best krachtig, zeker grafisch en gezien de prijs, al het absoluut geen volwaardige desktopvervanger. Maar er kunnen wel allerlei leuke project voor bedacht worden.

Mijn eerste plan is om te proberen om mijn oude <a href="http://en.wikipedia.org/wiki/Zire_Handheld">Palm Zire</a> als (extra) scherm aan te sluiten op dat ding. Ik weet dat er voor mijn inmiddels ter ziele gegane NSLU2 een beschrijving is hoe deze aan te sluiten, dus misschien valt het mee.

Daarna wil ik in ieder geval een tweede sd-kaart maken, zodat ik een experimenteerkaart heb en mijn huidige kaart gewoon kan gebruiken voor de NAS- en backup-functionaliteit, want de Pi is in ieder geval een mooie vervanging van mijn NSLU2.
