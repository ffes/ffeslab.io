---
layout: post
title: Weer eens een duurloop
date: 2012-10-28 19:01:00 +01:00
lang: nl
category: hardlopen
tags:
- laan van meerdervoortloop
- hardlopen
- training
---

Vanmorgen heb ik, voor het eerst sinds <a href="{% post_url 2012-09-30-lissabon/2012-09-30-pain-is-temporary-regret-is-forever %}">Lissabon</a>, weer een lange duurloop gedaan. En met de <a href="http://www.lvml.nl/">Laan van Meerdervoortloop</a> in aantocht was het wel handig om toch even te kijken waar ik sta.

Ik heb de afgelopen wel gewoon twee keer per week getraind bij <a href="http://www.kekwek.nl/">Kekwek</a>, maar van een lange duurloop was het niet meer gekomen. Dus vanmorgen lekker uitgeslapen en daarna mijn hardloopkleren aan en op weg voor een duurloop. Het plan was om het rondje Kijkduin van ongeveer 12 kilometer te doen, en dat ging eigenlijk <a href="http://connect.garmin.com/activity/237888469">best lekker</a>. De eerste kilometers iets te hard gelopen, maar over de hele afstand toch prima volgens de verwachting, zeker als je de momenten van stilstaan in verband met de verkeersdrukte niet meerekend.

Al met al kijk ik terug op een lekkere invulling van de ochtend. Ik ben benieuwd wat de Laan van Meerdervoortloop het komend weekend gaat brengen. En dat is natuurlijk niet een eenvoudige loop: ruim 5 kilometer duinen en strand. Maar de trainingen de laatste tijd gaan uitstekend, dus ik heb er wel vertrouwen in.
