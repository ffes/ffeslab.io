---
layout: post
title: Eerste trainingsweken richting Lissabon
date: 2012-06-06 08:33:00 +02:00
lang: nl
category: hardlopen
tags:
- blessure
- hardlopen
- training
- lissabon
- halve marathon
---

De knop moest om en dat heb ik gedaan. De eerste trainingsweken in aanloop naar halve marathon eind september in Lissabon heb in weer achter de rug. Maar nu doet het pijn.

Vorige week maandag was een drukke dag, maar toch moest die week die knop om. Met nog zo'n vier maanden te gaan tot de start moet ik echt gaan beginnen met het oppakken van mijn schema. Mijn schema duurt drie maanden, maar in de wetenschap dat de vakantiemaanden eraan komen, zullen er best wel een weekje zijn dat het niet lukt om optimaal te trainen. Dus wat speling inbouwen kan zeker geen kwaad. En als ik eerder klaar zou zijn is dat nooit erg.

Dus die maandag ben begonnen met de eerste week van het schema. Het begon vrij eenvoudig: 5 x 5 minuten tempo. Zoals gebruikelijk doe ik dat soort trainingen in en om het Zuiderpark. Eerst een buitenrondje met daar nog een klein stukje van het binnenrondje aan vastgeplakt om de training af te kunnen maken. Een prima start was gemaakt. Woensdag heb ik een lange duurloop gedaan, richting Kijkduin. Gewoon lekker ontspannen gelopen.

De tweede week alleen de twee trainingen bij <a href="http://www.kekwek.nl/">Kèkwèk</a> in Rijswijk en Kijkduin gedaan. Hier wel gewoon de lange tempo's kunnen doen, maar de lange duurloop schoot er bij in. Daar ga je dan al weer, met je speling je je schema.

Gisteravond liep ik in Kijkduin mee bij Kèkwèk. Daar stonden lange tempo's op het programma. Maar tijdens de tweede serie ging het mis. Een pijntje in mijn hamstring toen ik aanzette. Dus gelijk rustig aan gedaan. Bij een rustige dribbel voelde ik niets. Thuis er niet veel last meer van en na het douchen lekker gerold met <a href="http://www.thestick.eu/">The Stick</a>.

Maar vanmorgen werd ik toch wakker met een flinke last van mijn hamstring, zelfs als ik sta of zit dan voel ik het. Dus eerst maar even een paar dagen rust nemen en goed laten masseren. En dan zien we wel weer verder. De trainingen gingen allemaal goed, dus het is te hopen dat het meevalt. Afwachten is nu het enige dat ik kan.
