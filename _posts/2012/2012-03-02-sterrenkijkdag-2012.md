---
layout: post
title: Sterrenkijkdag 2012
date: 2012-03-02 23:24:00 +01:00
lang: nl
tags:
- astronomie
---

Vandaag las ik een <a href="https://twitter.com/astro_andre/status/175479789149368320">tweet</a> van <a href="http://nl.wikipedia.org/wiki/Andr%C3%A9_Kuipers_%28ruimtevaarder%29">André Kuipers</a>. Naar aanleiding van deze tweet ben ik vanavond eens langs geweest bij de <a href="http://www.sterrenwachtrijswijk.nl/">Sterrenwacht Rijswijk</a> voor Nationale Sterrenkijkdag. Aangezien Menno weg was, ben ik alleen richting Rijswijk gereden.

De Sterrenwacht Rijswijk bevindt zich bovenop de flats van Petronella Voûtestraat en bestond kortgeleden 40 jaar. Als we richting de snelweg rijden zie je deze flats, met afbeeldingen van diverse planeten, liggen. Maar ik was er nog nooit binnen geweest. Sterker nog, ik had nog nooit door een telescoop gekeken.

Om naar de sterren en planeten te kunnen kijken moet het weer wel een beetje meezitten. De hele dag was het bewolkt maar gelukkig trok het open. Het was wel een beetje nevelig, maar aan de hemel waren de maan, Jupiter en Venus duidelijk te zien.

Vanaf 20:00 uur was de sterrenwacht open en rond die tijd was ik er ook. Het was inmiddels al aardig druk in de niet al te grote ruimte. Eerst ging ik naar het dakterras, waar diverse kleinere kijkers stonden opgesteld.

Ook op het dakterras was het vrij druk, maar bij een kijker die op de <a href="http://nl.wikipedia.org/wiki/Maan">maan</a> was gericht viel het nog mee. Een gezin stond hier te kijken, dus ik was vrij snel aan de beurt. Het was mooi om zo'n helder en duidelijk beeld met de grens tussen licht en donker op de maan te kunnen zien. Het was ook opvallend om te zien dat zo'n telescoop het beeld omdraait. Je ziet het lichte en donkere deel precies andersom door de telescoop als wanneer je gewoon naar boven kijkt.

Daarna ben ik in de rij gaan staan voor een andere kijker die op <a href="http://nl.wikipedia.org/wiki/Jupiter_%28planeet%29">Jupiter</a> was gericht. Zelf vond ik het een flinke kijker, maar volgens de eigenaar was dit maar een kleintje. Het was mooi om rondom Jupiter ook twee van de manen van Jupiter te kunnen zien. De rij was nog langer geworden, want het was inmiddels flink druk op het dak. Nog even bij een telescoop staan kijken waarop een camera was aangesloten. Hierdoor kon je met meerdere mensen tegelijk naar de kraters van de maan kijken.

Vervolgens achter aangesloten bij de rij om de koepel in te kunnen.Dat ging vrij vlot, en boven in de koepel was een grotere telescoop te vinden. Ik had gedacht dat deze wel iets groter zou zijn. Deze was korter en breder en daardoor vangt hij meer licht op en dat levert blijkbaar betere resultaten op. Ook deze telescoop was op Jupiter gericht. Alleen begon de bewolking inmiddels wel wat dikker te worden. Daardoor werd het steeds lastiger om wat te zien en zoveel heb ik dus niet kunnen zien. Ik was blij dat ik door die "kleine" telescoop de manen al gezien had.

Gelukkig hebben ze bij de sterrenwacht twee keer per maand een publieksavond, dus ga ik zeker op een heldere avond nog een keer met Menno langs. Dan zal het daar een stuk rustiger zijn en hopelijk krijgen we dan meer tijd en kunnen we meer zien.
