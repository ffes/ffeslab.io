---
layout: post
title: Halve Marathon Lissabon
date: 2012-03-12 19:07:00 +01:00
lang: nl
category: hardlopen
tags:
- hardlopen
- lissabon
- halve marathon
---

Ik heb me opgegeven om met <a href="http://www.kekwek.nl/">Kèkwèk</a> mee te gaan naar de <a href="http://www.portugal-half-marathon.net/">halve marathon in Lissabon</a> het komende najaar.

Al jaren wordt er op de club gesproken over weer eens een gezamenlijke deelname aan een mooie hardloopwedstrijd ergens in het buitenland. In het verleden zijn er groepsreizen georganiseerd naar Boedapest, Parijs en Glasgow. Maar de laatste jaren was dat er niet meer van gekomen. Ik was dus nog nooit meegegaan met zo'n trip. Dit jaar hebben Willem en Patty de stoute schoenen weer aangetrokken en ze hebben een plan gemaakt om met een delegatie van Kèkwèk naar de halve marathon in Lissabon te gaan.

Na wat getwijfeld te hebben, heb ik de knoop doorgehakt en me ingeschreven voor dit sportieve weekend in de Portugese hoofdstad.

Lissabon is wel een plaats met een verhaal. Op <a href="http://www.smitsvanburgst.nl/">mijn werk</a> werd eind jaren negentig enkele jaren hard gewerkt aan het advies van de installaties in de hallen voor de wereldtentoonstelling, de <a href="http://nl.wikipedia.org/wiki/Expo_%2798">Expo '98</a>. Maar omdat Caroline toen zwanger was van Menno konden wij niet mee met het personeelsuitje naar Lissabon.

Ik heb eens gekeken naar het parcours van deze halve marathon. Het is best handig dat mensen dit tegenwoordig op sites als <a href="http://runkeeper.com/race/meia-maratona-do-centenrio-647/11863">RunKeeper</a> en <a href="http://connect.garmin.com/activity/117344693">Garmin</a> <a href="http://connect.garmin.com/activity/116891653">Connect</a> zetten. Dan kun je enigszins inschatten wat je te wachten staat. En redelijk vlak parcours, de start is op de <a href="http://nl.wikipedia.org/wiki/Vasco_da_Gamabrug">Vasco da Gamabrug</a>, dan heen en weer langs de kust om te finishen bij diezelfde Expo-gebouwen. En met een gemiddelde temperatuur van <a href="http://www.take-a-trip.eu/nl/lissabon/klimaat/weer/september/9/%20">22 °C</a> in die periode is het natuurlijk wel lekker lopen.

Dus binnenkort zal ik langzaamaan de trainingsintensiteit weer gaan opvoeren en ervoor zorgen dat ik er op tijd klaar voor ben. Vooral tijdens de zomer(vakantie)periode zal het nog een uitdaging worden om de benodigde trainingsdiscipline vast te houden.
