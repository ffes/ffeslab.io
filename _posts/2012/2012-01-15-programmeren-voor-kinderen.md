---
layout: post
title: Programmeren voor kinderen
date: 2012-01-15 22:03:00 +01:00
lang: nl
tags:
  - kinderen
  - programmeren
comments:
  - comment:
    author: Stefan Boeykens
    date: 2013-03-03 12:08:53 +01:00
    text:
      - Ben net hetzelfde aan het zoeken
      - http://www.pepermunt.net/leren-online/leren-programmeren.html
      - Heb zelf ervaring met Processing, Unity, VBA, C++ en zo voort, maar voor kinderen mag het wel iets lichter en speelser zijn, denk ik.
      - Scratch en Alice lijken wel de moeite.
---

Aangezien ik programmeur ben, vraagt Menno regelmatig wat ik op mijn werk doe en of hij ook kan leren programmeren. Dus heb ik vanavond gekeken of er sites te vinden zijn waar kinderen op redelijk eenvoudig wijze de basisbeginselen van het programmeren kunnen leren. Dit blijkt nog niet zo eenvoudig te zijn, zeker niet in het Nederlands.

Mijn eerste ervaringen met computers stammen nog uit de tijd van <a href="http://nl.wikipedia.org/wiki/Homecomputer">homecomputers</a>. In mijn geval eerste de <a href="http://nl.wikipedia.org/wiki/Atari_XL">Atari 800XL</a> en later een <a href="http://nl.wikipedia.org/wiki/MSX">Philips MSX1</a>. En destijds was het vrij vanzelfsprekend om ook te gaan programmeren op zo'n apparaat. De apparaten startten immers allemaal in de <a href="http://nl.wikipedia.org/wiki/BASIC">BASIC</a> <a href="http://nl.wikipedia.org/wiki/Interpreter">interpreter</a> op. Dus ging ik bij de bibliotheek boeken over de computer halen en dan aan de slag met dat ding. Zelf de BASIC-code van een spel overtypen en stukje bij beetje leren begrijpen wat het allemaal deed. En dan later die spelletjes uitbreiden en verbeteren.

Tegenwoordig is dat toch anders. Kinderen kunnen over het algemeen prima met de computer overweg. Ze leren op de basisschool al om te gaan met de diverse Office-toepassingen zoals Word en PowerPoint. Dit is natuurlijk erg zinvol. De kinderen van tegenwoordig hebben deze basiskennis hard nodig, maar daar blijft het dan vaak bij. Maar er zit een groot gat tussen het omgaan met een computer en het programmeren ervan.

Het is wel opvallend dat Menno kortgeleden met deze vraag kwam en dat ik al enige tijd de ontwikkelingen rondom de <a href="http://www.raspberrypi.org/">Raspberry Pi</a> volg. Dit is een volledige werkende Linux computer ter grootte van een bankpas die slechts $25 gaat kosten. Dit boardje, want meer is het niet, is bedoeld om kinderen eenvoudig toegang te geven tot een computer waar ze op kunnen stoeien en programmeren zonder de "gewone" computer te slopen.

Het lijkt Menno dus leuk om ook iets te kunnen programmeren. Maar waar begin je dan? Hoe leer je een leek de beginselen van het programmeren? Je probeert eerst diverse zoekopdrachten. Maar dat levert niet zoveel bruikbaars op. Daarna op zolder gezocht of er nog oude (school)boeken waren, maar ook dat was niet het geval. Wat nu? In het Engels is natuurlijk van alles het te vinden, maar voor kinderen is dat toch een obstakel. Misschien iets vertalen?

Ik weet nog niet precies hoe ik Menno ga leren programmeren, maar ik zal me er binnenkort toch in gaan verdiepen. Want die vraag komt te regelmatig terug om er niets mee te doen. En waarom zou ik ook! Ik kan programmeren en vind het zelf ook nog steeds leuk. Dus overdragen die kennis! Misschien treedt hij wel in mijn voetsporen met een baan in de IT.
