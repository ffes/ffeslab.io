---
layout: post
title: Tweede addertje gevonden bij Simyo
date: 2012-09-24 15:35:00 +02:00
lang: nl
---

Eerder schreef ik al over <a href="{% post_url 2012-02-14-simyo-het-addertje/2012-02-14-simyo-mobiel-internet-het-addertje %}">het addertje onder het gras</a> bij <a href="http://www.simyo.nl/">Simyo</a>. Vandaag liepen we tegen een tweede addertje aan, waardoor _hun_ vertraging _ons_ 8 euro heeft gekost.

Het eerste addertje is samengevat het feit dat als je geen beltegoed meer hebt, je ook geen gebruik meer mag maken van je aangeschafte internetbundel, ook al heb je daar nog ruim voldoende MB's over. Dus wel betalen voor iets dat je niet kunt gebruiken. Opwaarderen is de enige optie.

Vandaag blijkt dat de verwerking van je verbruiksgegevens soms pas na een aantal dagen verwerkt worden. Bij een negatief beltegoed, ontstaan door teveel internetgebruik, kun je soms nog een dag door internetten. Deze MB's worden dan gewoon voor € 1,- per MB afgerekend en loopt je tekort alleen maar verder op.

Uit de verbruikspagina van Mijn Simyo blijkt dat hij op zaterdag al een negatief beltegoed bereikt moet hebben van ongeveer € -3. Maar deze gegevens werden niet verwerkt dus zondagmiddag/avond bij oma nog zo'n 8 MB verbruikt en zo kom je op bijna € -11.

Telefonisch klagen bij Simyo helpt uiteraard niet: "Het is verbruikt. Erg vervelend dat het niet op tijd verwerkt is, maar daar kunnen wij uiteraard niets aan doen". Dat zij hun gegevens zo traag verwerken, is dus ineens mijn probleem geworden! Een schadepost van 8 euro. En nee, dat is geen schokkend bedrag en we eten er geen boterham minder door, maar het gaat om het principe! Ik ga in ieder geval een klacht indienen, al heb ik er geen vertrouwen in dat zij er iets mee gaan doen.

Overstappen zul je dan zeggen. Maar overstappen is vrij lastig. Simyo is een van de weinige die een prepaid internetbundel aanbieden voor een min of meer acceptabel bedrag. En aangezien het voor onze puber is, is een post-paid abonnement zeker geen optie. Dan had het deze maand dus helemaal uit de hand gelopen.

Neem bijvoorbeeld <a href="http://www.hollandsnieuwe.nl/">Hollandse Nieuwe</a>. Die heeft geen internetbundel voor prepaid-bellers. De 275 min/sms/MB sim-only variant lijkt aardig, maar die rond zijn internetverbruik zodanig af (per 100kb) dat hun eigen medewerkers mij recent afraadden om naar ze over te stappen. Wel heel eerlijk. Daarnaast hebben zij geen maximalisering van de kosten en je zit er voor langere tijd (minimaal 1 jaar) aan vast.

<a href="http://www.simpel.nl/">Simpel</a> heeft als enige een instelbaar belplafond en is goed betaalbaar. Maar het grootste probleem van Simpel is dat je gelijk een abonnement voor 2 jaar moet afsluiten. Als je tegen vergelijkbare addertjes aanloopt, zit je er toch 2 jaar aan vast! Dus Simpel laten we ook maar even voor wat het is.

<a href="http://www.ben.nl/prepaid">Ben Prepaid</a> lijkt misschien nog een optie. Die hebben ook een maandbundel voor internet en die oogt interessant, al zijn de bel- en sms-kosten wel hoger dan bij Simyo. Maar ja, wie belt en smst er nu nog? Nog maar eens verder onderzoeken.

Wie weet is er nog hoop om eindelijk weg te kunnen bij Simyo, want een tevreden klant wil ik mezelf niet noemen!
