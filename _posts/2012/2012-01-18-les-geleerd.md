---
layout: post
title: Les geleerd
date: 2012-01-18 22:32:00 01:00
lang: nl
category: hardlopen
tags:
  - hardlopen
  - training
comments:
  - comment:
    author: TeleRoel
    date: 2012-01-19 09:34:08 +01:00
    text:
      - Je moet er de mogelijkheid voor hebben, maar voor mij werkt dit dan goed. Op de heenweg zoveel mogelijk rechtuit tot halverwege (zeg 5 km), dan omkeren en dezelfde route terug. Gaat het lekker, dan verleng je gewoon de heenweg! Zo niet, dan keer je eerder om. Ik kan dat altijd prima doen via de watertoren en dan richting Wassenaar. Succes!
  - comment:
    author: Frank Fesevur
    date: 2012-01-19 09:52:33 +01:00
    text:
      - Zoiets doe ik ook meestal als ik richting de duinen ga. Maar dan moet ik er wel heengaan [;-)]
---

Vanavond heb ik getraind in het Zuiderpark. Maar het ging toch niet helemaal zoals ik thuis nog van plan was.

Thuis was het plan om een lange duurloop te gaan doen van ongeveer 10 kilometer. Maar bij het Zuiderpark nam ik het besluit om toch in het Zuiderpark te gaan lopen en dan zou ik wel 4 binnenrondjes lopen. Maar ik trap er iedere keer weer in. Die binnenrondjes werken voor mij totaal niet voor een lange duurloop. Ik moet dan gewoon een route gaan lopen. Om het Zuiderpark heen, door naar de Lozerlaan en dan weer naar huis. Of richting de duinen, of... Er is genoeg keuze, maar niet **in** het Zuiderpark.

(Hopelijk) heb ik nu echt die les geleerd. We zullen het zien...
