---
layout: post
title: Perseïden kijken
date: 2012-08-13 17:14:00 +02:00
lang: nl
tags:
- astronomie
---

Afgelopen nachten ging de aarde weer door de <a href="http://nl.wikipedia.org/wiki/Perse%C3%AFden">Perseïden</a> heen, en dus waren er weer veel vallende sterren te zien.

Vanuit de tuin van mijn broer was het prima sterren kijken, zo aan de rand van Alphen aan den Rijn. Echt heel veel spectaculaire strepen leverde dat zaterdagnacht niet op, maar het blijft toch een mooi schouwspel.

Maar het kijken bracht ook een mooie vakantieherinnering terug. Begin jaren negentig was ik op een georganiseerde actieve vakantie in de Pyreneeën. Rijden in een Land Rover, abseilen door een canyon en ook een tweedaagse bergwandeling naar een hooggelegen hut. Daar sliepen we 's nachts op de berg in de buitenlucht. Met de hele groep lagen we naar de donkere heldere sterrenhemel te kijken. Ik realiseerde het me zaterdagnacht pas, maar die nacht moet precies tijdens diezelfde jaarlijkse passage van de Perseïden zijn geweest, want in een rap tempo zagen we de ene na de andere vallende ster verschijnen. Net als de bijbehorende ene na de andere oooh of aaah. De volgende dag op de weg weer naar beneden waren de vallende sterren nog regelmatig het onderwerp van gesprek. Een moment om niet snel te vergeten.
Zaterdagnacht was het niet zo spectaculair, het was daar ook niet zo donker en het was ook niet het hoogtepunt van de passage, maar toch was het mooi om weer even terug te denken aan die nacht in de Spaanse Pyreneeën.
