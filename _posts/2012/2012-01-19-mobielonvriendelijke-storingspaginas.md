---
layout: post
title: Mobiel(on)vriendelijke storingspagina's
date: 2012-01-19 21:16:00 +01:00
lang: nl
tags:
- websites
- den haag
- smartphone
---

Vandaag hadden wij op kantoor een storing aan onze internetverbinding, "het internet deed het niet". Op zich niet zo'n probleem, dat kan een keer gebeuren. Maar probeer maar eens om dat soort storingsinformatie met behulp van je smartphone boven tafel te krijgen.

Als je zo'n storing hebt, ligt je vaste internet er dus uit en ga je met je mobiele telefoon proberen om de storingspagina van je provider te vinden. Gelukkig had ik al eens een snelkoppeling gemaakt en kon ik <a href="http://www.kpn.com/zakelijkstoringen">dat adres</a> gelijk intikken op. Maar dan blijkt dat die pagina niet op mijn HTC Desire niet werkt. Ik krijg de melding "Unsupported browser". Geen vast internet en op je mobiel geen informatie te vinden. Dan zit er niets anders op om te gaan bellen met de helpdesk en daar sta je even in de wacht want ze hebben het druk met vertellen dat er inderdaad een storing is.

Toen de storing voorbij was, in een chat aan een medewerker van KPN de vraag gesteld of er ook een mobielvriendelijke versie van die storingspagina is. En inderdaad, die is er niet. Deze medewerker was er ook verbaasd over. Hij zou het in de ideeënbus stoppen. Ben benieuwd of het er van gaat komen.

Maar KPN is niet de enige met dit probleem. Ook Ziggo heeft geen mobielvriendelijke <a href="https://www.ziggo.nl/help/storingen/">storingspagina</a>. Je krijgt dezelfde "Unsupported browser" melding. Een vraag via het contactformulier werd vrij vlot beantwoord met een standaard-reply (!) dat zij geen mobiele website hebben en dat ik maar de gewone website moet bekijken. En ze zeggen er gelijk bij dat het niet werkt door het gebruik van Flash. Lekker klantvriendelijk. Wel een mail terug, maar geen antwoord op mijn vraag!

En wat als de stroom uitvalt? Kan ik die informatie snel vinden met mijn mobiel? Ik weet dat <a href="http://www.gasenstroomstoringen.nl/">gasenstroomstoringen.nl</a> bestaat. Die site werkt ook al niet best. Van onze netbeheerder kunnen ze geen informatie tonen en je wordt dan doorverwezen naar <a href="http://www.stedin.net/">stedin.net</a>, zoek het dus zelf maar uit. Ook dit is niet echt de meest handig site om te bekijken met een mobiele telefoon, al moet ik zeggen dat ik daar tenminste nog de actuele storingen kon zien. Dat ging bij de telecomaanbieders helemaal niet.

Een snelle rondgang langs wat andere providers leert dat bij T-Mobile en Vodafone een storingspagina niet eens te vinden is. De storingspagina's van <a href="http://www.upc.nl/klantenservice/meldingen_storingen/">UPC</a> en <a href="http://www.caiway.nl/site/nl/klantenservice/klantenservice/informatie/storingenenonderhoud">Caiway</a> werken niet super, maar het lukt wel om actuele informatie te vinden.

Toch vraag ik me af waarom die grote bedrijven (meestal) wel de moeite nemen om een storingspagina te maken, maar hem dan vervolgens zo bouwen dat als je echt nodig hebt deze pagina moeilijk tot simpelweg onbruikbaar is?

En wat als we <a href="http://www.crisis.nl/">crisis.nl</a> echt een keer nodig hebben? Ook de site van de rampenzender in onze regio <a href="http://www.westonline.nl/">Radio West</a> is niet echt mobielvriendelijk, net zo min als die van de gemeente <a href="http://www.denhaag.nl/">Den Haag</a>. Dan is het mooi om te zien dat <a href="http://m.zoetermeer.nl/">Zoetermeer</a> wel een mobiele site heeft, inclusief een nieuwssectie. Het kan dus wel!
