---
layout: post
title: '"Goede voornemens"'
date: 2012-01-03 21:30:00 +01:00
lang: nl
category: hardlopen
tags:
  - hardlopen
  - training
comments:
  - comment:
    author: TeleRoel
    date: 2012-01-04 13:07:14 +01:00
    text:
      - Mijn goede voornemen voor een nieuw jaar is rond 12:05 op nieuwjaarsdag uitgevoerd als ik de Noordzee weer uitstap na de nieuwjaarsduik!
      - Daarnaast is de CPC al geboekt, zo'n thuiswedstrijd kan ik niet laten schieten.
      - Succes met je plannen Frank.
---

De titel pretendeert dat ik voor 2012 echte goede voornemens heb, maar dat is niet zo. Toch zijn er wel wat dingen die ik voor de komende maanden van plan ben, met name op hardloopgebied.

Ik zal deze week een besluit moeten nemen over wat voor wedstrijden ik in het voorjaar wil gaan lopen. Wordt het twee keer een 10 kilometer bij de <a href="http://www.20vanalphen.nl/">20 van Alphen</a> en bij de <a href="http://http//www.cpcloopdenhaag.nl/">City-Pier-City</a>. Of ga ik toch nog voor een halve marathon bij de CPC? Dat laatste zal nog een flinke inspanning vereisen, want na de <a href="http://www.zevenheuvelenloop.nl/">Zevenheuvelenloop</a> van afgelopen november heb ik niet verder gelopen dan ongeveer 10 kilometer. Die 11 kilometer extra is dan best veel in 2 maanden.

Ik ga op 22 januari twee rondjes gaan lopen bij de <a href="http://www.puinduinrun.nl/">Puinduinrun</a>. Een flinke uitdaging, want de puinduinen in Kijkduin zijn best hoog en vooral de trappen zijn lang en slopend. En alle lange trappen zitten in dat parcours. Daarnaast loop ik op 12 februari als businessloper bij de <a href="http://www.noord-aa-polderloop.nl/">Noord-Aa Polderloop</a> de 5 kilometer.

Al met al genoeg plannen voor 2012. Zoals gezegd zijn het geen "goede voornemens", het zijn gewoon redelijk concrete plannen. Dat hele gedoe over goede voornemens is niets voor mij. Te vrijblijvend! En waarom zo nodig op 1 januari? Als je op 25 september niet kunt beginnen met wat kilo's kwijt te raken of te stoppen met roken, waarom dan wel aan het begin van een nieuw jaar. Het is niet afhankelijk van een tijdstip, maar van je eigen wil om iets voor elkaar te krijgen.
