---
layout: post
title: Trainingsweken
date: 2012-08-11 12:56:00 +02:00
lang: nl
category: hardlopen
tags:
- hardlopen
- training
- lissabon
- halve marathon
---

De afgelopen weken wat minder geblogd, maar wel gewoon doorgetraind in voorbereiding op de halve marathon in Lissabon.

De afgelopen weken waren prima weken. Lekker gelopen, en voldoende kilometers gemaakt. 30 kilometer voor de week en ruim 13,5 kilometer voor de lange duurloop. Dat gaat de goede kant op. En nog belangrijker is dat het gevoel over het algemeen beter wordt.

Al was dat laatste vandaag weer heel tegenstrijdig. De hele training van <a href="http://www.kekwek.nl/">Kèkwèk</a> het idee gehad dat het niet echt lekker ging, maar uit de resultaten blijkt dat mijn gevoel niet klopte met de werkelijkheid. We liepen een 1000m en die liep in netjes in 4:30. Een prima tijd waar niets mis mee is, voor mij. Ook de andere oefeningen liep ik gewoon op "mijn plek" in de groep.

De duurloop van afgelopen donderdag ging erg lekker en daarbij was het gevoel ook goed. Afgezien van twee of drie keer wat oponthoud door het verkeer, kon ik iedere kilometer tussen de 6 minuten en de 6:15 houden. De route die ik sinds een tijdje loop is best leuk en kan ik ook flink uitdagend maken. Vanaf huis loop in richting de De Savornin Lohmanlaan en dan ga ik daar de duinen in richting Kijkduin. Die duinen zijn flink hoog, en als ik na Kijkduin het nieuwe fietspad richting de zandmotor neem moet je ook een flinke klim maken. Dat maakt deze route leuk (Kijkduin, de zee en de duinen) en uitdagend tegelijk. En de route is eenvoudig te verlengen, maar ik hoef gelukkig niet tot Hoek van Holland door te gaan.

Ik heb het gevoel dat het allemaal wel zal gaan lukken in Lissabon, maar ik ben er nog niet. Vooral de duurloop moet langer en de intervallen blijf ik er uiteraard gewoon bij doen.
