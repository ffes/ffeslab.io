---
layout: post
title: NSLU2 kapot, wat nu?
date: 2012-04-17 12:50:00 +02:00
lang: nl
comments:
  - comment:
    author: Marco vd A
    date: 2012-04-17 14:59:38 +02:00
    text:
      - Is dat Murphey of niet... Ik geloof niet in toeval, dus er moet iets zijn waardoor jij dit hebt en ik ook de afgelopen week. Ik had ook ongeveer zo iets...
      - Tot vorige week natuurlijk. Toen gaf hij een piep... De disk was weer niet goed. Ik moest hem verwijderen. Die met dat rode lampje. Ik trek de disk er uit en neem hem mee naar beneden en besluit dan toch maar een nieuwe te gaan kopen. Omdat ik de prijs wil overleggen met mijn vrouw, wacht ik tot ze thuis komt. In de tussentijd kijk ik nog een filmpje die op die disk staat en halverwege zegt hij ineens... connection lost... Huh... Om een lang verhaal korter te maken... Disk 2 had het begeven. Dan is de raid dus ineens helemaal kapot en niet meer te repareren. Oftewel... De veilige situatie is ineens kapot en onherstelbaar.
      - "Voor de liefhebber... Raid dot: 1 + 1 + 1 = 3. Op 3 disken zet hij 1 op de vierde disk de cheksum (in dit voorbeeld 3)"
      - "Gaat er 1 kapot: 1 + 1 + x = 3, dan kan hij berekenen wat de x moet zijn..."
      - "Gaan er dan ineens 2 kapot: 1 + x + x = 3, dan is er paniek... dan zijn er meerdere mogelijkheden en raakt hij corrupt..."
      - Disken zijn tegenwoordig weer aan de prijs. In plaats van de 2 disken van 750GB in de 1,5TB raid te zetten, kun je beter heel dat kabinet met disken maar weg doen en twee gewone disken kopen van 2TB en die af en toe syncen. Is wat meer werk, maar toch is het blijkbaar net zo veilig. Gaat er dan 1 (de helft van de totale disken) kapot, val je terug naar de laatste sync en niet naar alles kwijt... Goede les...
      - Gelukkig stond er alleen maar spul op dat 'gemist' kon worden en had ik zelfs een lijst met wat er op stond bijgehouden en kan ik van diverse andere lokaties alles weer terug vinden, dus echt heel erg gedupeerd ben ik niet. Ik ben een goede systeembeheerder en niks staat op 1 enkele plek [;-)] Maar schrikken is het wel...
      - Ik hoop dat jij ook met de schrik vrij komt [;-)]
  - comment:
    author: Frank Fesevur
    date: 2012-04-17 15:56:53 +02:00
    text:
      - Bij mij is er gelukkig niets aan de hand met de datadisk met de opslag. Dat RAID-probleem heb ik gelukkig nog niet hoeven ervaren, maar ik snap precies wat je bedoelt. Gelukkig gebruiken wij op kantoor op servers altijd RAID en hebben we garantie op die apparaten.
      - Maar zoals jij ongetwijfeld weet, is RAID geen backup maar een hardware-failure vangnet, en als inderdaad twee disks tegelijk kapot gaan is het &quot;jammer, maar helaas&quot;.
---

Van de week was ineens mijn trouwe NSLU2 (mijn server/nas) niet meer bereikbaar. Hij had zichzelf uitgeschakeld. Toen ik hem weer opstartte bleef hij zichzelf continu opnieuw opstarten. Wat was er aan de hand?

Dus eerst de USB-stick met het <a href="http://www.debian.org/ports/arm/">Debian</a>-OS gecontroleerd. Op het eerste gezicht leek die stick nog prima te functioneren. Nog enkele pogingen gedaan om hem te laten repareren, maar dat hielp allemaal niet. Hij bleef hetzelfde doen. Continu opnieuw opstarten. De externe harddisk die voor de dataopslag wordt gebruikt was nog gewoon benaderbaar. Klein praktisch puntje is dat deze, omdat hij aan een Linux-systeem hangt, als ext3 is geformatteerd en dus niet rechtstreeks door een Windows-pc gelezen kan worden.

Bij <a href="http://www.mycom.nl/">MyCom</a> voor niet al te veel een nieuwe USB-stick gekocht. Deze opnieuw gepartitioneerd en geformatteerd en vervolgens opnieuw Debian erop geïnstalleerd. Gelukkig startte mijn NSLU2 weer op. Was blijkbaar toch die oude stick niet helemaal goed meer.

Met de bestanden die ik van de oude USB-stick had geback-upt aan de slag om weer een werkend systeem te maken. Diverse instellingen goed gezet, packages geïnstalleerd en geconfigureerd. En ineens was mijn NSLU2 weer uit. Het zou toch niet...

Het kastje weer aangezet en mijn vrees bleek de waarheid. Exact hetzelfde probleem als met de originele USB-stick: continu opnieuw opstarten. Ik kan eigenlijk niet anders concluderen dan dat mijn servertje na jaren trouwe dienst de geest heeft gegeven. Dat is balen!

Na deze "klap" verwerkt te hebben is het belangrijk om weer verder kijken. Wat nu? Het was toch wel heel handig om zo'n server thuis te hebben. Een centrale netwerkschijf, het laten uitvoeren van een aantal eenvoudige taken. Hij deed het allemaal. Even naar de winkel en de eerste de beste NAS uit het schap pakken gaat hem dus niet worden.

Een nieuwe NAS moet een aantal extra zaken kunnen. Een belangrijke eis voor mij is wel dat er een redelijk volledige Linux op moet kunnen draaien. Een andere eis is een laag stroomverbruik. Verder zou een alles-in-een kast prettig zijn en zijn multi-media capaciteiten mooi meegenomen.

De <a href="http://www.raspberrypi.org/">Raspberry PI</a> zou zo maar de perfecte oplossing kunnen zijn. Hij is zeer betaalbaar en voldoet aan bijna alle eisen. Alleen niet aan de alles-in-een-kast, maar dat had ik met de NSLU2 ook niet. Maar de PI is nog steeds niet goed leverbaar. Nog even geduld dus, ik sta op de wachtlijst.

Daarom doen we het voorlopig maar even zonder server. Moet kunnen, toch?
