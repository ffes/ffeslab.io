---
layout: post
title: Trainingsweek
date: 2012-04-08 14:18:00 +02:00
lang: nl
category: hardlopen
tags:
- hardlopen
- training
---

Deze week heb ik weer hardgelopen. Doordat ik me vorige week ziek voelde, was dat inmiddels al weer bijna twee weken geleden. En dat merk je dan ook.

Op woensdag stond het standaardrondje richting Kijkduin op de planning. Aangezien ik van te voren al had ingeschat dat twee weken niets doen zijn tol zou gaan eisen ging ik er vanuit iets vaker te moeten wandelen. Vanaf huis ging ik richting de <a href="http://www.egelopvangdenhaag.nl/">Egelopvang</a>. Daar was een grote groep, zo'n 35 personen, van <a href="http://www.haagatletiek.nl/">Haag Atletiek</a> net begonnen met de training. Ik ging na de egelopvang rechtsaf het steile zandpad op richting zee. Wat een vervelende klim blijft dat toch! Daarna nog een stukje richting de strandjutterskeet opgelopen met een andere groep van Haag. Ik ben er geloof ik 6 groepen tegengekomen.

Vanaf Kijkduin ben ik weer rustig terug naar huis gelopen. Ik moest inderdaad wel af en toe wandelen, maar dat was verwacht. Dus al met al met een tevreden gevoel thuisgekomen.

En zaterdag was een gewone training bij Kèkwèk. Voornamelijk intervallen en wat duurwerk. Twee weken geleden had ik aan <a href="http://www.spier.nu/">Jan</a> gevraagd of hij voor mij een "<a href="http://www.thestick.eu/">Stick</a>" wilde meenemen. Dat is soort stok om zelf je (been)spieren te masseren. Na het douchen heb ik hem gelijk even geprobeerd en het voelt goed. Nu even afwachten of dit gaat helpen om iets minder snel last van mijn spieren te hebben.
