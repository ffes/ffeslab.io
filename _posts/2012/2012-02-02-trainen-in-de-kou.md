---
layout: post
title: Trainen in de kou
date: 2012-02-02 12:49:00 +01:00
lang: nl
category: hardlopen
tags:
- cpc
- den haag
- hardlopen
- training
---

Gisteren in de kou heerlijk gelopen. Ja, het was koud, maar met verschillende laagjes en handschoenen aan en een muts op, was het prima te doen.

Het was toch even over een drempeltje stappen (zowel letterlijk als figuurlijk) om met -5 °C naar buiten te gaan om te gaan hardlopen. Dan komt het erop aan om je kleding goed te kiezen. Eerst een shirt met korte mouwen, daar overheen een shirt met lange mouwen en dan het dikste van mijn twee hardloopjasjes aan. Het reflecterende hesje, de handschoenen en de muts maakt het compleet. Het bleek een goede keuze.

Tijdens het inlopen richting het Zuiderpark werd het al snel duidelijk dat het buitenrondje doorlopen tot aan de Lozerlaan niet het meest handige zou zijn. Dan moest ik vanaf die Lozerlaan tot aan huis continu tegen de ijzige wind in en met een gevoelstemperatuur van nog eens zo'n 10 graden lager was dat geen fijn vooruitzicht. Daarom was twee keer een buitenrondje een meer voor de hand liggende keuze. Dan maar iets korter trainen, maar met deze kou wilde ik geen rare dingen doen.

Tijdens het rekken en strekken bij de ingang van het Zuiderpark kwam ik Eveline tegen, die met klaar was met haar <a href="https://twitter.com/EvelinedeJagher/status/164757461629276161">intervaltraining</a>. Ook zij gaat, samen met <a href="http://mooisenmeer.blogspot.com/">Suzanne</a>, de 10 km bij de <a href="http://www.cpcloopdenhaag.nl/">CPC</a> doen dus we zijn allemaal lekker bezig.

Het eerste buitenrondje bleek dat ik de juiste keuze had gemaakt. De stukken met tegenwind waren prima te doen en de stukken met de wind in de rug had ik het gewoon warm. Het was rustiger met lopers dan anders op dit tijdstip. Blijkbaar waren zij wel bang voor de kou. Bij het ingaan van de tweede ronde zag ik dat ik het tempo goed kon vasthouden. De 5 km gingen ruim binnen het half uur en ik besloot om het rondje iets te verlangen tot aan de Leyweg en dan naar huis.

Hierdoor kwam mijn uiteindelijk afstand uit op 9.25 km en dat allemaal in 54:44 minuten. Ik was <a href="https://twitter.com/FraCaMen/status/164795415441571841">supertevreden</a>! Heerlijk gelopen, niet te diep gegaan, warm genoeg gebleven. Kortom een geslaagde training. Dit geeft de burger moed!
