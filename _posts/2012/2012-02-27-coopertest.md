---
layout: post
title: Coopertest
date: 2012-02-27 20:21:00 +01:00
lang: nl
category: hardlopen
tags:
- wedstrijd
- hardlopen
- coopertest
---

Afgelopen zaterdag liepen we&nbsp;bij <a href="http://www.kekwek.nl/">Kèkwèk</a> onze "jaarlijkse" <a href="http://nl.wikipedia.org/wiki/Coopertest">coopertest</a>.

Op onregelmatige basis lopen wij bij Kèkwèk een coopertest op de wielerbaan van <a href="http://www.despartaan.nl/">De Spartaan</a>. Eén rondje op die baan is exact 500 meter. Normaal gesproken doen we wel één keer per jaar een coopertest, maar nu was het al weer anderhalf jaar geleden. Op school en tijdens mijn handbalperiode liepen we ook wel eens een coopertest, en dan was dat mijn grootste nachtmerrie. Tegenwoordig is dat anders. Even 12 minuutjes je stinkende best doen en klaar. Ik keek er naar uit. En het is een mooi meetpunt.

Een momentopname is het ook, maar het geeft toch wel een beeld. De vorige coopertest, in oktober 2010, was ik absoluut niet in vorm. Ik liep toen alleen op zaterdag bij de club en trainde verder niet doordeweeks en liep daardoor maar 2336 meter. Niet onverwacht, maar het was mijn slechtste coopertest sinds ik was begonnen met hardlopen.

Zaterdag had ik hogere verwachtingen. Na mijn prima tijd in Zoetermeer en de gedane training van de afgelopen maanden wilde ik proberen nu toch eindelijk een keer die 2500 meter te halen. Dat betekent dus een gemiddelde snelheid van 12,5 km/u oftewel 4,8 min/km. En daarvoor moet ik toch stevig aan de bak.

Kort na de start liepen we al snel in de te verwachte volgorde en ik wist dat er niemand was om samen mee te lopen en dus je aan elkaar op te trekken. Vrij snel kwam Patty me nog voorbij, ook dat kwam niet als een verrassing. Ze bleef de hele tijd een mooi mikpunt, maar het was zaak het gat niet te groot te laten worden. Ik hield een snelheid van net boven de 12 km/u goed vol en toen de laatste minuut in ging was de afstand tot de 2500 meter nog te doen. Ik kon aan het eind nog wat extra's geven en enkele seconden voor het fluitsignaal passeerde ik de finishlijn. Doel bereikt. Uiteindelijk liep ik 2505 meter. Een PR!!! En volgens de tabellen betekent dat ook een "zeer goede" conditie. Ook nooit weg.
