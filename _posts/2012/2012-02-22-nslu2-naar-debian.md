---
layout: post
title: NSLU2 naar Debian
date: 2012-02-22 22:57:00 +01:00
lang: nl
---

Sinds enige jaren heb ik thuis een Linksys <a href="http://nl.wikipedia.org/wiki/NSLU2">NSLU2</a> staan die wordt gebruikt als eenvoudige <a href="http://nl.wikipedia.org/wiki/Network_Attached_Storage">NAS</a> en <a href="http://nl.wikipedia.org/wiki/Back-up">backup</a> van belangrijke bestanden zoals bijvoorbeeld foto's. Alhoewel dit oude kastje niet meer aan de huidige systeemeisen zou voldoen (266 MHz ARM, 32MB) voldoet hij nog voor ons interne gebruik.

Het leuke aan de NSLU2 is dat Linksys de broncode van de Linux-kernel die ze gebruiken hebben moeten vrijgeven door de GPL-licentie van Linux. Daardoor zijn hobbyisten ermee aan de slag gegaan en zijn er diverse <a href="http://www.nslu2-linux.org/">alternatieve firmwares</a> gemaakt.

Onze NSLU2 heb ik enkele jaren geleden via Marktplaats gekocht en toen draaide de <a href="http://www.nslu2-linux.org/wiki/Unslung/HomePage">Unslung</a>-firmware er al op. Deze firmware is gebaseerd op de originele  Linksys software, maar die begon toch wel echt oud te worden. Weinig updates, zeer oude kernel, door de webinterface gebruikte hij relatief veel geheugen, de directorystructuur was soms wat rommelig door die extra laag.

Al met al een mooi moment om de rom te flashen en er een andere Linux-distributie op te gaan draaien. Enkele servers op kantoor draaien <a href="http://www.ubuntu.org/">Ubuntu</a>, mijn live USB-stick voor mijn laptop is <a href="http://live.debian.net/">Debian</a>, en <a href="http://www.debian.org/ports/arm/">Debian</a> draait op de NSLU2, dus dat was de meest voor de hand liggende keuze.

Een goede voorbereiding voorkomt een hoop gedoe. Dus heb ik eerst wat <a href="http://unslung.blogspot.com/">sites</a> <a href="http://www.keesmoerman.nl/nslu.html">doorgelezen</a>. Helaas gingen deze over de vorige versie van Debian. Voor de huidige versie, squeeze, het is <a href="http://www.cyrius.com/debian/nslu2/unpack.html">allemaal handwerk</a>. Heel ingewikkeld zag het er allemaal niet uit. Ik had nog een USB-stick liggen waar het operating system op kan draaien en de data-opslag blijft gewoon op dezelfde harddisk draaien. Dus daar hoefde ik niets aan te doen. Wel heb ik eerst nog even gecontroleerd of de disk leesbaar was op een andere pc en dat was geen probleem. Dus aan de slag.

De beschrijving was duidelijk en ook bij de uitvoering ervan geen problemen. Dus sinds <a href="https://twitter.com/FraCaMen/status/172035902162468865">gisteren</a> draait de NSLU2 op Debian en waren de eerste configuratiestappen genomen. Het is wat vreemd dat de Samba-server niet standaard geïnstalleerd is, het is immers een kastje dat speciaal voor dat doel verkocht werd. De configuratie hiervan was nog even een puzzel, maar dat is ook gelukt.

Zo kan onze server er weer even tegen. En door de nieuwe software lijkt hij wat sneller te reageren. Dat is mooi meegenomen.
