---
layout: post
title: Tramstoring, geen informatie
date: 2012-09-01 21:32:00 +02:00
lang: nl
tags:
- openbaar vervoer
- den haag centraal
---

Gistermorgen was de keuze, ga ik met de fiets door de regen of pak ik toch de tram. Het werd de tram, alhoewel het eigenlijk al bijna droog was. Maar op de terugweg ging het mis met de trams.

De trein bracht me netjes om half zes op Den Haag Centraal, dus dan loop je richting de trams. Boven aangekomen stond op het bord dat Randstadrail 4 over 0 minuten zou komen. Dat was een mazzeltje, was ik mooi op tijd thuis. Maar er stond wel een lege lijn 3 te wachten.

Na <a href="https://twitter.com/FraCaMen/status/241561202860388353">ruim 5 minuten</a> stond er nog steeds dat RR4 over 0 minuten zou komen, en was lijn 3 ook nog niet vertrokken. Het werd inmiddels wel duidelijk dat er ergens een probleem was. Dus toch maar even op twitter kijken of de <a href="https://twitter.com/HTM_Reisinfo">HTM</a> al iets gemeld had. Nee, dus.

Rond kwart voor zes kwam er een dame van de HTM naar boven dat er een storing was en dat er geen trams door de tunnel konden, meer wist zij ook niet. Dan maar naar beneden. Kijken of daar iemand was die iets meer wist.

Beneden hoorde ik van medepassagiers dat 2 en 6 daar zouden gaan rijden. De dame in het servicebusje wist te vertellen dat ze inderdaad zouden komen, maar wist niet hoe lang het nog ging duren. Op de elektronische borden stonden ze in iedere geval niet. Boven zie ik mensen vanuit de tunnel richting het station lopen. Die kwamen blijkbaar uit een gestrande tram.

In de wetenschap dat het dus wel even ging duren, door de stad richting de Brouwersgracht gelopen. Als de trams niet door tunnel kunnen, staan ze misschien wel daar. Niemand die het zeker wist, maar ik nam de gok.

In de Grote Marktstraat stond een groep van zo'n 15 man van de HTM gezellig met elkaar te kletsen. Het zullen wel de beveiligers van de tunnel zijn, maar informatie hadden ze niet. En op twitter stond ook nog steeds geen bericht. Het was inmiddels bijna 6 uur, de storing in de tunnel was al ongeveer een half uur aan de gang.

Op de Brouwersgracht aangekomen stond daar inderdaad een aantal trams waarom lijn 4 en kwam er een lijn 6 vanuit de stad die de Brouwersgracht op ging. Op lijn 4 stond nog Zoetermeer maar voordat ik er erg in had reed hij weg richting de Uithof, met nog steeds Zoetermeer als eindbestemming. Op de perrons bij de Brouwersgracht was niemand aanwezig van de HTM, niemand dus die je enige informatie kan geven.

Een <a href="https://twitter.com/FraCaMen/status/241568662971097088">gefrustreerde tweet</a> verstuurd en kort daarna kwam een lijn 2. Snel ingestapt en dan maar nog een keer een stukje lopen. Als ik maar weer onderweg was richting huis.

In de tram zie ik dat de HTM een paar minuten voordat ik mijn tweet verstuurde een aantal berichten dat geplaatst. Niet dat de info klopte want het staat er echt: <a href="https://twitter.com/HTM_Reisinfo/status/241566117317992449">RR4 rijdt vanuit de Uithof tot aan CS</a>. Dat moest dus Brouwersgracht zijn! Ook kreeg ik een DM van ze: "Geen info.....lees je tweets!". Ja, zo kan ik het ook. Ruim een half uur storing en niets plaatsen, personeel is er totaal niet of weet niets, dan verkeerde informatie tweeten en als ik dan een frustratietweet plaats, zo'n antwoord.

Dus beste HTM, dit is niet de manier op met je klanten te communiceren. Had om half zes een kort bericht geplaatst dat er een storing in de tramtunnel is en dat je nog geen exacte informatie hebt. Dan weet je in ieder geval iets. En zorg ervoor dat er op de cruciale punten (in dit geval CS, de Brouwersgracht en de mensen die toch al bij/in de tunnel zijn) mensen zijn die weten hoe het zit. En als je dan een half uur later een aantal tweets plaatst, zorg dan dat die informatie klopt!

Maar volgende keer in geval van twijfel pak ik gewoon de fiets, want nu was ik ruim een uur bezig om thuis te komen terwijl het maar een kwartiertje met de fiets is. Als ik direct was gaan lopen, was ik ook eerder thuis geweest.
