---
layout: post
title: Zonneklok gerepareerd
date: 2012-12-02 17:29:00 +01:00
lang: nl
tags:
- websites
- zonneklok
- programmeren
- raspberry pi
---

Sinds enige tijd heb ik een <a href="https://twitter.com/zonneklok">twitter</a>-account en bijbehorende <a href="http://www.zonneklok.nl/">website</a> genaamde Zonneklok, die aangeeft hoe laat de zon in Nederland opkomt en ondergaat. Maar het programma dat de tweets plaatst was kapot.

Op 10 oktober was de <a href="https://twitter.com/zonneklok/status/256074646946979840">laatste tweet</a> geplaatst, dus ik moest er hoognodig naar kijken. Dit weekend heb ik dat eindelijk gedaan.

Het bleek dat mijn eigen geschreven programma dat de tweets verstuurd om de een of andere reden niet weer werkt. Ik wilde niet dat er "onzinnige" test-tweets in de timeline van <a href="https://twitter.com/zonneklok">@zonneklok</a> kwamen en de library die ik gebruik om dat programma tweets te laten versturen was flink veranderd. Daarom heb ik ervoor gekozen om mijn eigen programma niet aan te passen of opnieuw te maken, maar gebruikt te maken van de <a href="http://en.wikipedia.org/wiki/Unix_philosophy">Unix-filosofie</a> en te zoeken naar een programma dat vanaf de command-line een tweet kan versturen. Na wat zoeken bleek dat <a href="https://github.com/jgoerzen/twidge/wiki">twidge</a> voor mij het beste werkt. Dit biedt alles wat ik nodig heb en het is een standaard package van <a href="http://www.raspbian.org/">Rasbian</a>. Daarmee vereenvoudig ik alles en dat scheelt weer onderhoud.

Daarnaast heb ik ook de software verplaatst van een server op kantoor naar mijn eigen <a href="http://www.raspberrypi.org/">Raspberry PI</a> die thuis als server(tje) draait. Hierdoor verandert er eigenlijk niets want deze systemen draaien allebei Linux, maar het is wel leuk dat mijn PI er een extra taakje bijkrijgt. Heel spannend is het allemaal niet voor dat ding 's nachts een keer bepalen hoe laat de tweets verstuurd moeten worden, die commando's schedulen en op het juiste moment uitvoeren.

Maar het belangrijkste is dat Zonneklok het weer doet. Nu moet ik toch maar eens wat tijd gaan stoppen in de promotie van de site en twitter-account. Want heel erg populair is hij nog niet. En dat terwijl een min of meer vergelijkbare account als de <a href="https://twitter.com/Klok_Domtoren">klok van de Dom in Utrecht</a> wel bijna 2500 volgers heeft, om over de <a href="https://twitter.com/big_ben_clock">Big Ben in Londen</a> nog maar te zwijgen. Hoe krijg je bijvoorbeeld een BN'er zo ver dat hij of zij er iets over zegt of zo'n zonsopkomst-tweet retweet? Als iemand suggesties heeft, plaats een reactie!
