---
layout: post
title: Vrouw achter het stuur...
date: 2012-10-07 12:19:00 +02:00
lang: nl
tags:
- openbaar vervoer
- den haag
---

Vrijdag werd binnen 5 minuten het vooroordeel over de vrouwelijke chauffeur zowel bevestigd als ontkracht.

Ik zat in <a href="http://nl.wikipedia.org/wiki/Tramlijn_6_%28Haaglanden%29">tram 6</a> op weg naar huis en de tram reed in de Hobbemastraat. Op de kruising met de Paulus Potterstraat staat een Peugeot 207 met 4 jonge dames erin netjes te wachten tot de tram gepasseerd is. Alleen stond ze net iets te ver naar voren, waardoor de tram er niet langs kon.

Tot zover niets bijzonders, denk je dan. Even in zijn achteruit, een half metertje naar achteren rijden en iedereen kan zijn weg snel en soepel vervolgen. Helaas was de realiteit iets anders. De dame achter het stuur kunnen we direct opgeven voor het BNN-programma "<a href="https://nl.wikipedia.org/wiki/De_slechtste_chauffeur_van_Nederland">De allerslechtste chauffeur van Nederland</a>" want in plaats van de auto in achteruit te zetten, reed ze iedere keer een heel kleine stukje naar voren. Blijkbaar in de hoop dat de tram dan wel even achteruit zou rijden, want de dame achter het stuur begon druk te gebaren naar de trambestuurder dat hij achteruit moest!

Voor de mensen die niet zo bekend zijn met trams, dat kan dus niet zomaar. De bestuurder moet dan naar de achterkant van de tram lopen. De achterbank verwijderen en want daaronder zitten een versimpeld bedieningspaneel om de tram achteruit te laten rijden. Dat ging hij niet doen!

In het voorste deel van de tram was er inmiddels flinke hilariteit over de ontstane situatie. "Wat denkt die muts wel", "Ze kan toch makkelijk achteruit" en "Echt een vrouw achter het stuur" waren enkele gehoorde kreten. Inmiddels stond ze met haar Peugeot bijna tegen de tram aan.

Vooraan in de tram stond een net zo jonge dame die even overlegde met de bestuurder van de tram en vervolgens een andere deur nam om uit te stappen. Kordaat liep ze op auto af en had een gesprek van een seconde of 10. Daarop stapte de jongedame achter het stuur uit, het meisje uit de tram stapte in de auto, reed die een paar meter achteruit en stapte weer uit. De trambestuurder deed de deur voor haar open en ze stapte weer in, de dames in auto in verwondering achterlatend over deze eenvoudige maar doeltreffende oplossing. En de tram kon zijn weg vervolgen. Terug in de tram werd het meisje door een groot aantal mensen gecomplimenteerd.
