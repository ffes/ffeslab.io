---
layout: post
title: Work in progress
date: 2005-07-25 21:00
lang: en
category: software
tags:
- dorgem
---

Currently I'm trying to add FreeImage to [Dorgem](https://dorgem.sourceforge.net/).
First of all, this will give you the option to use other formats than .BMP for bitmap captions.

But I'm have difficulties with the various transparent variants of .PNG and .GIF.
This is quite difficult also because of the way the overlays are handled right now.
For those of you interested in the technical details, the overlay function only gets a hDC passed through and FreeImage wants a FIBITMAP and there is no easy conversion available.

So maybe I'll give it a try in DevIL as well, but the developers of that library seem much less active than those of FreeImage.

This is not yet available, since (as the title says) it is work in progress.
It just is not finished.
