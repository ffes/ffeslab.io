---
layout: post
title: Harddisk - part 3
date: 2005-06-13
lang: en
comments:
  - comment:
    author: Tachyon Eagle
    text:
      - episode 3 being final ... what analogy ... [:-)]
      - hmmm , it's sad that even your drive was converted by the dark side of the force .
      - but in every kind of HDD lies hope ...
---

The final episode in my harddisk tales.

Thursday evening I reinstalled my old harddisk and let Windows start.
And how strange it was to find out that Windows didn't see the drives on that new disk anymore.

So I rebooted from the Win98 disk and the D and E drive were accessible again.
So I copy the files I wanted to rescue and removed the disk from my PC.

Friday I ran the diagnostics of the PowerMax tool from Maxtor and it failed.
Unfortunately the disk is out of warrant so I guess I'll have to throw it away.
