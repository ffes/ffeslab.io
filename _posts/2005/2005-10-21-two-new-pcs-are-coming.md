---
layout: post
title: Two new PCs are coming!
date: 2005-10-21
lang: en
---

Yesterday we ordered a new PC for home usage, and today my new laptop should arrive at the office.

The new home PC is absolutely nothing special.
It is a Sempron 3000+, with 512 MB and a 200 GB HD.
We really needed more disk space, and buying a new HD for my 5,5 years old 500 MHz PIII didn't seam like a wise thing to do.
So now we go from 20GB to 200GB. Should be enough for a while.

I'm planning to install Slackware Linux on it.
Hopefully things will go better then when I tried this before

If the tracking information on the UPS site is right (and why should I doubt that [;)]) the new laptop is in the delivery truck right now.
It is a Dell Latitude D510, wide screen, 1GB memory and 80GB (7200 rpm).

So two PC's need to be configured these days, so a lot of waiting for installers to complete [:(]
