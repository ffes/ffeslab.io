---
layout: post
title: SourceForge [update]
date: 2005-11-15
lang: en
---

Apparently they were having problems with the new styled [SourceForge](https://sourceforge.net) website.
It is back to the old design.
Yesterday evening the site was down quite some time, so it doesn't come as a big surprise to me.

One thing I didn't like in the new design of the project pages that the versions of the released files were not visible anymore directly from that project page.
