---
layout: post
title: E-mail Storage
date: 2005-06-29
lang: en
category: software
tags:
- dorgem
comments:
  - comment:
    author: Nick
    date: 2008-04-23 22:15
    text:
      - tested today with smtp.gmail.com did not work even though telling me that everything went successfully [:(]
  - comment:
    author: Nick
    date: 2008-04-23 22:16
    text:
      - sorry forgot to say, the version i used was 2.1.0 and i have Windows XP
---

Today I've committed the first implementation of e-mail storage events.
This means that you can e-mail the captured images and can be very useful when used in combination with motion detection.
You need to enter the e-mail address and mail server.
Optionally you can also give a username and password for SMTP authentication.

The SMTP client is not fully tested yet.
It is an MFC conversion of the ATL based SMTP client found at [CodeGuru](https://www.codeguru.com/).
It already seems to work fine, but the code still needs some cleanup.
And the help is not updated yet, but I don't think that will be a problem to people who use nightly builds.

So grep yourself (tomorrow) the nightly build, have fun with it and let me know what you think of it.
