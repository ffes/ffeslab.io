---
layout: post
title: Getting close to a release
date: 2005-05-17
lang: en
category: software
tags:
- dorgem
comments:
  - comment:
    author: ArSa
    text:
      - i am exited! [:)]
---

Finally the last feature for the 2.1 release is finished.
The "Custom Captions" for the webserver.
I've implemented it, but I need to test some minor things, like if it compiles with VS.NET.
But that needs to be done at the office.

I also got a bug report with a possible fix for the unexpected crashes some people have.
I will definitely take a closer look at the things he suggests in that item.
Let's hope that it will solve the problem.

I noticed it has been more then a year since the last beta release, but please know that the 2.1 non-beta is coming.
Again I won't predict a release date.
You never know what will happen.
