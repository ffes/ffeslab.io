---
layout: post
title: 10 Years married
date: 2005-06-22
lang: en
---

Yesterday we were married for 10 years!

To celebrate this we went to a Chinese restaurant.
And since Menno has not been that often in a real restaurant (I exclude the McDonalds and likes) he had a great time.
He was excited about everything, the way the restaurant was furnished, that he could order his own drink and food, the hot towel we got when we were finished eating, the way his ice-cream looked, etc, etc.

Caroline and Menno, I love you [:-X]
