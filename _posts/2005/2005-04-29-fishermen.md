---
layout: post
title: Fishermen
date: 2005-04-29
lang: en
comments:
  - comment:
    author: Erik
    text:
      - Well, I suppose that even a fish might sense some danger when a neon-coloured coat is just above the water. Not that that makes a camouflage-coat necessary of course.
---

Every time when I ride to the office on my bike, I ride by a small lake.
This is a favorite spot for fishermen.
But what I don't understand is why almost all of them need to wear camouflage clothes.
Are they afraid the fishes would see them otherwise?
