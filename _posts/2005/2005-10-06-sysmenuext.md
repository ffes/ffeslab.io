---
layout: post
title: SysMenuExt
date: 2005-10-06
lang: en
---

A while ago I started a new project at SourceForge named [SysMenuExt](https://sysmenuext.sourceforge.net/).
Yesterday I released the first public version of it, version 0.6.0.
The program has not yet reached an 1.0 status, but currently it sort of works.
So I decided to release it.

Basically SysMenuExt is a open source rebuild of the apparently abandoned project PowerMenu.
It is heavily based upon some articles the can be found a [CodeProject](https://www.codeproject.com/).

It adds three items to the system menu (the menu that opens when you right-click the program on the task bar) of every program you have started.
With this items you make that program "Always on top", Semi-transparent and you can minimize it to the system to tray (next to the clock).
