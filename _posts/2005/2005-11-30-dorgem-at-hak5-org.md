---
layout: post
title: Dorgem at hak5.org
date: 2005-11-30 21:00
lang: en
category: software
tags:
- dorgem
comments:
  - comment:
    author: Erik
    text:
      - Heheh... cool!
---

Michael recently pointed me to [www.hak5.org](https://www.hak5.org) through my tagboard.
In their episodes number 2, they show how easy it is to add a webcam to your site using [Dorgem](https://dorgem.sourceforge.net/).

Thanks Michael for the info!
