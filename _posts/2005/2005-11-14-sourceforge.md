---
layout: post
title: SourceForge
date: 2005-11-14 21:00
lang: en
---

Whoooo!!! They have redesigned [SourceForge](https://sourceforge.net)!

Boy, what a shock after all these years suddenly a completely new design.
Don't know yet if I like it, but it sure is different.
