---
layout: post
title: FotoVinder.nl
date: 2005-09-15
lang: en
---

The last weeks my and two colleagues have been very busy in our spare time with a new online and offline game, FotoVinder.nl.
It is free, no money involved, just for fun.

In short, this is a site were someone places a photo and the other members of the site must guess where this photo is taken.
The one who gives the right answer first most place a new photo.

The site is only in Dutch, but feel free to have a look.
