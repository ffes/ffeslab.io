---
layout: post
title: What is the hype?
date: 2005-05-25
lang: en
---

Just ran into this article on the ITV F1 site named "E-MAIL WAS KEY TO KIMI VICTORY!".

When will journalists stop hyping electronic communication?
It is just another way of communication.

Would it have been worth an article if the guys in England would have called to Monaco with their mobile phone?
What would have happened to that journalist if he would have found out they had been chatting on MSN or even better IRC!
He would have gone mad and probably needed treatment in a hospital [:D]
