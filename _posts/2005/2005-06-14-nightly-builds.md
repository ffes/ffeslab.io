---
layout: post
title: Nightly Builds
date: 2005-06-14
lang: en
category: software
tags:
- dorgem
---

The last days I've been busy setting up a nightly build system for [Dorgem](https://dorgem.sourceforge.net/).

So now you can always download a nightly build.
This system also generates a `changelog.txt` from the CVS.
In this file you can see the most recent changes to the CVS and see if it is necessary to download the nightly build.

So know that the files in that directory are uploaded every night, even if there was not change to the CVS.

Also know that the nightly builds are created every day at 05:00 local time here in The Netherlands.
