---
layout: post
title: CVS compiles with VS.NET 2003
date: 2005-05-09
lang: en
category: software
tags:
- dorgem
---

I just updated the CVS of [Dorgem](https://dorgem.sourceforge.net/).
Now it also compiles with Visual Studio .NET 2003.
But Visual C++ 6 is still my main development platform for Dorgem.
The main reason for this it that at home I still only have to use VC6 (on my old Pentium III 500 MHz with 20GB harddisk).

And I most say that I still prefer VS6 for pure MFC based C++ development, which Dorgem is.
I think that IDE works much better.
Where is the Class Wizard?
