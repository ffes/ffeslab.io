---
layout: post
title: Harddisk - part 2
date: 2005-06-09
lang: en
---

> And oh miracle... Everything just ran as expected!

That was the last sentence in my previous entry.
And guess what...

It doesn't run as expected anymore.
In fact it doesn't run at all anymore :(

What happened.
I had left 15 GB unallocated space that I wanted to use for Linux.
So yesterday I let my computer boot from the Slackware 10.1 CD.
I started `cfdisk` to partition that space into a Linux partition.
When tried to write the partition information the disk, ERRORS.
`cfdisk` could not create the partition.
But even worse, Windows doesn't start anymore.

With a Win98 boot disk I still can access the C drive (luckily I choose FAT32) and the few things I added (like one day of photos) were still accessible.
And I still have the old disk in the same condition when I took it out of my PC.

So tonight I will try to see what I can do with a BartPE rescue CD.
