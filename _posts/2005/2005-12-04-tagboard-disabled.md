---
layout: post
title: Tagboard disabled
date: 2005-12-04
lang: en
category: website
comments:
  - comment:
    author: Michael
    text:
      - What about an IP ban? I've fixed my board so you can easily ban people who spam from fixed IP adds.. (of course this only works IF they spam from fixed IPs... [:(]
  - comment:
    author: Frank
    text:
      - Nope, the came from different IP addresses. One was in Argentine another in the US and one from a IP that didn't resolve to a hostname. [:-(]
  - comment:
    author: Erik
    text:
      - Man, this stinks... And making a login just for a tagboard is a BIT overdone...
---

I just disabled the tagboard.
I was spammed twice today [:-(]

I haven't had time to install the antibot script, so until then you can not enter a new entry in the tagboard.

Sorry but it is the only thing I can do right now.
