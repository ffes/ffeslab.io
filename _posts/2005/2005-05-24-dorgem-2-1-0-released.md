---
layout: post
title: Dorgem 2.1.0 released
date: 2005-05-24
lang: en
category: software
tags:
- dorgem
---

I just released [Dorgem](https://dorgem.sourceforge.net/) version 2.1.0 (non beta ;)).

I hope this version will solve some of the stability problems some people were experiencing.
I know some of these problems were clearly camera related.
Other not, but I found one thing in the source that could be the source of those problems.
For those of you who are interesting in the technical details: The `CreateDIBSection()` needs a `FlushGDI()` on WinNT/2k/XP to complete.

When you look at the differences between 2.0 and this 2.1 the list with changes, it is quite long.
So I hope you all enjoy this new version.

