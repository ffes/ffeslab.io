---
layout: post
title: Watching Katrina
date: 2005-08-30
lang: en
---

Yesterday I spend a large part of the evening looking at the live video stream of WWL-TV, the local news station of New Orleans to see the developments in their struggle with [hurricane Katrina](https://en.wikipedia.org/wiki/Hurricane_Katrina).

I watched some very shorts parts of their broadcast at the office.
When I returned home and Menno was asleep I tried to connect again, but they were offline for some time.
When they returned the studio was not operational yet and they had to broadcast from an office.
When they returned to the studio, apparently it was damaged as well because every now and then the papers were almost blown of the desks.

Although I've been on the Internet for more the 10 years now, I still find it kind of weird to be looking at the local news at the other site of the world, witness the destruction done by Katrina and see and hear the stories of the people there.
