---
layout: post
title: Dorgem IRC Channel
date: 2005-04-11
lang: en
category: software
tags:
- dorgem
comments:
  - comment:
    author: Kevin
    text:
      - COOL! I use Dorgem all the time and it will be exciting to check the IRC channel and perhaps chat once in a while. Great idea [:)]
  - comment:
    author: jack
    text:
      - what is it for programme, i dont understand.
---

Recently I have setup an IRC channel for [Dorgem](https://dorgem.sourceforge.net/).

Server: irc.freenode.net  
Channel: #dorgem  
My nick: FFes

It's all a bit experimental at this stage.
I haven't got a clue if people are interested in this, so I will see.
I will try to be there as much as possible.
This means the European office hours you probably won't find me there, but at the evening the changes are bigger.
But remember, I do have an offline life [;)]

The IRC servers of [Freenode.net](https://freenode.net) are especially for open source projects to have a way for users and developers (in any combination) to communicate with each other.

Hope to see you on the IRC channel.
