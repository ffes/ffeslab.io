---
layout: post
title: Camera problems?
date: 2005-04-14
lang: en
category: software
tags:
- dorgem
---

The more time I invest in the resource problem some people are experiencing, the more start to think they are VfW and camera related.

I just 'spoke' to zann in the IRC and he often runs [Dorgem](https://dorgem.sourceforge.net/) for a month without any problem.
And he is not the only one.
Other people have similar experiences.
So therefore I more and more start to think these problems are not in Dorgem.

So I plan to fix some small minor problems, that have been in the program too long now and finally (yes, it have been too long, I know [:)]) release version 2.1
