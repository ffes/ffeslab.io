---
layout: post
title: Back to the usual
date: 2005-08-16
lang: en
---

Last Thursday we returned from our very pleasant vacation at [camping Reilerweier](https://www.reilerweier.lu) in Reuler near Clervaux in Luxembourg.
We really had a good time, despite the not so good weather.

The camping was exactly what we needed (being a family with a 7 year old son [;-)]).
There was enough animation to keep us happy, but not too much so we could go and do the things we wanted to do.
But despite that we don't have the feeling we've seen everything we wanted, so there is a chance we will be there next year again.

And as soon as you are back home, the usual things start again.
Doing your daily shopping, a birthday, back to work, Menno back to school, etc, etc.

So now I will continue to work on the FreeImage and/or DevIL integration for Dorgem [I told you before]({% post_url 2005/2005-07-25-work-in-progress %}).

