---
layout: post
title: Strange combination
date: 2005-05-04
lang: en
---

This week is always a very strange combination here in The Netherlands.

Last Saturday was 30 April, [Queen's Day](https://en.wikipedia.org/wiki/Queen%27s_Day) here in The Netherlands.
So this means a national holiday.
The Queen and her family visited my home town The Hague.
We went there to see if we could catch a glimpse of them, and we did.

Today is 4 May, [Remembrance of the dead](https://nl.wikipedia.org/wiki/Nationale_Dodenherdenking).
This means that everybody in The Netherlands is silent for 2 minutes at 20:00 to remember everybody who has died since WOII in war or during peace keeping operations.

And tomorrow is 5 May, Liberation day.
We celebrate the end of WOII in The Netherlands and that we can live in freedom.

Also tomorrow, Menno will go on camp for 4 days and we will be off for a short break as well.
