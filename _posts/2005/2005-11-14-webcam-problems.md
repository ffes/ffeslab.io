---
layout: post
title: Webcam problems
date: 2005-11-14 19:00
lang: en
---

Since the installation of my new PC, I'm having big problems with my webcam.
It is a Philips ToUCam XS and it is supported by Windows XP natively.
With my old PC I had no problem at all with it.
When I connected it the first time it worked as expected, but now all I see is a green picture.
I've read many others have similar problems.

There are two main differences between the two PCs.
XP SP1 vs XP SP2 and USB 1 vs USB 2.
The other differences seem irrelevant to me.

So currently there is not much I can do on Dorgem.
Without a webcam it is difficult to test [:(]
