---
layout: post
title: Dorgem resource problems in beta 5
date: 2005-03-09
lang: en
category: software
tags:
- dorgem
---

The last week I have been busy with [Dorgem](https://dorgem.sourceforge.net/). Finally some people will say. [;)]

I have been trying to nail the resource problems some people have with Dorgam 2.1.0 beta 5.
So I have installed BoundsChecker on a test PC and it came with a resource related problem.
It has something to do with the `SelectObject()` calls in `DoCapture()` in `DorgemDlg.cpp`.
But I don't see what I am doing wrong here.
So if anyone with knowledge of DC programming can take a look at it, this would be appreciated very much!
I don't use the MFC CDC classes, but the Win32 API DC calls.
