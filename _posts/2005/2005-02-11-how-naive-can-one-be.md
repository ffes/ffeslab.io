---
layout: post
title: How naive can one be?
date: 2005-02-11
lang: en
comments:
  - comment:
    author: Michael
    text:
      - Some people just don't want to admit that there IS NO ultimate OS! I managed to crash the "oh so stable" Linux in like 15 minutes after I installed it. But some people are so zealous in their love for their particulair OS that rational thinking is thrown out the door... how sad.
  - comment:
    author: tachyon_eagle
    text:
      - there is a qutore "if programmers would be the civilization , the first pigeon would destroy it" - it's the ultimate truth - beleive it [;-)] ,
      - you can crash anything you want, sometimes it doesn't matter , even large mainframes can be crashed but there it's not something special or fatal ... as system is made heavily redundant ...
      - and as far as Firefox / Mozzila is concerned , surely they are greatest browsers for windows today, but tehy still lack a few things.... the biggest flaw I found,that they are unable to crunch 10+ MB html and survive it (even on strong machine) .... (generating dorgem images 1 per second = +- 70k files in directory per day= try to display it over php ... and you'll see [:-)] - it just doesn't work out - the only browser that did part of it was konqueror)
---

Today I read this article by Mark Morford. Man, is he naive!

For those of you who don't want to read his entire column, in short he is very surprised the everyone stays with Windows and not switch to a Mac.
One of his main arguments is that there are so many security problems in Windows and so many spyware and viruses for Windows.
And they (hardly) exist for Mac.

**Wake up Mark!**

There are many viruses and spyware for Windows because it is used much! For a virus or spyware writer it is much more interesting to write a virus that works on 95% of all the computer (Windows) in stead of 5% of the users (Mac, Unix-like and other OSes)

When much more people start to use a Mac or Linux as main OS, security problems will be found.
The fact they are not found, doesn't mean they don't exist.
Viruses and spyware will be written for these OSes as well when they would be used more.

Look at Firefox and Mozilla.
Many users, including myself, switch from from Internet Explorer and not only for the tabbed browsing.

But today I read an article that the first spyware for Mozilla was spotted in the wild.
Now the market share of Internet Explorer has dropped to 85%, it was just a matter of time before this would happen.
