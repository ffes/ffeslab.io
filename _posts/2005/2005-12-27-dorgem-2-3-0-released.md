---
layout: post
title: PrevRSI 2.3.0 released
date: 2005-12-27 21:00
lang: en
category: software
tags:
- PrevRSI
---

I just released [PrevRSI 2.3.0](https://prevrsi.sourceforge.net).

This is the RSI prevention tool I maintain.
If you use the computer often, I can strongly advice to use a RSI prevention program.
And why pay for a program when a good (I hope [;)]) open source alternative is available.
