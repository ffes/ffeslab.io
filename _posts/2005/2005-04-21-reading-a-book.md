---
layout: post
title: Reading a book
date: 2005-04-21
lang: en
---

This morning in the train a man was reading a book.
Nothing special you will say, but he every time when he had finished a page he literally ripped that page out of his book, he made a pellet of it and threw it in the waste basket.

But that way you can never forget where he left off [:-)]
