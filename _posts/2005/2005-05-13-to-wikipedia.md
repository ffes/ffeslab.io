---
layout: post
title: To Wikipedia
date: 2005-05-13
lang: en
comments:
  - comment:
    author: tachyon_eagle
    text:
      - yup, Wiki is great [:-)] it is also super-searchable with google thru "anything wiki" will surely hit your topic
---

It is not a real verb, but yesterday evening once again I was redirected to an article in Wikipedia, the free on-line Encyclopedia.
I spend a major part of the evening clicking and reading in it.

If you haven't discovered it yet, give it a try! Be sure to have time enough to do so [;)]
