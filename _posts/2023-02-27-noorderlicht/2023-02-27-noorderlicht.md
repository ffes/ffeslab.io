---
title: Noorderlicht
lang: nl
date: 2023-02-27 23:49:16 +0100
category: fotografie
tags:
- aurora borealis
- noorderlicht
---

Gisteren was er een hoogtepunt in zonneactiviteit en dat leverde ook hier in Nederland mooie plaatjes op van het [noorderlicht](https://nl.wikipedia.org/wiki/Poollicht).
Alleen had ik dat even gemist.
Hier in de stad is dat sowieso lastig om te zien.

Maar toen ik las dat er vanavond weer een goede kans was om het noorderlicht in Nederland te zien, was de keuze snel gemaakt.
Zo snel mogelijk naar de kust!

Mijn [Gorillapod](https://joby.com/nl-en/gorillapod/) gepakt met de houder om mijn telefoon te bevestigen erbij.
Ik heb al jaren geen spiegelreflex meer, maar de Gorillapod voor die camera heb ik nog wel.
De camera heeft Menno tijdens de corona als webcam in gebruik genomen en meegekregen toen hij het huis verliet.
En dat is een Canon 1100D van inmiddels ruim 11 jaar oud, dus ik had alleen mijn Samsung Galaxy A72 om foto's mee te maken.
Het was even afwachten hoe deze zich zou gedragen, maar Samsung heeft prima camera's en ook een pro-stand waarmee je de sluitertijd en ISO kunt instellen.
En na jaren met een spiegelreflex fotograferen weet ik hoe diafragma, sluittijd en ISO werken.

Rond kwart over negen stapte ik in de auto om naar de [Wassenaarse Slag](https://nl.wikipedia.org/wiki/Wassenaarse_Slag) te rijden.
Dit is in de Haagse regio een prima plek om het noorderlicht te zien.
In maart 2015 was er ook zo'n grote kans om het noorderlicht te zien en toen heb ik daar ook gestaan en het heel flauw kunnen vastleggen.
De Wassenaarse Slag is aan de kust dus een open blik op zee en omdat het duingebied [Meijendel](https://nl.wikipedia.org/wiki/Meijendel) best groot is, is er voor randstadbegrippen relatief weinig lichtvervuiling.

{% include map.html title="Wassenaarse Slag" lat="52.164786" lon="4.349953" bbox="4.34772,52.16394,4.35218,52.16562" %}

Na ongeveer een half uur rijden stond ik op de parkeerplaats, waar het best druk was voor het tijdstip.
Ik was uiteraard niet de enige!
Op het strand was een rustig plekje met een beetje beschutting snel gevonden.

Met het blote oog had je eigenlijk niet het idee dat er noorderlicht was.
Boven zee was het net iets lichter en een hele lichte zweem van kleur leek er wel in te zitten.
Nee, zo mooi als gisteravond zou het vast niet worden.

Dus maar eens proberen een foto te maken uit de losse hand om een idee te krijgen.

![Eerste foto](noorderlicht1.jpg)

WOW, NOORDERLICHT!!!
Was die lichte gloed toch echt van het noorderlicht!

Snel mijn Gorillapod aan een paal geknoopt en met wat instellingen gespeeld en de foto's werden alleen maar beter.
Verbazingwekkend hoe goed de camera van een (bijna 2 jaar oude) midrange smartphone is.

Rond 22:15 lag het hoogtepunt.
Dat zag je ook aan de meeste foto's die je in diverse media langs zag komen.

![Hoogtepunt van de activiteit](noorderlicht2.jpg)

Nog een tijdje gebleven en foto's gemaakt, maar zo mooi als rond 22:15 werd het niet meer.
Op de weg terug naar de auto heb ik nog met een gezin staan praten.
Ze waren jaloers op mijn foto's.
Met zijn iPhone was het hem niet gelukt om goede foto's te maken.
Ik weet dat een iPhone ook een pro-stand moet hebben, maar omdat ik zelf geen iPhone kon ik hem niet helpen.
En als je niet weet wat je moet instellen heeft zo'n stand ook niet zoveel zin.
Wel ter plekke de foto die zij het mooiste vonden gemaild.

![Met paal](noorderlicht3.jpg)

Voldaan stapte ik even later in de auto.
Blij dat ik ben gegaan.
