---
layout: post
title: Babel Fish
date: 2006-02-21
lang: en
comments:
  - comment:
    author: Vigo
    text:
      - I think your being to harsh... Its not an exact translator neither claims to be one. It aims to translate well enough to be understandable and I think its great at that, I have used it to translate lots of text wich I wasnt able to read otherwise.
      - Also, a few French ppl have tried to contact me... and I would never have known why if I couldnt translate their messages with Babelfish.
      - The only problem with Babelfish is it is too picky [:S] just one strange character and it doesn't recognize the word anymore
      - (And I love the HG2G reference http://en.wikipedia.org/wiki/Babelfish [:)]
---

You probably have heard about [Babel Fish](https://en.wikipedia.org/wiki/Babel_Fish_(website)), the translation service of [Alta Vista](https://en.wikipedia.org/wiki/AltaVista).

Well, it calls itself a translation service, but I would call it a word replace service.
When I translate "How are you?" to Dutch, it returns "Hoe bent u?" :S
How useful is a translation service that doesn't even know how to translate such a basic sentence as "How are you?".
For every student learning a foreign language, this is one of the first things to learn but Babel Fish still needs to learn it.
