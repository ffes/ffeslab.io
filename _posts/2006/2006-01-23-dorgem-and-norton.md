---
layout: post
title: Dorgem and Norton
date: 2006-01-23
lang: en
category: software
tags:
- dorgem
comments:
  - comment:
    author: Ashley
    text:
      - There seems to be a 15 day trial version at symantec.com. Go to downloads > Trialware > Virus Protection. Hope this helps. [:-)]
---

Is there anyone who can help investigating bug [#1281375](https://sourceforge.net/p/dorgem/bugs/33/)?
It is a problem where Norton Anti-Virus says that through [Dorgem](https://dorgem.sourceforge.net/) spyware and trojans are blocked.

I don't have Norton Anti-Virus and there is no trail software available, so I am not able to reproduce the problem and see what happens exactly.

Is there anyone who can help or has similar problems.
Please add a comment to the bug or [contact me directly]({% link about.html %}).
