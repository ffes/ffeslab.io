---
layout: post
title: Dorgem and wxVidCap
date: 2006-04-08
lang: en
category: software
tags:
- dorgem
comments:
  - comment:
    author: Keri
    text:
      - I'm kind of hoping that you might make Dorgem a self-contained webcam program, so I could put it on my USB drive, and no registry settings written.
      - Otherwise, I'm loving it!
  - comment:
    author: Jem Fry
    text:
      - I love Dorgem. It's by far the best Webcam program and I have gigabytes of photos of my children growing up thanks to the application. I was very happy to see you planning a service/daemon based approach for the next version. The one problem with Dorgem is that it's too easy to close it accidentally, especially when you have toddlers on the keyboard, whereas a service version would carry on running in the background even if the GUI is closed.
---

Last week I have tested wxVidCap.
It is a [wxWidgets](https://www.wxwidgets.org) class to add video capture support to an application.
Currently only the VfW is fully implemented, but I must say I'm quite impressed with it.

I know some of you will say, I thought you wanted to drop VfW.
Yes, you're right.
But adding a DirectX implementation to this class should not be too difficult (once you know DirectX of course [:-S]).
And it has a (limited) implementation for V4L (Video for Linux) as well.
The way it is build other platforms could be added as well, when the proper hardware is available.

And with this class I can at least make a start writing [Dorgem](https://dorgem.sourceforge.net/) 3 using wxWidgets.

I still have to think about the main architecture of the program.
I'm leaning towards daemon / service based, so console app that does the hard work and a GUI to configure it.
On Linux it is quite common to have a command line based multimedia application.
It should make it easier to run it on a server.
It would not work anymore in Win9x, but I don't see that as a problem anymore.
I know two wxDaemon classes exists.
The authors said they would add it to wxCode, but apparently this has not happened yet.
I have contacted them both, but they both didn't respond yet.
