---
layout: post
title: PrevRSI 2.3.1 released
date: 2006-01-06
lang: en
category: software
tags:
- PrevRSI
---

I just released a minor update for [PrevRSI](https://prevrsi.sourceforge.net).
It fixed a bug in the changer system.

If you don't use the changer, it is not really important to upgrade.
But you miss a very nice feature to change the pause screen automatically.
At the office we have a collection of HTML pages with various webcams all over the world.
During every break other webcams are shown.

Also found out prevrši is a (I guess) Croatian word.
Can anyone tell me what this means?
