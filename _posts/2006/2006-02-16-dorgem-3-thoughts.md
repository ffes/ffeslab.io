---
layout: post
title: Dorgem 3 thoughts
date: 2006-02-16
lang: en
category: software
tags:
- dorgem
comments:
  - comment:
    author: codec
    text:
      - Heya,
      - I would like to see Dorgem for Linux.
      - I really like Dorgem, it's a great tool (has all I need for my webcam).
      - But I hate Windows and I'm still searchin' for a good webcam-app on Linux. [:)]
  - comment:
    author: John Osenbaugh
    text:
      - Actually I was using dorgem to take still pictures of clients to put in tneir charts(in a clinical enviornment) due to the fact that digital cameras keep breaking, getting lost and "walking off"
      - I got a couple of really cheap web cams that actually have a button to take a snap shot and am curious if there is a way to use that button rather than have our techs click the button. The easier I make it, the better off I will be in the long run [:)]
      - Any feedback, suggesions or registry changes that I could make to solve this issue would be greatly appreciated.
      - I would also like to say thanks to all who contributed to the Dorgem project. Since I started using dotnetnuke i have started to realize the benefit and value to using open source apps. I was always kind of shy about them in the past!
      - Keep up the good work!
  - comment:
    author: David Horner
    text:
      - Thanks for releasing your software. It really seems to be a nice platform for some time lapse work I've been wanting to do.
      - As for the rewrite and the question of libraries to use....
      - I'm a fan of working out a plugin and object interface structure so that I could plug in binary actions. If you are interested in something like this for your re-write, I would be very interested in helping.
      - Thanks again!
      - Dave
      - http://dave.thehorners.com
  - comment:
    author: Albert Sims
    text:
      - Just wanted to say I just found the Dorgem product, and love it! It'll be the perfect replacement for the program I was using for years, until I discovered no more development on it as of Sept.15 of this year (Webcam32). This program does everything I need to upload my cams, and I'll be putting a link up on my site very soon! A "little development" whenever you have time is better than no development at all! [:-)]
---

Lately I've been investigating many things for next releases of [Dorgem](https://dorgem.sourceforge.net/) and come to the conclusion that the only proper way to add new features is the stop developing for a 2.x release and move on to Dorgem 3.0 even though it requires a almost complete rebuild of Dorgem.

A rebuild?
Yes, I'm afraid that is the only real way to be able implement new things, like for instance DirectShow.
Currently the communication with the camera is based upon Video for Windows (VfW) and is build into the main dialog.
New cameras don't always support VfW anymore and VfW has problems with more then one camera on one machine.

Another thing is the streaming web server.
The current implementation simply can not give a constant stream of images.
A new web server is needed.

And the list goes on.
To add proper support for other graphical file formats, I have tried to add [DevIL](https://openil.sourceforge.net) and [FreeImage](https://freeimage.sourceforge.io).
A quick and dirty method to be able to use .jpg as an overlay would be possible.
I like the quick part, but definitely not the dirty part.

So when I would rebuild Dorgem, are there other things that can be changed as well?
Should I stick with MFC?
Are people interested in a version for other operating systems?
What would be the new architecture of Dorgem 3 and beyond?

A lot of questions and not too much answer yet.

I recently programmed a little using [wxWidgets](https://www.wxwidgets.org) and I must say I like it.
It has advantages over MFC, like that it is multi platform.
A wx-application build for Windows can be rebuild on Linux and MacOS, when not too much platform specific things are used.
And other pro is that it drops the requirement of the Microsoft compiler.
Furthermore wxWidgets has built-in support for common graphical file formats and support gettext so the application can be translated into other languages.

But the programming I have done in wxWidgets yet is not that much and I need to learn a lot.

When an application is build with multiple platforms in minds, platform specific thing (like communication with the camera) need to be separated from the GUI.
On Linux people probably also want run it as a daemon.
On Windows running as a service would be nice.

As said, much questions and few answers.
But still I thought it would be wise to share them with you.

The main conclusion it that there will no 2.x release anymore.
I will stop the nightly build system but will leave the latest nightly build in place.
And I will start to make more plans for Dorgem 3.x
