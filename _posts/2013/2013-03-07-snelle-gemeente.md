---
layout: post
title: Snelle gemeente
date: 2013-03-07 14:36:00 +01:00
lang: nl
tags:
  - den haag
  - verkeerslichten
  - gemeente
comments:
  - comment:
    author: Anoniem
    date: 2013-03-14 09:01:42 +01:00
    text:
      - ja dat is zeker een snelle reactie,maar vast omdat de ambtenaar er zelf ook last van had.
---

Al enige werken stoor ik me aan de regeling van de verkeerslichten op de kruising met de Zuiderparklaan en Loosduinsekade/weg. Daarom heb ik gisteren online een melding gedaan bij de gemeente.

De gemeente <a href="http://www.denhaag.nl/">Den Haag</a> heeft daar een <a href="http://www.denhaag.nl/home/bewoners/loket/to/Storing-verkeerslicht-melden.htm">speciale pagina</a> inclusief formulier voor met, in ieder geval voor mij, duidelijke keuzes en vrij vlot was de melding gedaan. Want wat is het probleem op die kruising? Doordat twee lichten niet goed op elkaar aansluiten sta je heel snel weer voor een rood licht. Dus rijden mensen door het rood of blokkeren ze de tram.

Omdat we allemaal wel eens beïnvloed worden door de bekende vooroordelen over <a href="https://www.google.nl/search?q=vooroordelen+ambtenaren">ambtenaren</a> en gemeenten had ik niet direct een reactie verwacht. Maar binnen een paar uur werd ik teruggebeld! De beheerder van die kruising, ik ga er vanuit dat hij daar geen dagtaak aan heeft, aan de lijn. Zij hadden het probleem al een paar weken eerder onderkend bij een routinecontrole. Er ontstonden hierdoor af en toe inderdaad onwenselijke en soms gewoonweg gevaarlijke situaties. Een snelle oplossing ter plekke had niet geholpen, maar deze week wordt er een aangepaste regeling in gebruik genomen. Hij hoopte dat alles in één keer zal werken, maar als het eind volgende week nog niet opgelost was of ik dan weer contact wilde opnemen.

Een heel prettig gesprek met die meneer en een hele snelle reactie. Complimentje verdient. En ik nu hopen dat het probleem snel is opgelost met een nieuwe programmering.
