---
layout: post
title: Waarom nog steeds de aangepaste dienstregeling?
date: 2013-01-24 12:47:00 +01:00
lang: nl
tags:
- openbaar vervoer
---

Als dagelijks forens van Den Haag Centraal naar Zoetermeer ben ik best een tevreden klant van de NS. Totdat er sneeuw gaat vallen, want dan raken ze het spoor al snel bijster.

Dan wordt er overgeschakeld op de aangepaste dienstregeling. Ze hebben zelfs een <a href="http://www.youtube.com/watch?v=tuthEbPRAFw">filmpje</a> gemaakt met uitleg waarom dit nodig is. Van de genoemde 18% merk ik trouwens niets want op mijn traject wordt maar 50% van de ritten gereden. Ik vraag me af wat het percentage zou zijn als je iets meer zou filteren op spitstijden en drukke trajecten.

Nu snap ik best dat het nodig is om deze aangepaste dienstregeling in te voeren. Vorige week maandag en dinsdag viel er zoveel sneeuw dat we de <a href="http://www.nu.nl/binnenland/3003907/recordspits-sneeuwval.html">drukste ochtendspits</a> ooit hadden. Maar met de aangepaste dienstregeling was mijn rit prima te doen. Bij de HTM was het een chaos, maar de <a href="https://twitter.com/FraCaMen/status/291201878103044096">NS reed</a> en dat terwijl in de Haagse regio zo'n 20 cm sneeuw was gevallen.

Afgelopen vrijdag (dus na 3 dagen aangepaste dienstregeling) werd er weer teruggeschakeld naar de normale dienstregeling. Alles weer voorbij zou je dan denken, maar niets is minder waar. Zondag viel er nog wat sneeuw, maar zeker niet de hoeveelheden van eerder die week. Dus op maandag ging de NS weer over op de aangepaste dienstregeling. Weer extra opletten hoe laat je vertrekt. Onhandig, maar het zal wel nodig zijn.

Heel naïef achteraf, dacht ik: vorige week konden ze na 20 cm sneeuw na 3 dagen weer de normale dienstregeling rijden, dus zo lang zal het nu kan het nu niet gaan duren. Zelfs de HTM rijdt weer de gewone dienstregeling. Maar helaas blijkt de praktijk anders. Want vandaag rijden ze nog steeds diezelfde aangepaste dienstregeling. De <a href="http://www.rover.nl/">reizigers</a> hebben het <a href="http://www.nu.nl/binnenland/3011734/treinreizigers-aangepaste-dienstregeling-beu.html">ermee gehad</a>.

En wat doet de NS dan om zijn betalende klanten het nog een beetje naar de zin te maken? Vanmorgen komt de trein vanuit Utrecht binnen. Dit is een dubbele trein en deze is erg vol. Maar toen de trein leeg was werd de trein gesplitst! Alleen het voorste deel ging een half uurtje later in de richting van Utrecht. Dus de NS heeft gewoon voldoende materieel en creëert gewoon een overvolle trein door het niet in te zetten! Welke kantoorklerk heeft bedacht dat om 8:57 een enkele korte trein voldoende is??? Je had nog net geen schoenlepel nodig om de deuren dicht te krijgen.

Nu weet ik dat het <a href="http://www.hpdetijd.nl/2013-01-15/de-winterellende-op-het-spoor-is-een-bewuste-keuze/">allemaal keuzes</a> zijn om niet teveel uit te geven aan spoorwegonderhoud en dat een paar weken gedoe daar het gevolg van is. En dat goedkoop vaak duurkoop blijkt te zijn, heeft de Fyra natuurlijk weer aangetoond. Maar waarom moet het nu zó lang duren. Als vorige week na 3 dagen de normale dienstregeling wel kon, waarom nu dan niet?!?

_Update vrijdag 25 jan:_
Vandaag wordt weer de normale dienstregeling gereden.
