---
layout: post
title: Darktable
date: 2013-01-15 19:02:00 +01:00
lang: nl
category: fotografie
tags:
- fotografie
- darktable
---

Sinds een tijdje ben ik wat meer bezig met fotografie. Afgelopen zomer kochten we een nieuwe camera, een <a href="http://www.canon.nl/For_Home/Product_Finder/Cameras/Digital_SLR/EOS_1100D/">Canon EOS 1100D</a> inclusief een <a href="http://www.tamron.com/nl/photolens/di_II_all_in_one/a14.html">Tamron 18-200</a> lens.

Een leuke instapcamera voor een prima prijs. De verschil met de PowerShot SX1 die we hiervoor gebruikten is groot. De verwisselbare lens en de snelheid waarmee de camera bijvoorbeeld scherp stelt zijn absoluut pluspunten.

Als ik eropuit ga om foto's te maken, dan schiet ik in ieder geval in <a href="http://nl.wikipedia.org/wiki/RAW_%28bestandsformaat%29">RAW</a>. Het grote voordeel van RAW is dat het feitelijk een digitaal negatief is. Vrijwel alle informatie die zoals de camera die had tijdens de opname zijn erin opgeslagen. Dus je kunt nog de witbalans aanpassen of helderheid. Maar het nadeel van RAW is dat het over het algemeen niet rechtstreeks te gebruiken is. Daarvoor heb je software nodig die de RAW kan converteren naar <a href="http://nl.wikipedia.org/wiki/Joint_Photographic_Experts_Group">JPEG</a>. Een populair programma hiervoor is <a href="http://www.adobe.com/nl/products/photoshop-lightroom.html">Adobe Lightroom</a>. Het grootste nadeel hiervan is dat het een betaald product is, al is het niet super duur. Daarbij opgeteld dat mijn eigen pc nog XP heeft en dus te oud is om Lightroom te laten draaien is dat geen optie.

Dus ben ik op zoek gegaan naar een alternatief voor Lightroom. Na diverse producten bekeken te hebben heb ik een poging gewaagd om naar <a href="http://www.darktable.org/">darktable</a> te kijken. Dat was nog niet zo heel eenvoudig, omdat het programma niet onder Windows draait, maar alleen op Linux en Mac OSX. Dus mijn oude Linux-live USB-stick afgestoft en daar de recentste versie van <a href="http://www.ubuntu.com/">Ubuntu</a> opgezet. Nu heb ik in ieder geval een testomgeving. De <a href="http://www.darktable.org/install/#ubuntu">installatiestappen</a> uitgevoerd en ik had darktable draaiend.

Aangezien ik nog nooit met dit soort software gewerkt had, eerst <a href="http://www.darktable.org/resources/">diverse video's</a> bekeken om het concept van het programma te leren begrijpen. Want het werkt toch anders dan meeste andere programma's. Het is niet direct fotobewerking. Het is ook organiseren en beheren van de foto's.

Nu heb ik van het begin af aan al mijn foto's gestructureerd opgeslagen op mijn pc. Een directory per jaar en daarbinnen directory's per dag, met een korte omschrijving in de directorynaam. Zo heb je altijd een chronologisch overzicht van je foto's. Maar er hangen bijvoorbeeld geen tags of labels aan foto's. Dus even snel zoeken naar alle foto's met "zon" als onderwerp is niet mogelijk. Dit kan in darktable wel, maar dat moet je uiteraard eerst voor alle (?) foto's invoeren. Een flinke klus dus.

En tot nu toe ben ik best enthousiast over darktable. Het werkt prettig en ook het bewerken van de foto's met de diverse filters werkt vrij eenvoudig. Het grootste nadeel blijft dat ik het niet onder Windows kan draaien. Iedere keer als ik het wil gebruiken moet ik opnieuw opstarten met mijn USB-stick. Misschien dat ik me eraan waag om te proberen het programma zelf te compileren, maar ik weet niet of ik die tijd erin wil steken. Wie weet "wordt vervolgd".
