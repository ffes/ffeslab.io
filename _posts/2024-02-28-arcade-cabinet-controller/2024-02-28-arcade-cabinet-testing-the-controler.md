---
layout: post
title: Testing the controller
series: Building an arcade cabinet
date: 2024-02-28 22:24:53 +01:00
lang: en
tags:
  - RetroPie
  - retro gaming
  - arcade cabinet
---

Earlier this month I [ordered the parts for the controller]({% post_url 2024/2024-02-03-arcade-cabinet-researching %}) for my arcade cabinet.
This week everything arrived and I took a good look at what I ordered.

As I mentioned I ordered two joysticks, one to replace the d-pad and one to act as an analog stick.
The control board that I obviously need also came with two controllers styled as analog sticks.
A set of buttons made the order complete.

This all should be able to act like the average PlayStation or Xbox controller.
So that is the first thing to test.

For some reason the controller board didn't come with a description what all the connectors are used for.
So I had to downloaded a set of images from the website to know where each button should be plugged into the controller board.

First I connected the controller board with the provided USB-cable to the pc.
A blue led started to shine, so the controller board does something.

![Testing the controller](controller.jpg)

Let's start simple.
I went through all the cables that came with the controller board.
First I connected one cable to the `A` button and another cable to the `Select` button.
With the downloaded images I could connect the buttons to the controller board.
Now that these two buttons are connected I should be able to so some configuration.

With the PlayStation 3 controller I already have connected to the pc I started to configure the input controls in RetroPie.
To configure your new input device RetroPie ask you to press a button on the controller.
When I pressed the `A` button the configuration editor started.

Yeah, the basics work!

With the PS3 controller I went to the button that should become the `A` button, pressed the button and it was excepted.
Then I used the PS3 control again to go to the `Select` button, pressed it and it worked as well.
So far so good.

I exited the configuration editor and I could now go on with the joysticks.

Going through the cable, I fount out that there are enough cables for all the buttons, and the two analog sticks that come with the board.
The analog stick replacement joystick, with the white ball, has its own cables and I could also connect that one and configure it.
But the cables needed for the traditional joystick, with the red ball, that should replace the d-pad, were not there.
After searching a bit online I found those as well and ordered them immediately.
These cables shouldn't take too long to arrive.

It is really great to see that everything I order works as expected, especially the analog sticks.

Next step will be to design the layout how these joysticks and buttons should be drilled into the cabinet I am going to order.
I need to contact [the shop](https://arcade-expert.nl) I want to buy my stuff to ask the questions I have.
