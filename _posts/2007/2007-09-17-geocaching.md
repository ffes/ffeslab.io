---
layout: post
title: Geocaching
date: 2007-09-17
lang: en
tags:
  - geocaching
comments:
  - comment:
    author: André
    date: 2007-09-23 08:58
    text:
      - Yes, Geo is nice to have. When the weather, the enviroment and te people you are wits it can give great fun. We have read about it en also will try this gimick.
      - Greetings, André
---

Yesterday we had a [Garmin GPS-60](https://www.garmin.com/nl-NL/p/6446) at home.
We had hired the gps for a very nice day with my colleague's at [De Hoge Veluwe](https://www.hogeveluwe.nl).
And since I had to return today, Menno and I went out to try and find a [geocache](https://en.wikipedia.org/wiki/Geocaching).
I was our first time.
We did a simple [multi-cache](https://coord.info/GCMFKF) nearby our home.
The weather was great, the route was nice, but unfortunately we could not find the cache itself.
It was hidden somewhere in the the bushes and since we had to be somewhere in time, we simply didn't have enough time.
But apart from that it was quite fun to do.
So when we have access to a gps again we will definitely do one again.
