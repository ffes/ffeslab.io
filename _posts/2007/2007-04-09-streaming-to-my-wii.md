---
layout: post
title: Streaming to my Wii?
date: 2007-04-09
lang: en
---

As you may know, since the [beginning of February]({% post_url 2007/2007-02-02-ladies-and-gentlemen-wii-got-him %}) we are the happy owners of a Nintendo Wii.
Great fun!
Love to play Bowling, love the Virtual Console version of Mario Kart 64, but unfortunately it lacks the Media Center ability.
Luckily it has a Opera Internet browser that supports Flash.
So you can listen to "radio" via WiiHear or FineTune.
But there is no easy way to listen to my own collection of MP3 files that is on my PC.

What I found so far is Red5.
This is a Open Source replacement of the official Flash streaming server and it allows you to stream media.
But things are not that easy.
When I want to stream my MP3 files to my Wii, I need to have a Red5 server plugin (written in Java) that streams the data and I need a Flash application (written in ActionScript) that plays this streaming media and allows me compile my own playlist.
As far as I know they are both not available at this moment.
It still has to be written. [:-(]
When I think of it, I know this is too complicated for me to do at this moment.
I have no experience with either Java nor ActionScript, and other projects need my attentions as well (like [Dorgem](https://dorgem.sourceforge.net/)).
And even the simple broadcast demo that comes with Red5 (it streams data from my TV card to my local browser) runs fine on my PC, but does not work on the Wii.
So things are not as easy as they seem.

But when you look at the demos of Red5 and the user experience I have with WiiHear, I know it must be possible to create it, all with 100% open source, platform independent, solutions.
All you need to do is write that Java plugin for Red5 and the Flash application [:-S]
