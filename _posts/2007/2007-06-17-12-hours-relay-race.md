---
layout: post
title: 12-hours relay race
date: 2007-06-17
lang: en
category: hardlopen
tags:
  - hardlopen
  - wedstrijd
  - verslag
---

Yesterday was a new chapter in "my life as a runner".
I participated in a 12-hours relay race.
This means that the team I was part of has to run a relay for 12 hours.
The lap size was 1851 meter.
Luckily I was not in best team (in fact we ended second last) so it took me and my five team mates about 10 minutes to complete the lap, so there was enough time to recover.

It was a great event.
The event was organized by a [local athletics club](https://www.haagatletiek.nl).
There was 12-hours run and a 6-hours run and you could enter as team or as a solo runner.
When you enter as team for the 12-hours your team has 6 members and for the 6-hours the team has 4 members.
There were around 30 teams and there were quite a lot of those [ultra runners](https://en.wikipedia.org/wiki/Ultrarunning), for the 6-hours as well as for the 12-hours.

We had joined with 2 teams, Mile 17 and (my team) Smile 17, both sponsored by [Mile 17](https://www.mile17.nl).
Mile 17 were in 5th place for quite some time, but because of some injuries they ended in 6th place.
And as said Smile 17 ended 11th of the 12 teams.
But for 5 of the 6 team member it was the first time and those 5 all started running about 6 months ago, so quite an achievement to finish anyway.
The team spirit was high.
I didn't need the massage by our masseur Jan but he was needed by others.
The catering was great, thanks Yvonne!
And who knows, maybe next year another try.
