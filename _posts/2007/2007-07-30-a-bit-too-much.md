---
layout: post
title: A bit too much?
date: 2007-07-30
lang: en
category: hardlopen
tags:
  - hardlopen
  - training
---

Saturday I couldn't run the regular training because we had to pickup Menno from his [Scouting](https://www.scoutingferguson.nl) Camp.
He had a great camp! They had to help get rid of ghosts and needed help from a [Ghostbuster](https://en.wikipedia.org/wiki/Ghostbusters).

Yesterday some people of [KèkWèk](https://www.kekwek.nl) did a endurance training.
The plan was to run from the center of The Hague to Clingendael and Meijendel and back.
To Clingendael and back was 8 km, all the way to Meijendel and back was about 17 km.
My initial plan was to run the 8 km route, but at the split I decided to stay with the group for a little while and return somewhat later.
But before I knew it, we were at the furthest point in Meijendel.
Then there was no way to return earlier.
I had to go all the way and that meant 17 km.
But I never had run more then an hour or more the 10 km.

The last kilometers my legs did hurt, there was not much speed anymore and had to walk a bit more but I did complete the 17 km.
Yesterday evening my muscles did hurt a lot less and today it is almost gone.
So after all, I'm quite pleased with the run. It was a bit long, but apparently not **too** much!
