---
layout: post
title: Dorgem very-pre-alpha released
date: 2007-04-29
lang: en
category: software
tags:
- dorgem
comments:
  - comment:
    author: Zann
    date: 2007-04-30 20:51
    text:
      - Awesome news, Frank! I will give the alpha a good try as soon as I have a chance.
---

I just released a "very-pre-alpha" version of [Dorgem](https://dorgem.sourceforge.net/)3.
Know that this is version is **absolutely** not finished.
It is more a prove of concept then a workable version.
It is easier to tell what is done and not tell what still needs to be done:
- Running on Win32, only tested on Windows XP
- You need to configure it, by editing an XML file in your profile directory
- A file storage is available.
- Bitmap and text captions are available.
- It still uses Video for Windows to talk to the camera.
- The date related text captions are using the `wxDateTime::Format()` function, which uses the `strftime()` function.

To test it, copy the Profiles.xml to your personal profile directory: `C:\Documents and Settings\USERNAME\Application Data\Dorgem` and edit it to fit your needs.
Start `Dorgem.exe`.
Running it will not effect your current Dorgem2 settings.
They are stored in the registry and Dorgem3 uses XML files to store the settings.

The next thing I am going to do is to implement the FTP storage.
As soon it is done I will release a new alpha version.

No support is given on this version, but I would like to hear what you think of it, given its known limitations!
