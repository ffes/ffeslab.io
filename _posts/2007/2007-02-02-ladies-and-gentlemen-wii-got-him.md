---
layout: post
title: Blogging with the Wii
date: 2007-02-14
lang: en
---

I'm writing this small entry with the Opera browser I just downloaded on our Wii.
It is a bit difficult to type with the on-screen keyboard but for this one time it is fun!
