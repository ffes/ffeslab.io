---
layout: post
title: Subscribed for my first 10 km
date: 2007-07-03
lang: en
category: hardlopen
tags:
  - hardlopen
  - training
  - Haarlemmermeer Marathon
---

Today I subscribed for my first 10 km run.
On 2 September I will be running in [Haarlemmermeer](https://en.wikipedia.org/wiki/Haarlemmermeer) (near [Schiphol Airport](https://en.wikipedia.org/wiki/Schiphol_Airport)) at the [Haarlemmermeer Marathon](http://www.haarlemmermeermarathon.nl/).
So now I really have to start to train.
After the [12 hours relay]({% post_url 2007/2007-06-17-12-hours-relay-race  %}) I didn't do much apart from the regular Saturday morning training with [KèkWèk](https://www.kekwek.nl).
Tomorrow a run to get the mileage.
