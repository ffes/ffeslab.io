---
layout: post
title: Streaming to the Wii
date: 2007-04-13
lang: en
---

As [I wrote before]({% post_url 2007/2007-04-09-streaming-to-my-wii %}), I want to use my Wii to listen to my MP3 files in the living room.
Yesterday the Opera browser for the Wii came out of beta and that helps a lot.
The startup speed has improved **a lot**)).
And now the oflaDemo of Red5 sort of works.
When I try to listen to a short MP3 file, it works.
When I listen to a longer MP3 file, it runs out of memory and crashes the browser.
But at least I know it works.
All I need now is a Flash player that does the job well.

Here is how it works so far (with a Windows Red5 installation):
- Install Red5
- Put some MP3s in webapps/oflaDemo/streams under the Red5 installation directory.
- Make sure the Red5 service is running (type `net start red5` in a command prompt)
- Make sure the firewall settings are right. Allow traffic on port 5080 (webserver) and 1935 (streaming server)

So much for the one time server setup. Let's move on to the Wii side.
- Open the Internet Channel on your Wii
- Browse to your PC at port `5080`. In my case I enter `192.168.2.100:5080`
- Click on the demos link
- Click on the oflaDemo.swf
- Adjust the URI. Replace localhost with the right IP address
- Press Connect
- Click on an MP3 in the list

If everything went right, you now should hear the music [:-)]
So when there is a decent Flash application and a proper HTML page, most of the steps above could be reduced to just opening a Favorite and then select the songs.
