---
layout: post
title: Spam on the comments
date: 2007-04-25
lang: en
category: website
---

Recently the spam started coming in on the comments. 
Therefore I changed the "enter comments" part of my weblog and it should no longer be possible to enter a comment without actually using the form.
For regular users there shouldn't be any difference, but bots should not be able to abuse the comments as easy anymore.
