---
layout: post
title: Running (Sun 14-Apr-2007)
date: 2007-04-14
lang: en
category: hardlopen
tags:
  - hardlopen
  - royal ten
  - training
comments:
  - comment:
    author: David RolandHolst
    date: 2007-04-14 18:46
    text:
      - Hi Frank,
      - I am trying your very fine Exuberant Ctags application, but I'm not sure it supports my modeling language, GAMS (see www.gams.com ). Can you please advise?
      - Sorry to use your blog for this, but the email address on the software docs seems dead ( fesevur@hetnet.nl).
      - Cheers, David
---

Since Caroline has an injury and won't be able to run for a while, I decided to take her place for the sponsor run for [her choir](https://voicesunlimited.nl/) during the [Royal Ten](https://www.royalten.nl/).
And as a result of that I also started to run with [KèkWèk](https://www.kekwek.nl).
So today I had my first training with them, and I'm quite pleased with my results so far.
I could keep up with the group without too many problems.
So next week I will be there as well.
Let's hope the weather will be as great as it is right now, sunny, 20°C and little wind!
But I need to have new running shoes because I have a small blister.
