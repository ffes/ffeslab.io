---
layout: post
title: Running through a shop
date: 2007-07-08
lang: en
category: hardlopen
tags:
  - hardlopen
  - wedstrijd
---

Today I entered a small run here in The Hague.
[Excelsior](https://www.xcelsior.nl) (a local running shop) organized a run last year and today was the second edition.
It's a small run, only 150 runners.
It is a small route in the town of just under 5 km.
You could run 5, 10 or 15km (1, 2 or 3 laps).
I ran the 5 km and it was great fun, because the start and the finish were in that shop.
You entered the shop through the storage and leave through the front door.

The route was nice.
Flat and through the city so good pavement, but halfway the lap was through a park and there was no wind at all, so a bit hot.
The summer here in The Netherlands is not that great (to say it mild), we are not used to a temperature of 22 C anymore [:-S]

I started a bit too fast, but I completed the lap in 25:30, a very decent time I must say.
But at the [10 km in September](http://www.haarlemmermeermarathon.nl) I must not start that fast or it will become a very long run.
