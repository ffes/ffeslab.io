---
layout: post
title: Ladies and gentlemen, Wii got him
date: 2007-02-02
lang: en
---

Yesterday we finally got the long awaited call, our [Nintendo Wii](https://en.wikipedia.org/wiki/Wii) had arrived.

So I left the office one hour earlier and when I arrived at the shop, Caroline and Menno were already there.
Unfortunately the Wii Play package didn't arrive, but the most important thing was there, our Wii.

After installing we played Wii Sports a large part of the evening.
I like Bowling and Golf best so far, Tennis still needs a lot of practice, Baseball is nice and Boxing I haven't really tried.

But the overall view so far... IT IS SO MUCH FUN!!!
