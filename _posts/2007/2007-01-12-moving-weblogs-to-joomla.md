---
layout: post
title: Moving weblogs to Joomla?
date: 2007-01-12
lang: en
category: website
---

As you may have seen, I have recently installed [Joomla](https://www.joomla.org) on our family website.
And I gave the site a new look.
Although... The old one... You could hardly call that a look.

As a result of that, I want to move our current (selfmade) weblogs to Joomla as well.

I am still looking for some good components or modules for a decent archive (at least like I have now) and a good working comment system.

And I need to convert my old entries (including the comments), but that seems a job that needs to be done manually.
Which should not be much of a problem.

So if anyone can advise a component, please let me know.
