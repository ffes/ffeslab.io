---
layout: post
title: Dorgem 3, finally started
date: 2007-01-17
lang: en
category: software
tags:
- dorgem
---

A couple of weeks a ago, I finally started working on [Dorgem](https://dorgem.sourceforge.net/) 3, which is basically is gonna be a rewrite instead of just an upgrade.
Obviously parts can be reused, but many things can't.
I am using [wxWidgets](https://www.wxwidgets.org) 2.8.0 and I use the wxVidCap component to talk to the camera.
I am using the Video for Windows implementation right now, since that is the only available way for Windows in that component at the moment.

Using wxWidgets gives me at least the opportunity to consider a Linux and MacOS version.
I do have [Gentoo](https://www.gentoo.org/) Linux running at home (in dual boot), but without X or Video4Linux at the moment.
And wxVidCap has only limited support for V4L, so much is to be explored in that field.

I have not decided yet on the GUI or running as service, so very little is done on that right now.
But that is no problem.
First there has to be a basic engine that does the hard work.

This basic engine must have at least one type of store event, at least one type of caption event and read and write its settings.
So far, the bitmap caption event is 70% done.
Only the reading and writing the settings needs to be done.
One of the nice things about using wxWidgets is that now automatically various popular file formats can be used as an overlay, including a transparent GIF.
You are no longer forced to use BMP and 'make' it transparent by choosing a color.

The file store event has been finished for 90%.
It can read and write its settings to a XML file in the user profile directory and but still needs to be tested thoroughly.

As you may know the previous versions of Dorgem were written with MFC.
It is a library I am very familiar with.
wxWidgets is said to be a library that has close resemblance with MFC, but still porting the code to wxWidgets is not as easy as I thought it would be.
And I haven't even come to the GUI part of it yet.
So much has to be learned.
