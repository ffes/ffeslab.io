---
layout: post
title: Dorgem 3 progress
date: 2007-03-01
lang: en
category: software
tags:
- dorgem
comments:
  - comment:
    author: Zann
    date: 2007-03-04 00:37
    text:
      - Hey Frank, good to know things are going good for you! Especially glad to hear that Dorgem's development is not abandoned. By the way, you may want to drop in the irc channel and update the topic or something - a lot of people think Dorgem is dead [:(] I try to convince them otherwise from time to time, but I am not in the channel 24/7 [:)] Peace, Zann
      - P.S. Running Dorgem as a service would be nice. I wonder if it is possible already with 3d party tools, like srvany or firedaemon...
  - comment:
    author: Frank
    date: 2007-03-04 13:41
    text:
      - Zann, I have changed the topic of the IRC channel as you suggested. It now also points to this blog. I will try to be more in the channel.
      - And about running as a service. I have tried it with the 2.x and srvany but for me it didn't work as expected. My webcam needs the preview screen to initialize properly and with a service that is not possible. You need to open the GUI for that and so the advantage of a service is gone.
  - comment:
    author: Ayush Saran
    date: 2007-03-15 12:24
    text:
      - Hey frank,
      - i found dorgem while searching google for a motion detection utility and also saw it on lifehacker.com which confirmed that its a great program, i love using it and feel very safe knowing its watching my room when im not home.
      - Im a web design student from San Diego,CA and id love to build you a website for dorgem or to update the one you have at dorgem.sourceforge.com
      - absolutely free ofcourse, im just looking for some work to add to my portfolio before i finish college and go looking for a job, it seems like all the information i would need is already on the internet i just need to redesign.
      - I can have it done in a week and setup a live demo on my servers for you to look at, if at all intrested plese drop me an email at ayushsaran+dorgem@gmail.com
  - comment:
    author: Nils
    date: 2007-04-02 21:52
    text:
      - Dorgem is a very nice little program. Glad to hear that it's still being developed. How is the DirectX capture progressing? I have some people who would like to use 1280x1024 resolution for capture, but can't because of the VFW part.
  - comment:
    author: Rick Byrd
    date: 2007-04-09 15:01
    text:
      - Thank you for providing Dorgem for free download. Dorgem is quite simple and stable compared to several others I have tested! I have really just begun testing Dorgem and hope to use it to display a current and previous picture on my website, perhaps by using the external program feature to call an FTP script that renames webcam pics, i.e. "webcam1.jpg". Or is there another way using tags? I will keep exploring Dorgem to find out!
---

The last days I have been making quite some progress in the development of [Dorgem](https://dorgem.sourceforge.net/) 3.
I'm still concentrating on the engine.
Nothing special is done on the GUI.
All development is done on Windows, but with multi-platform support and Unicode support in mind.

Things done so far:
- File Storage
- First steps for FTP storage
- Bitmap caption
- Text caption

Next things to do:
- Finish the FTP storage
- Read and write the settings to the Profiles.xml

After that important decisions need to be made about the program, like will it be service based?
