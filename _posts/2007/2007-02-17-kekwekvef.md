---
layout: post
title: KèkWèkVèf
date: 2007-02-17
lang: en
category: hardlopen
tags:
  - hardlopen
  - wedstrijd
  - verslag
---

Today Caroline ran the [KèkWèkVèf](https://www.kekwek.nl).
This was one of the runs she was preparing for to help [her choir](https://voiceunlimited.nl).
It was a bit difficult at the end, but she managed to do it in 39:01.
You can read her personal story in her weblog (if you understand Dutch).

**Caroline, you did a very good job!**

You can see the photos in our album.
