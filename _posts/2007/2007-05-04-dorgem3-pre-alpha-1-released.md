---
layout: post
title: Dorgem3 pre-alpha-1 released
date: 2007-05-04
lang: en
category: software
tags:
- dorgem
comments:
  - comment:
    author: Justin Pugh
    date: 2007-05-13 19:31
    text:
      - Wow! I thought that Dorgem died, I never found the link to this blog and the Sourceforge site had no mention of this. I'm so glad that Dorgem is being improved and updated, thanks for working on it still! I use it to upload my weathercam and I like it because it's free and very simple. It isn't cluttered with fancy graphics and it's very resource efficient. Plus it's free [:D] Keep up the awesome work.
---

Yesterday I uploaded a new (still) pre-alpha 1 version of [Dorgem](https://dorgem.sourceforge.net/).
This version has, compared to the previous very-pre-alpha, FTP storage, motion detection and a (not yet configurable) web server for still images.
The next step will be to put the storage in separate threads.

The sources are also available, as always under GPL2.
If anyone want to give it a try under Linux?
There is a never tested Makefile available.
I haven't had a change to test it myself yet.

And once again, be aware that this version can not be considered stable, but please try it out and let me know what you think.
Leave your comment here or send me [an email]({% link about.html %}).
