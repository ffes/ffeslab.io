---
layout: post
title: Haarlemmermeer 10 km
date: 2007-09-02
lang: en
category: hardlopen
tags:
  - hardlopen
  - wedstrijd
  - verslag
  - Haarlemmermeer Marathon
---

Today I ran my first 10 km race in [Hoofddorp](http://www.haarlemmermeermarathon.nl/).
The weather was great.
Menno first did his 1,1 km run for children and after that André and I started for the 10 km.
And we both did very well.
André did finish in 55:44 and I completed my first 10 km in 58:11.
So I'm very pleased with it.
For a full review in Dutch look at the site of [KèkWèk](https://www.kekwek.nl).
