---
layout: post
title: Laan van Meerdervoortloop
date: 2007-10-28
lang: en
category: hardlopen
tags:
  - hardlopen
  - wedstrijd
  - verslag
  - laan van meerdervoortloop
---

Today was my second 10km run.
This time at the [Laan van Meerdervoortloop](http://www.lvml.nl/).
A run along the longest avenue of The Netherlands.

The conditions were a bit tough because there was quite some wind.
Since it was in our home town, there were a large number of people of [KèkWèk](https://www.kekwek.nl) that took part in this run, so some nice group photos have been taken.
Overall it was a very pleasant run.
I did 5:30 over each of the first four kilometers and was at the 5 km point in 27:49.
After that I could keep a good pace and did finish in 56:22.
So a personal record on the 10 km!

While I was running my 10 km, Menno participated in the Kidsrun of 1500 m.
And he did very well!
He finished in the top ten, short after the third place winner of the boys.
So maybe he has a talent and he should do something with it.
