---
layout: post
title: Map My Run
date: 2007-06-07
lang: en
category: hardlopen
tags:
  - hardlopen
comments:
  - comment:
    author: Erik
    date: 2007-06-07 16:48
    text:
      - And why, oh why.... don't you post the map in your blog? Just like I did... [;-)]
---

Today my colleague Erik showed me [Map My Run](https://www.mapmyrun.com/).
It is a site where runners can map their runs (the name says it all :-)) on Google Maps.
I just entered yesterday's run and it turned out that I had run 7.39 km.
