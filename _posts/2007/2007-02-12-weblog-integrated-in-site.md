---
layout: post
title: Weblog integrated in site
date: 2007-02-12
lang: en
category: website
---

Yesterday I have rewritten my weblog to let it integrate better with the new Joomla site we are using. Hope you like the result so far.

Some things don't work for the full 100% but I will leave it as it for now. I think I will switch to Joomblog when the database of my web space is upgraded to a newer version of MySQL. The things not working are:
- It is not possible to enter comments. Maybe this will be fixed later.
- The RSS-feed does not open the full site, but only the article. That is because a Joomla wrapper is used (which uses iframe). I didn't want to write a real component, so we will have to live with that for now.

Also know that the RSS feed has moved. A placeholder with a warning is put in place and points to the new RSS feed.
