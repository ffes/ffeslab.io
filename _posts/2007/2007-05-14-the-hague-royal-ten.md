---
layout: post
title: The Hague Royal Ten
date: 2007-05-14
lang: en
category: hardlopen
tags:
  - hardlopen
  - royal ten
  - wedstrijd
  - verslag
---

Yesterday was the big day for [Voices Unlimited](https://voicesunlimited.nl/), Caroline her choir: the sponsor run at the [The Hague Royal Ten](https://www.royalten.nl/).
And so I had to run my 5 km to raise money for their concert.

I had set myself the goal to run it within 30 minutes.
The race went really well and I finished at about 28:35.
That was faster then I had expected!
I knew a time under 30 minutes would be possible, but this is a nice surprise.
Since it was an official run with a chip, today I could see my official time on the site: **28:22**.
I will put some photos in the album.

Some people would only pay their promised sponsor fee if I would finish within those 30 minutes.
My brother André even promised a bit extra for every ten seconds faster.

After this fine result I will set myself a new target.
I think I will run a 10 km somewhere in the autumn.
So now I have to admit I like this running.
