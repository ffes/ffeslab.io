---
layout: post
title: Dorgem 3 progress (01-Jul-2007)
date: 2007-07-01
lang: en
category: software
tags:
- dorgem
comments:
  - comment:
    author: Gerard Aders
    date: 2007-10-01 18:46
    text:
      - Stumbled on your private site while looking for updates on Dorgem.
      - Like to let you know that I have been using Dorgem for my home automation system since last year (Basis = Homeseer) Communication with Dorgem is thru making (Homeseer) and deleting(Dorgem) files. The filename is the command.
      - Still it works great. I use 2 cameras, one at the front door that logs visitors and one in the room. Both ca be viewed fom my PDA when travelling.
      - I did choose Dorgem from several open and commercial sources since it uses only a small amount of CPU time, and it is well documented. I can adapt it to my own use (while not being a C++ programmer).
      - Thanks for your efforts.
---

Overall it has been some very busy weeks. That  one of the reasons why it has been too quiet here about [Dorgem](https://dorgem.sourceforge.net/).

What I did on Dorgem is almost invisible for the users, and turned out to be quite a job.
I stopped using TinyXML and now use the xmXML classes that come with [wxWidgets](https://www.wxwidgets.org) 2.8.
Not that I don't like TinyXML or that it is too limited.
No, TinyXML does a wonderful job, but I just didn't see any need to have two XML capable libraries in my program.

Another thing I'm working on is to make the store event threading.
That means that the UI doesn't hang when an image is being stored on the FTP server.
Threading is something I didn't use much before, so a lot of thing need to be learned.
When this is done, I will release a new pre-alpha version.
