---
layout: post
title: Fort Apocalypse
date: 2007-03-18
lang: en
tags:
  - retro gaming
---

Yesterday I spend a large part of the evening playing a 25 year old game.

When I was in secondary school I owned a [Atari 800XL](https://en.wikipedia.org/wiki/Atari_8-bit_family) home computer and one of my favorite games in that time was [Fort Apocalypse](https://en.wikipedia.org/wiki/Fort_Apocalypse).
It was the first game I ever finished, so good memories on that game.

Yesterday I ran into a site about games for home computers.
After searching (I had forgotten the name of the game) I found Fort Apocalypse.
And guess what, [Steve Hales](https://www.dadgum.com/halcyon/BOOK/HALES.HTM) (the original author) offers it at his [own website](https://www.igorlabs.com/classic-games/).
Yes, in that time (1982) a game was written by one person.
After I downloaded an [Atari emulator](https://atari800.github.io/) I spend a large part of the evening gaming the old fashioned way.

The story of the game is simple.
You need to rescue hostages in a cave with a helicopter and destroy the base inside that cave.
It is a shoot-em-up game.

Even after all those years I think it still is a good game.
Playing it with the numpad of a keyboard is a bit hard, but I still like it.
The game play is good, the graphics obviously are outdated, but for that time quite good.
The same goes for the sound effects.
Playing the game is not very easy, you really need to get used to it.

And I also found back some other games, like Pitfall II, Colony (that looks ugly now a days), Boulder Dash, River Raid and Zaxxon.
