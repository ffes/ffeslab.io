---
layout: post
title: NppFileSettings 2.0.0 released
date: 2022-10-21 23:17:17 +02:00
lang: en
category: software
tags:
  - nppfilesettings
  - notepad++
---

Today I released version 2.0.0 of [NppFileSettings]({% link nppfilesettings.md %}).

New features:

- Removed first line recognition, because that is in Notepad++ itself and renamed therefore the plugin to NppFileSettings
- Add 64-bit build
- Add support for vim options `filetype` and `syntax`
- Internal rewrite a lot of code

To download version 2.0.0, go to [download page on GitHub](https://github.com/ffes/nppfilesettings/releases/tag/v2.0.0).
