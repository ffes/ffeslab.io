---
layout: post
title: Using WordPress as a headless CMS
date: 2022-08-18 23:32:27 +02:00
lang: en
category: website
tags:
  - website
  - WordPress
  - Angular
---

Recently my wife and I went on holiday.
We traveled by train and stayed in various towns in Germany.
Since it was the first time we enjoyed our holiday this way, we decided we wanted to take our family and friends along with us through a travel blog.
Recently friends of us kept travel blogs as well and it was nice to experience their journeys as they traveled.

But for a nerd like me just opening an account on some blog site and link to that was not how I wanted to do this.
For years we have had a [website](https://www.fesevur.nl), but never really used it.
Use that site to host our blog was an obvious choice, but how would we write our content, where would the data be stored?

We were traveling by train with backpacks, so we had to travel light.
All we could use to write and post were our phones.
We wanted to be able to write during the day and we both wanted to write.
My wife is not a nerd so a static site generator and git integration to post was no option.
I use that for this blog on my own site.
But I can only post when I have a decent machine available and have no need (and therefore no option) to do that from my phone.

I investigated various options but most of them were not suitable for our travel blog.
Too expensive, too complicated, too many ads, not mobile friendly, etc.
We would be leaving in a week's time!

Back in 2019 I saw a [Jamstack talk](https://www.youtube.com/watch?v=g0OfnWCcBk4) that [WordPress has an API](https://developer.wordpress.org/rest-api/) and can be used as a [headless CMS](https://en.wikipedia.org/wiki/Headless_content_management_system).
In that talk, he said you could use WordPress this way for people who are already familiar with their backend.
He also suggested not to use it for new projects, because there are better headless CMSes out there.
But that talk was really focused on self hosted business websites and not a small personal blog like this project I planned to use it for.
I needed something easy to use and cheap.

[There](https://www.youtube.com/watch?v=rHNl5PZT0VU) [are](https://www.youtube.com/watch?v=a6Gou7ZVC-M) [many](https://www.youtube.com/watch?v=Ulo1dioXlsY) [examples](https://www.youtube.com/watch?v=YGhtEWeIKFo) on the internet on how to use WordPress as a headless CMS.
Could it be an option to (ab)use WordPress this way?
Then I saw that WordPress has a [decent mobile app](https://wordpress.org/mobile/) that we could use to write our content and upload the photos.
This started to sound like a plan.

But then again, WordPress, really?
Although I've never used WordPress, the stories I hear about it are not great.
It's slow, bloated and not very secure.
But most of this is related to self hosted instances with many plugins, as the guy in the Jamstack talk clarified.
I would just use [WordPress.com](https://wordpress.com) to write and get the posts from the API.
And so many sites are running WordPress.
They must be doing something right.
And not unimportant, time was running out.
A decision had to be made.

So I went ahead with this idea and [created a very simple]({% post_url 2022/2022-06-30-install-angular-in-a-container %}) [Angular website](https://angular.io/), added a component to host the (static) start page, added a couple of services and components to get the public posts from the WordPress API and show them.
Not very complicated.

We didn't need the comments, so no need to worry about security.
We would share links with our audience on WhatsApp and we were sure they would react there.
And I kept the layout very simple, just basic Bootstrap.
With a couple of small layout tips from our son, a webdev himself, it looked good enough for the job it had to do.
I put the [source code on GitLab](https://gitlab.com/ffes/fesevur-nl), used their CI to upload it to [Netlify](https://www.netlify.com/) and the site was up and running.

Writing the travel blog was really a two person effort.
While we were on the road, or should I say "on the tracks", we both used the WordPress app to write and adjust the text.
It is not possible to do that simultaneously.
You overwrite each other's content.
But when you are riding in a (crowded) train sitting next to each other, it is very easy to write together on one phone.

In this case I think using WordPress was the right choice.
The major plus is their mobile app!
It just works for our needs.
If you understand HTML the blocks you need to create make a lot of sense.
The Angular frontend gets the job done as well.

Our [travel blog](https://www.fesevur.nl/posts) was a success.
We liked documenting our journey.
It feels like it makes you more aware of your journey.
Our friends and family liked reading about it and they reacted as we expected.

All in all a nice little project.
I'll need to improve the layout a bit and only the 10 last posts are retrieved.
Maybe add support for categories and/or tags.
Still some work to do.
