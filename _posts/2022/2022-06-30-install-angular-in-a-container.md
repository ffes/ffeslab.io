---
layout: post
title: Install Angular in a container
date: 2022-06-30 22:17:05 +02:00
last_modified_at: 2024-11-26 19:27:13 +01:00
lang: en
tags:
  - Angular
  - Docker
  - tutorial
---

As a developer I try to install as few packages as possible and try to do as much as possible in Docker containers.
Otherwise I have a to install a lot of dependencies, that need to be updated.
Think of the dependencies for developing a website, like Ruby for building my Jekyll sites, .NET for the API-backend or Node for the SPA-frontends I build.

Today I wanted to build a simple Angular website to test something.
To just run `ng new` once I didn't want to install Node and the Angular CLI om my machine, so I tried to install the Angular CLI within a Docker container and it was surprisingly easy.

So first I created an empty directory on my machine and created a file `docker-run-node.sh` in it.

```bash
#!/usr/bin/env bash

docker run --rm -ti \
        -p 4200:4200 \
        --volume $PWD:/app \
        --workdir /app \
        --user $(id -u):$(id -g) \
        node:lts /bin/bash
```

This way I have an easy and consistent way to start my container.
I already forwarded port 4200, because I know I'm gonna need that.

Once the container was running, within it I ran the command to install the Angular CLI although not as a global tool.

```bash
npm install @angular/cli
```

First I had to delete `package.json` and `package-lock.json`.
They contain the information from installing the Angular CLI, but that was not needed anymore.
Since I am going to create a new Angular site, the relevant information in those files will come back soon.
If you don't delete these files `ng new` will fail.

Now I can run all the `ng` command I need as long as I prefix them with `npx`.
So running `npx ng new appName --directory ./` created the new Angular website for me.
To add a component I can run `npx ng g c new-component`.
