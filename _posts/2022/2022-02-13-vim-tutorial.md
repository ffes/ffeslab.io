---
layout: post
title: Vim, not as hard as it seems
date: 2022-02-13 15:59:46 +01:00
lang: en
category: website
tags:
  - website
  - vim
  - tutorial
---

Today I released the first part of a [vim tutorial]({% link vim.md %}) I wrote.

## History

On 29 February 2012 the first [Raspberry Pi](https://www.raspberrypi.org) was officially released.
I ordered my first Raspberry Pi on that day, but [it took till June to arrive]({% post_url 2012/2012-06-29-mijn-raspberry-pi-is-binnen %}).

In May 2012 [The MagPI](https://magpi.raspberrypi.org) was started as a community project and they were looking for people to write articles for their magazine.
That summer I wrote the first part of this tutorial as a potential series of articles for the MagPI, but the second part was never finished so I didn't bother to send it to them.

In the beginning of 2022 I found the original drafts of those articles again and thought it would be a shame not to publish the text I have written back then.
So I converted the text to markdown, removed the Raspberry Pi specific stuff and placed it [on my website]({% link vim.md %}).

A decent part of the next part of the tutorial is already written, and I still need to finish that.
But I thought, let's not wait any longer and publish the first part.
I can always improve it, add sections, do whatever is needed, but it has been laying around for too long.

## Disclaimer

First of all, English is not my native language so please report typos or grammatical errors.

I have been using vi, and later vim, since the late 1980s as my primary editor when using the command line on Unix and Linux.
But I don't claim to be a vim expert, more a long time user.
So suggestions, features I've missed, etc, [let me know]({% link about.html %}).
