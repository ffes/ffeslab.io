---
layout: post
title: Switching from Putty to OpenSSH on Windows and WSL
date: 2022-10-05 20:01:11 +02:00
lang: en
tags:
  - OpenSSH
  - Putty
  - WSL
---

I find myself using [Putty](https://en.wikipedia.org/wiki/PuTTY) less and [OpenSSH](https://en.wikipedia.org/wiki/OpenSSH) more and more.
The main reason I used to use Putty for many years was, obviously, to SSH into various machines.
But Windows has OpenSSH included for quite some time now, why not use that?
Well, habits is the main answer.
And configuration is also an answer.

## Converting Putty Sessions

In Putty I have a lot of sessions and the Putty Key file was to only thing I used for years.
Since I started using OpenSSH with git, I already had my key converted to the OpenSSH format.
But the sessions are a different story.
They are in the Windows Registry and OpenSSH uses a config file.
And without the sessions converted I couldn't really use OpenSSH in a practical way.

Obviously I am not the first that runs into this problem.
I quickly found [two](https://github.com/tothand/putty2openssh) [projects](https://github.com/abdrakhmanov/PuTTY-to-OpenSSH-Converter) on GitHub that could do the job.
These projects export the sessions from the Windows Registry and do some basic parsing on that file to get the most important settings.
And since my Putty sessions are not that special, just a bit too many to do it all by hand, they both should be able to do the conversion for me.

I went ahead with the python based converter, but that was still in Python 2 which is [End of Life](https://www.python.org/doc/sunset-python-2/).
Luckily the Python 2 docker images are still around, so I could use that to run the script without polluting my system.

```sh
docker run -it --rm -v $PWD:/usr/src/myapp -w /usr/src/myapp python:2 python parse.py > converted_putty_sessions
```

Now I had a list of all the Putty sessions in a file.
It needed to cleaned up a bit, but within half an hour I had them added `.ssh/config`.

## Using the same config on WSL2

Next problem is that I use OpenSSH in a Windows Command Prompt, Git Bash and on a WSL2-based Ubuntu.
The Command Prompt and Git Bash use the configuration in `C:\Users\Frank\.ssh` and WSL2 uses `$HOME/.ssh` which is in the VM.
But I don't want to maintain the config in two places.

Again I am not the first that runs into this problem.
In a [question on superuser.com](https://superuser.com/q/1183176) I found the suggestion to [mount the Windows `.ssh` directory in WSL2](https://superuser.com/a/1737216).
An easy one time setup and it just works.
The only cosmetic issue is that the Windows based OpenSSH uses CRLF line-endings in `known_hosts` and WSL2 uses LF's, so they both appear in that file but OpenSSH doesn't seem to mind.

Combining some things other people already dug into, helped a lot.
I still have Putty on my system, but have hardly used it anymore.
Only use Pageant every now and then in combination with FileZilla to do [SSH-FTP](https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol).
