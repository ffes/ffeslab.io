---
layout: post
title: Blog verhuisd
date: 2017-10-15 10:17:06 +02:00
lang: nl
category: website
tags:
- website
---

Ik weet dat mijn website, inclusief de weblog, de afgelopen jaren niet de aandacht heeft gekregen die het verdiend.

Afgelopen week heb ik de layout van mijn website voorzien van een nieuw thema. Recent was ik een aardige [layout](https://github.com/LordMathis/hugo-theme-nix/) tegengekomen die iemand gemaakt heeft voor [die andere static site generator](https://gohugo.io/). De tijd om afscheid te nemen van de "standaard bootstrap look" was gekomen. Dankzij het gebruik van de [static site generator](https://www.staticgen.com/about) [Jekyll](http://jekyllrb.com/) was dit vrij eenvoudig. Op basis van dat thema, met wat aanpassingen, heb ik de layout-template van mijn website aangepast en op wat kleine details na werkte alles weer.

De weblog draaide ik tot nu toe op [Google Blogger](https://www.blogger.com/), maar die wilde ik al een tijdje verplaatsen naar mijn eigen webserver. Zeker nu de layout van mijn website aangepast is, viel de weblog een beetje uit de toon.

Daarom "even" mijn bestaande blog [geconverteerd](http://import.jekyllrb.com/docs/blogger/). Dat was de eenvoudige stap, want daarna ervoor gekozen om de meeste pagina's van html om te zetten in markdown. Alleen de pagina's die plaatjes (of foto's) bevatten zijn html gebleven. De pagina's met foto's zijn nog niet klaar, want het kostte allemaal toch een stuk meer tijd. Maar het was het waard.

Het resultaat is een iets mooiere website en het weer zelf hosten van mijn weblog. Zoals je ziet heb ik ook weer eens een artikel geschreven voor mijn blog. Wie weet volgen er meer.
