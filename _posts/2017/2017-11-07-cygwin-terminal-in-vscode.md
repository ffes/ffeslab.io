---
layout: post
title: Using a Cygwin Terminal in VSCode
date: 2017-11-07 22:23:10 +01:00
lang: en
tags:
- vscode
- cygwin
- terminal
---

A little over a year ago I opened [an issue](https://github.com/Microsoft/vscode/issues/14977) for [Visual Studio Code](https://code.visualstudio.com/). 
Basically it was a suggestion to add documentation how to start a [Cygwin](https://www.cygwin.com/) terminal as integrated terminal.

The reason why I opened the issues was that it is not straightforward to use a Cygwin terminal in vscode.

The first thought was to simply start `bash.exe` from the cygwin `/bin` directory.

```json
{
  "terminal.integrated.shell.windows": "C:\\cygwin\\bin\\bash.exe"
}
```

At first everything looks fine, but the problem is that the login script is not started and for instance the path is not properly set.

```sh
Frank@pc247 /cygdrive/c/Source/fesevur-com
$ ls
bash: ls: command not found
```

So let's add the command parameter to make sure `bash` start as a login shell.

```json
{
  "terminal.integrated.shell.windows": "C:\\cygwin\\bin\\bash.exe",
  "terminal.integrated.shellArgs.windows": [ "--login" ]
}
```

Still not the result I wanted. Now the terminal works as expected, but it starts in the `$HOME` directory instead of the workspace directory of vscode.

```sh
Frank@pc247 ~
$ pwd
/home/Frank
```

Then I realized that the `chere` package in Cygwin had to solve this problem as well. And since I already had that package installed (I use it every day), I added these command options and now it works.

```json
{
  "terminal.integrated.shell.windows": "C:\\Cygwin\\bin\\bash.exe",
  "terminal.integrated.shellArgs.windows": ["/bin/xhere", "/bin/bash"]
}
```

Note that you don't have to use the `chere` command to add the `Bash here` to the context menu of the Windows Explorer. You only use the `xhere` helper command.

Also note that when you want to use another shell then `bash`, you can replace the `/bin/bash` in `terminal.integrated.shellArgs.windows` with the full POSIX path to your shell of choice.

I thought this would be helpful for others as well so I asked the vscode team to add it to the documentation, but they decided not to do that.
But still I think others will run into this issue as well. Since I more or less restarted my blog, so I decided to write this post about it.

While writing this article I searched and found [an article on Stack Overflow](https://stackoverflow.com/questions/46061894/vs-code-cygwin-as-integrated-terminal). This mentions another solution which probably doesn't require the `chere` package to be installed. This is an assumption. I don't have a machine where I run vscode and have Cygwin installed without `chere`. The difference with above is that an environment variable is set, which apparently has the same effect as starting `/bin/xhere`. Not sure if it works with other shells as well.

```json-doc
{
  "terminal.integrated.shell.windows": "C:\\cygwin\\bin\\bash.exe",
  // Use this to keep bash from doing a `cd $HOME`
  "terminal.integrated.env.windows": { "CHERE_INVOKING": "1" },
  "terminal.integrated.shellArgs.windows": [ "--login" ]
}
```
