---
layout: post
title: Google Ads
date: 2005-03-17
lang: en
comments:
  - comment:
    author: Erik
    text:
      - Mwahaaha! Coincidence or...? IkZieThee can be soooo cool...
  - comment:
    author: Michael
    text:
      - hahahah! wasn't there a website with more of these wrongly placed googled adds??
  - comment:
    author: Frank
    text:
      - I've searched quickly, but couldn't find it. Maybe google filtered it out [;)]
---

This Google Ad made me laugh:

![Invest in Aegon?](aegon.png)

Small summary of the main article:  
Billion dollar claim threatens Aegon (Dutch insurer) because
they should have mislead customers in their brochures.

Translation of the ad:  
Invest now in Aegon? A unique method for investors who hate to loose money.
