---
layout: post
title: Introduction
series: Football predictions in ASP.NET and Angular
lang: en
tags:
  - ASP.NET Core
  - Angular
  - REST API
  - Football predictions
---

I am about to start a project to improve my skills in [ASP.NET Core](https://dot.net) and [Angular](https://angular.io/).
I want to create a [football](https://en.wikipedia.org/wiki/Association_football) (or what some may call soccer) prediction game.

In recent years I hosted a very small prediction game for a very selected group of people, just for fun. It ran during a European or World Championship Football.
The old version is written in PHP and the layout really needs an upgrade very much.
As I want to improve my skills in ASP.NET Core and Angular I decided rewrite that prediction game from scratch, but probably re-use the database structure.
The next [European Championship Football](https://en.wikipedia.org/wiki/UEFA_Euro_2020) will start in June 2020, so I have a firm deadline but still quite some time.

The technology stack is chosen because that is what we use at the office and my skills are lagging behind. I need a project to get into it.

Certain things don't matter to me.
* Scalability will not be an issue since it is written for a very small group of selected people.
* I will most likely host it myself, so the cloud is not an issue at the moment.
* Multilingual is not important. All users will Dutch, so the UI will be in Dutch. The code will be in English.

Things that do matter.
* It should be [responsive](https://en.wikipedia.org/wiki/Responsive_web_design), probably using [Bootstrap](https://getbootstrap.com/).
* It must use a [REST API](https://en.wikipedia.org/wiki/Representational_state_transfer).
* It may become a [PWA](https://en.wikipedia.org/wiki/Progressive_web_applications), not sure yet.
* All the administrative tasks must be done from within the application. No manually editing the database.
* Both the World Cup and the European Championship must work. I want to be able to re-use the code two years later.

A task that will be much more work then actually needed for such a small groups of users and use period, but I see it mostly as a learning process.
The learning is more important then the end result.

The general idea of the game is to let the users predict the basic results for the first round (who wins or is it a draw).
Then they must predict the countries that make it into a certain round, so which 8 countries reach the quarter finals, and so on.
During the tournament the users can view everything, but cannot edit anything anymore.
The administrator enters the results after every match and the scores are updated live.

The Euro 2020 Qualifying tournament to decide which countries will participating be in the tournament is still in progress, so I will use the data from the previous tournament during the development.

I want to take you along this journey I am going to take. I will tell you about my ups and downs.
