---
layout: post
title: Upgrade to .NET Core 3.1
series: Football predictions in ASP.NET and Angular
lang: en
date: 2019-12-07 21:42:23 +0100
tags:
  - ASP.NET Core
  - Angular
  - REST API
  - Football predictions
---

With the [release of .NET Core 3.1](https://devblogs.microsoft.com/dotnet/announcing-net-core-3-1/) this week I upgraded my project to it.

## Upgrading the software

The upgrade on my Windows machine was easy. Upgrade Visual Studio to 16.4 and you automatically get .NET Core 3.1.

On my Linux machine it was a little bit more work, but all it took was typing the three commands below as root.
On Linux the whole process only took a couple of minutes. Much faster then the VS upgrade, although that contains much more then just .NET.

```
apt remove dotnet-sdk-3.0
apt autoremove
apt install dotnet-sdk-3.1
```

## Upgrading the project

After updating the software I could upgrade the project.
In `Football.csproj` there is an entry to specify the target framework

```xml
<TargetFramework>netcoreapp3.0</TargetFramework>
```

I changed the value to `netcoreapp3.1` and after a rebuild it was done.

## And what about Angular?

I noticed that the project uses Angular 8.0.0, and the current release is [8.2.14](https://github.com/angular/angular/releases/tag/8.2.14).
I also saw that Angular 9 is in a RC-state and today [RC5 was released](https://github.com/angular/angular/releases/tag/9.0.0-rc.5).
Since I am not doing any real Angular development at the moment I will wait for the release of 9.0 to upgrade Angular.

## Euro 2020 Draws

Last week were the [draws for Euro 2020](https://www.uefa.com/uefaeuro-2020/).
Most of the countries and matches are known now, so I can start working on filling the database with that data.
The last countries will be known in March when the play-offs are held.
It will result in some minor changes to the fixtures, but that should all be easily dealt with in the database.
