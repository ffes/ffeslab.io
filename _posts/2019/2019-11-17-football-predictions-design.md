---
layout: post
title: Creating a basic design
series: Football predictions in ASP.NET and Angular
lang: en
date: 2019-11-17 21:24:46 +0100
tags:
  - ASP.NET Core
  - Angular
  - REST API
  - Football predictions
---

A website or webapp needs to have a design. Since I am a developer and not a designer I will go for the default bootstrap look for now.
Maybe one day somebody will come with a real design.

## HTML

I need a couple of pages to be able to enter the predictions.

* A register form
* A logon form
* List of all the groups and of the knockout phases
* A form to enter the predictions of a group
* A form to enter the countries that make it to certain phase in the various knockout phases

These really basic designs are committed to the repository in the directory [_extras/design](https://gitlab.com/ffes/football/tree/master/_extras/design).

I don't think about the management forms at the moment. That will come at a later time.

## API

The REST endpoints need to be thought off. For now I think these endpoints will get the job done.
User authentication is not yet included.

### Prediction phase

All POSTs must be disabled during the tournament.

```
GET /input/groups
```

Returns all groups and per group if it has been entered completed.

```
GET /input/group/{groupid}

WHERE {groupid} IN [a..h]
```

Returns an array with all the matches in the given group.

- MatchID
- ISO code of the home playing country
- Name of the home playing country
- ISO code of away playing country
- Name of the away playing country
- Date (yyyy-mm-dd HH:MM:SS)
- City where the match is played
- Name of the stadium
- The users prediction of the game
  - 0 == Not predicted yet
  - 1 == Home playing country wins
  - 2 == Away playing country wins
  - 3 == Draw

Note that the country within the UK don't have a two letter country code. They are officially part of [ISO 3166-2:GB](https://en.wikipedia.org/wiki/ISO_3166-2:GB).
For this game I will create unofficial two letter codes for countries of the UK.

```
POST /input/group/{groupid}

WHERE {groupid} IN [a..h]
```

Post the user's prediction. An array with (all) matched in the group

- MatchID
- The users prediction of the game
  - 0 == Not predicted yet
  - 1 == Home playing country wins
  - 2 == Away playing country wins
  - 3 == Draw

### Knockout phase

```
GET /input/ko/{phaseid}
```

Returns an array with all the predicted countries in the phase
- ISO country code or null

```
POST /input/ko/{phaseid}
```

Posts an array with all the predicted countries in the phase
- ISO country code or null

## During the tournament

```
GET /play/standing
```

Returns the current standing with all users in ascending order

- UserID
- Name
- Nickname (Usually the first name)
- Points

```
GET /play/group/{groupid}
GET /play/group/{groupid}/user/{userid}
```

Return the users own predictions or the predictions of the given user.

`/play/group/{groupid}` is probably exact the same as `/input/group/{groupid}`, in which case it is not needed.
`/input/group/{groupid}/user/{userid}` may only work when the tournament has actually started.

```
GET /play/ko/{phaseid}
GET /play/ko/{phaseid}/user/{userid}
```

Return the users own predictions or the predictions of the given user.

`/play/ko/{phaseid}` is probably exact the same as `/input/ko/{phaseid}`, in which case it is not needed.
`/play/ko/{phaseid}/user/{userid}` may only work when the tournament has actually started.

## User information

```
GET /user
```

Return general information about the logged in user.

Maybe implement a `POST` for the user to change some of the information, but that has a low priority.

## Management

Only available for special users.
They must be used to enter the results of the played matches.

### During the group phase

```
GET /mngt/group/{groupid}
POST /mngt/group/{groupid}
```

Very similar to `GET /input/group/{groupid}` but it retrieves the matches in the group with the actual results.
`Result == 0` means that result of the match hasn't been entered yet.

### During the knockout phase

```
GET /mngt/ko/{phaseid}
```

Returns an array with all the matches in the given phase.

```
POST /mngt/match/{matchid}
```

- MatchID
- ISO code of home playing country or null
- ISO code of away playing country or null

### User management

Only available for special users to maintain all the information of the given user.

```
GET /mngt/user/{userid}
POST /mngt/user/{userid}
```

## General

```
/version
```

Returns the version of the API
