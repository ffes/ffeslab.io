---
layout: post
title: Create the database
series: Football predictions in ASP.NET and Angular
lang: en
date: 2019-12-16 19:17:24 +0100
tags:
  - ASP.NET Core
  - Football predictions
  - Fixtures
  - Database
  - SQLite
---

The draw for [Euro 2020](https://www.uefa.com/uefaeuro-2020/) is done and the schedule has been published by [EUFA](https://www.uefa.com/uefaeuro-2020/news/0254-0d41684d1216-06773df7faed-1000--euro-2020-all-the-fixtures/).
This means that the database can be created and filled based on this.

## Creating the database

In the early staged of the development [I will use SQLite]({% post_url 2019/2019-12-13-football-predictions-database-test %}), but when the site matures I need to use another SQL engine.
Those engines understand the same basic SQL syntax, but they all differ when it comes to the `CREATE TABLE` command.
Data types and the syntax to define foreign keys may differ.
I created a file to create the tables and a separate file to fill those tables.
When I'll create the final database I probably only need to adjust `tables.sql` for the specific database engine.
The `INSERT` commands are less likely to change.

I'll be needing Dutch content for my own use but for everyone who want to follow along or use the project themselves I created an English version as well.
Translations into other languages should be easy.

These files are [committed to the repository](https://gitlab.com/ffes/football/tree/master/_extras/database).

To create the SQLite database with these files use these commands:

```bash
sqlite3 database.sqlite < tables.sql
sqlite3 database.sqlite < 2020-fixtures-en.sql
```

## What's next?

Next I want to create the first API to retrieve the matches in a group: `GET /input/group/{groupid}`.

**Finally!**
