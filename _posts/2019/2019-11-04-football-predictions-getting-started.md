---
layout: post
title: Getting Started
series: Football predictions in ASP.NET and Angular
lang: en
date: 2019-11-04 23:26:06 +0100
tags:
  - ASP.NET Core
  - Angular
  - Football predictions
---

Today I made the first steps to create the new Football Prediction Game in [ASP.NET Core](https://dot.net) and [Angular](https://angular.io/).

I decided that I wanted to start the project on my Linux laptop, running Ubuntu 19.04.
Later I will definitly develop on my Windows 10 machine as well where I have [Visual Studio 2019 Community Edition](https://visualstudio.microsoft.com/vs/) already installed.

To develop on Linux I had to install the .NET Core 3.0 SDK. I simply followed the described steps on the [.NET website](https://dotnet.microsoft.com/download/linux-package-manager/ubuntu19-04/sdk-current).
That just worked.

For Angular development you need to have [Node.js](https://nodejs.org/) installed.
I already have the LTS version of Node.js installed, but if you don't have it just follow [the steps described on their website](https://nodejs.org/en/download/package-manager/).

In [Visual Studio Code](https://code.visualstudio.com/) I installed the [C# extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp).

With everything in place I created the new project.

```bash
mkdir Football
cd Footbal
dotnet new angular
```

That results is project that combines ASP.NET and Angular. Luckily this template was already updated to Angular 8. No need to worry about that.

Now I want can run the project for the first time.

```bash
dotnet run
```

When I opened `https://localhost:5001` I got an big warning about the self-signed certificate. For now I accept it. I need to investigate that later.

So far, so good. But these were the easy steps. I haven't even really started.

Digging a bit into the created project I found that the line endings for all the files are all in `CRLF` format, even though they were created on Linux.
And I know that I will use the Angular CLI, so I converted all the files in the `ClientApp` directory of `LF`.

This is done very easily on Linux with:

```bash
cd ClientApp
find . -exec dos2unix {} \;
```

Altough it is a very simple repository, I still pushed it to [GitLab](https://gitlab.com/ffes/football).

Now I need to think about the layout first. The layout of the old website is not usable.
Being a developer and not a designer that will be the first challenge.
