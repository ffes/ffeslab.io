---
title: Oude kaassalade zonder mosterd
date: 2024-10-20 15:31:58 +02:00
lang: nl
tags:
  - Recipes
---

Als groot liefhebber van oude kaas, houd ik ook van Oude Kaassalade.
Alleen vindt blijkbaar iedereen dat om de een of andere reden daar altijd mosterd in moet.
Maar ik ben geen fan van mosterd, om het voorzichtig te zeggen.
Dan blijft er niet zoveel keuze over. [:-(]

Bij de [kaasboer op het Sav](https://savlohmanplein.alexanderhoevekaas.nl/) hebben ze een lekkere Hollandse Kaassalade.
Deze is gelukkig zonder mosterd!
Maar als je die niet gekocht hebt, en je hebt wel trek in kaassalade dan moet je iets verzinnen.
Dus was ik al een tijdje op zoek naar een recept voor Oude Kaassalade zonder mosterd.
Die heb ik niet kunnen vinden, dus zat er niets anders op dan zelf iets proberen te maken.

Vanmorgen geëxperimenteerd met geraspte oude kaas om zelf een lekkere Oude Kaassalade te maken, met wat we in huis hadden.
In een aantal recepten was ik een combinatie van crème fraîche en mayonaise tegengekomen en ook wat ideeën welke kruiden erdoor te doen.
Je wilt namelijk wel iets dat de pit van mosterd vervangt.

Dat resulteerde in [dit recept](https://kookboek.fesevur.nl/oude-kaas-salade) in ons [familie kookboek]({% post_url 2020/2020-12-30-building-recipes-website %}) en een heerlijke lunch.
