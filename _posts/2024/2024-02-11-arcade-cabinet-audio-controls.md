---
layout: post
title: Researching audio controls
series: Building an arcade cabinet
date: 2024-02-11 15:18:16 +01:00
lang: en
tags:
  - RetroPie
  - retro gaming
  - arcade cabinet
  - arduino
---

In the [previous post]({% post_url 2024/2024-02-03-arcade-cabinet-researching %}) I mentioned that I want a way to control the volume.
During the day I probably don't mind putting up the volume a bit, but that is most likely not what I want later in the evening.

Most amplifiers you can buy at the specialized arcade webshops have a volume knob, but that is hard to build into a 18 mm wooden cabinet.
So I have to find a way to control the volume without using that knob on the amplifier.

The first thing that came to mind was to control volume just like my [Corsair K55 keyboard](https://www.corsair.com/eu/en/p/keyboards/ch-9226c65-na/k55-core-rgb-mechanical-gaming-keyboard-black-ch-9226c65-na) does.
It has three keys: `Volume Up`, `Volume Down` and `Mute`.
There must be a way to recreate that myself.
I would like to buy two regular arcade buttons for `Volume Up` and `Volume Down` and build them into the cabinet.
Hook those keys to a control board that emulates a USB-keyboard and connect that in the PC that is inside the cabinet.

Maybe I'll buy a third button for `Mute` but that can be decided later.
If I can make two buttons work, a third one shouldn't be a problem.

After searching a bit I found [various](https://www.instructables.com/Digispark-Volume-Control) [posts](https://www.sparkfun.com/tutorials/337) [and](https://youtu.be/jvHRfsgw4l8) [videos](https://youtu.be/yTc2GLXfCOY) that show how this can be done.
I don't need much fancy stuff because it will all be build in.
It just has to work.

All solutions use a small board that has a chip that is [Arduino](https://www.arduino.cc) [compatible](https://en.wikipedia.org/wiki/Arduino).
And almost all of them use a rotary knob, but that has the same problem as the original volume knob on the amplifier.
It is not easy to build that into a 18 mm cabinet.
So I need to apply the logic in those examples to my setup with arcade buttons.

The advantage of being Arduino compatible is that you can use the Arduino IDE to program the board and let that send the `MEDIA_VOLUME_UP` and `MEDIA_VOLUME_DOWN` commands to the computer.
Another advantage of these Arduino compatible boards is that they are cheap and broadly available.
We have an old [Arduino Starter Kit](https://store.arduino.cc/products/arduino-starter-kit-multi-language) at home that we bought for Menno when he was in secondary school.
This will make developing it a bit easier.

A lot of the research has been done for now.
First all the parts of the controller have to arrive, so I can build that and test with it.
If it all works as expected, I can design the holes that need to be drilled in the cabinet and order the cabinet.
