---
title: Recovering some images of the old blog
date: 2024-10-08 22:09:34 +02:00
lang: en
category: website
---

Last weekend, in a subdirectory far far away, I found back some images used in my [very old blog]({% post_url 2023/2023-08-16-recovering-my-old-blog %}).

So I updated 
[the]({% post_url 2004-10-19-time-machine-invented/2004-10-19-time-machine-invented %})
[three]({% post_url 2004-12-13-openoffice-org-spelling-checker/2004-12-13-openoffice-org-spelling-checker %})
[posts]({% post_url 2005-03-17-google-ads/2005-03-17-google-ads %})
where these images were used.

I think the last
[two]({% post_url 2005/2005-08-25-oops %})
[images]({% post_url 2007/2007-03-03-they-arrived %})
are lost forever. [:-(]
