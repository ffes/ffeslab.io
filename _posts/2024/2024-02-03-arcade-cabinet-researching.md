---
layout: post
title: Researching
series: Building an arcade cabinet
date: 2024-02-03 17:14:16 +01:00
lang: en
tags:
  - RetroPie
  - retro gaming
  - arcade cabinet
---

Last spring I started to use [a very old laptop for retro gaming]({% post_url 2023/2023-04-29-retro-gaming-on-a-very-old-laptop %}).
Back then it was a bit of an experiment, but it turned out I use it quite often.
So I decided to make the next step and investigate if I could build a real arcade cabinet.

In general, I know what I want.
I want a full size standing arcade cabinet that fits a normal 24" full-hd monitor.
And it should be able to play the games I was playing on the old laptop.

The first thing I did was start researching all the things that I need to know and need to buy.

## Cabinet

First I went on the internet to try and find out what the options are for the cabinet itself.

I quickly saw that in general there are two options.
Build from scratch or build a kit.

From scratch means you need wood (new or used), have a proper plan, tools and skills and do it all yourself.
There are [many](https://www.thegeekpub.com/arcade-plans-build-an-arcade-cabinet/) [examples](https://teveelvrijetijd.wordpress.com/2018/12/17/diy-mobiele-arcadekast-bijna-gratis/) of people using old [Ikea closets](https://ikeahackers.net/2019/12/retro-arcade-machine-diy.html), or with [new wood](https://www.instructables.com/A-Super-Easy-Arcade-Machine-from-1-Sheet-of-Plywoo/).
But I don't have the tools or the skills to do that myself.
So even though it seems very cool to build everything from scratch, that is not for me.
Know your limitations!

Building a kit is the option that remains.
I want to buy this kit in the Netherlands for several reasons.

- There are enough Dutch webshops that offer this.
  None of them are really local to me, but that is no problem.
  Shipping is very well organized here.
- When I want to ask something it is great to send an e-mail or pick up the phone and make that call in my native tongue.
- I like to help a Dutch business in the niche market of retro gaming and arcade building.

But it turns out that [only one webshop](https://arcade-expert.nl/) really offers what I want.
A full size cabinet large enough for a 24" monitor.

I need to order a [bartop](https://arcade-expert.nl/Wide-Body-Extended-&#40;WBE&#41;-Super-Custom-2-Player-Arcade-Bartop-Bouwpakket) and a [pedestal](https://arcade-expert.nl/WBE-DIY-Bartop-Onderstel-Bouwpakket-18mm-Gemelamineerd-MDF-Zwart) that fit nicely together.
When you order the bartop a variety of holes can be drilled for you so you need to know what you want before you order.


## Controller

The most important holes are the holes for the controller.
That means the joystick and the various button.

Right now I play with a USB connected PlayStation 3 controller that Menno didn't take with him when he left the family home.
And I really like to play the [Nintendo 64](https://en.wikipedia.org/wiki/Nintendo_64) game [Mario Kart 64](https://en.wikipedia.org/wiki/Mario_Kart_64).
That needs to be played with the [analog stick](https://en.wikipedia.org/wiki/Analog_stick) of your controller.
I soon found out that all the controllers that are available to built into an arcade cabinet only have one joystick per player and that joystick is a replacement of the [D-pad](https://en.wikipedia.org/wiki/D-pad) of your PlayStation controller.
And having to connect the PlayStation controller to an arcade cabinet just doesn't feel right.

After some searching I found one store on AliExpress that sold [a replacement for the analog stick](https://aliexpress.com/item/1005002244816632.html).
But back then I wasn't sure if I really wanted to build a cabinet, so saved the URL and want on with my life.

I also decided I don't need two set of controls.
Most of the time I will be the only player and when somebody comes to visit and wants to play along you can swap places or use the PlayStation controller as the second controller.


## PC

I already had decided a PC would be the hardware of choice.
Many people use a Raspberry PI for RetroPie, but I want Intel compatible hardware.
When I want to install something that needs an Intel processor, like DOSBOX or Wine, it is better to have the hardware instead of emulating it.
Since I want a full size cabinet there is enough space for a desktop PC.

Somewhere last autumn at the office an old Dell OptiPlex with a 3th generation i5 became available.
I was allowed to take it home and I used that as a replacement for the laptop I was using until then.

I replaced the harddisk with a 128 GB SSD and installed RetroPie on it.
I also ordered a new monitor to replace the oldest monitor I was using in my regular setup and connected that monitor to the OptiPlex.
The PC and monitor for the cabinet are ready for now.


## Audio

An arcade cabinet also need speakers.
Every PC has basic audio outputs and that is all you need.
But I want to be able to control the volume easily and that turns out to be a problem.

A lot of [amplifiers have a volume knob](https://arcade-expert.nl/Audio), but these knobs are not suitable to be build into a 18 mm MDF wooden cabinet.
They are designed to be used in a plastic case.
They are just not long enough to go through the wood.

I haven't figured out what the best solution is yet.
Maybe I can buy a USB keyboard controller, connect two arcade buttons and emulate the volume up and down buttons that are on my regular keyboard with that.
More research is needed.


## Ordering the controller

Since most of the research is done, I decided to make the first step and order the controller.
So I can design the holes I want to be drilled in the cabinet and order the cabinet itself.

And then I found out the AliExpress store that I found earlier was out of business.
At that time they were the only one to have a arcade equivalent of the analog stick.
Now what?

Turns out other stores on AliExpress do sell analog controller although there are not much of them.
But one is all I need, so after some searching and comparing I was able to order the controller I want.

- [A set of 2 analog sticks, including the control board](https://www.aliexpress.com/item/1005006257170963.html) to connect everything to. This analog sticks have a form factor similar to the ones in controllers.
- [1 regular arcade joystick](https://www.aliexpress.com/item/1005006280556388.html) to replace the D-pad.
- [1 arcade analog joystick](https://www.aliexpress.com/item/1005006257170963.html) that can replace one of the analog sticks that comes with control board.
  This will be the main analog stick to use.
- [A set of buttons](https://www.aliexpress.com/item/1005006365471571.html), including  `A`, `B`, `X`, `Y`, `Start` and `Select`.
  And in the set are also 4 buttons for the left and right buttons on your controller.
  They will be build on the side of the cabinet to act as pinball controls.

So everything is ordered to have the same controls as a PlayStation 3 controller has and it should arrive in a couple of weeks.

It was a little more expensive that I initially thought.
AliExpress shipping is much more expensive now a days (for all the right reasons).
And none of the stores had everything I wanted in one set, so I needed to order most thing separately.
But to quote a colleague of mine: A hobby may cost money.


## Artwork

My wife is [an artist](https://bycaroline.art) so I will not order any stickers or other artwork to put on the cabinet.
Once the cabinet is built, we will make the design ourselves and she will decorate it.
