---
title: Noorderlicht
lang: nl
date: 2024-05-11 12:02:07 +02:00
last_modified_at: 2024-10-11 09:02:57 +02:00
category: fotografie
tags:
  - aurora borealis
  - noorderlicht
---

Afgelopen nacht was er een hoogtepunt in zonneactiviteit, wat leidde tot zichtbaar [noorderlicht](https://nl.wikipedia.org/wiki/Poollicht) in Nederland.
Als zelfs [nu.nl](https://www.nu.nl/binnenland/6312397/zeldzame-zonnestorm-komt-naar-aarde-nachtenlang-grote-kans-op-noorderlicht.html) een artikel plaatst dan weet je dat je erbij moet zijn.
Nog even snel op [Noorderlichtjagers](https://www.noorderlichtjagers.nl) gekeken wat de waarden zijn.
Die zagen er veelbelovend uit!
Dus snel mijn spullen gepakt (dat wil zeggen vooral mijn [Gorillapod](https://joby.com/nl-en/gorillapod/) met houder voor de telefoon) en onderweg.

![De KP-waarden van vanavond](kp_mid.png)

Het oorspronkelijke plan was om [weer naar de Wassenaarse Slag]({% post_url 2023-02-27-noorderlicht/2023-02-27-noorderlicht %}) te gaan, maar daar staan de strandtenten weer.
En aangezien het een mooie dag was geweest, verwachtte ik daar nog veel licht.
Dan naar de parkeerplaats van [Meijendel](https://nl.wikipedia.org/wiki/Meijendel).
Dat ligt midden in de duinen dus voor de regio ook relatief weinig lichtvervuiling.

{% include map.html title="Meijendel" lat="52.13171" lon="4.35358" bbox="4.34896,52.12911,4.35788,52.13247" %}

Na ongeveer een half uur rijden stond ik rond 23:00 uur op de parkeerplaats, waar het nog rustig was.
Er stonden een paar auto's en bij een auto stond iemand een camera op een statief te plaatsen.
Zij waren hier duidelijk met dezelfde reden als ik.
Het bleek later een gezin met jonge kinderen te zijn, die toch maar uit bed hadden gehaald om dit niet te missen.

Inmiddels was het echt wel donker geworden, maar met het blote oog had je eigenlijk niet het idee dat er noorderlicht was.
Boven zee was het net wat lichter en een hele lichte zweem van kleur leek er wel in te zitten.
Dit had ik vorige jaar ook meegemaakt, dus gelijk een paar foto's te maken uit de losse hand om een idee te krijgen.

![Eerste foto](noorderlicht1.jpg)

Onmiskenbaar noorderlicht!

Helaas was er op de parkeerplaats geen geschikte paal waar ik mijn Gorillapod aan kon knopen.
Die stonden allemaal te dicht bij de struiken en dan bleef er te weinig lucht over om te fotograferen.
En om het noorderlicht vast te kunnen leggen is het nodig om de juiste instelling te kiezen.
Vandaag was het een ISO van 500 en een sluitertijd van 4 seconden.
Als je dan zonder statief schiet is de achtergrond wel bewogen, maar dat neem je op de koop toe.
Volgende keer toch het volledige statief in de kofferbak leggen.

Ondanks het gebrek aan statief, maakt mijn telefoon echt mooie foto's.
Diverse foto's volgden.
En omdat ik dit al eerder had gedaan en in het verleden met een spiegelreflexcamera heb gefotografeerd kon ik diverse mensen helpen met de instellingen op hun telefoon.

We zagen in de lucht echt wel kleurverschil.
Een groenige horizontale balk en een rode gloed waren met het blote oog echt zichtbaar.

Kort voor 12 uur vertrok het gezin, en kort na 12 uur begon het noorderlicht steeds feller te worden.
Het trok steeds verder omhoog en er kwam meer kleur in de lucht.
Langzaam zag je gekleurde banen ontstaan.
Dit was het moment waarop iedereen op de parkeerplaats stond te wachten.

**Zichtbaar noorderlicht, kippenvel!**

![Hoogtepunt van de activiteit](noorderlicht2.jpg)

Toen deze lichtshow bezig was stopte er een politieauto op de parkeerplaats.
De agenten vroegen of we wat zagen.
Ik probeerde ze te overtuigen dat ze uit moesten stappen, maar daar waren ze eerst wat terughoudend in.
Na enige overtuigende woorden aan de agenten stapten ze toch uit en hebben nog een tijdje foto's staan maken en gezellig staan kletsen.

Van de agenten hoorde ik dat het op de Wassenaarse Slag ook donker was, want de strandtenten daar waren al gesloten.
Dus dat is toch een optie voor de volgende keer.

![Hoogtepunt van de activiteit](noorderlicht3.jpg)

Rond kwart voor 1 uur werd het echt minder en om 1 uur vond ik het mooi geweest.
Het was een latertje geworden, maar ik was wederom blij dat ik gegaan ben.
