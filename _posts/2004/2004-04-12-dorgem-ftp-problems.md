---
layout: post
title: Dorgem FTP problems
date: 2004-04-12
lang: en
category: software
tags:
- dorgem
---

The last days has I've been a sort of busy with in the development of [Dorgem](https://dorgem.sourceforge.net/).
I've been trying to find out why I got some reports of users that they have problems with the FTP uploads.

I found out that it seems to be a WinINet FTP problem.
I've used WinInet FTP upload that I use in all versions so far.
When used for a small amount of transactions it works fine, but when heavily used it gives strange problems.
It hangs on different functions, sometimes on a `OpenConnection()`, other times on a `PutFile()`.

So I plan to move away from WinInet FTP for the next release.
Currently I'm looking for a open source C++ FTP class that works reliable and supports passive FTP.
