---
layout: post
title: New FTP client for Dorgem
date: 2004-04-17
lang: en
category: software
tags:
- dorgem
---

Currently I'm testing a new, non WinInet based, FTP client for [Dorgem](https://dorgem.sourceforge.net/).
The first tests look promising, but I haven't tested everything yet.
I hope to complete this somewhere next week.
When it is done, I will release a beta 5 for this.