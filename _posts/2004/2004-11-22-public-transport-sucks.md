---
layout: post
title: Public Transport Sucks!
date: 2004-11-22
lang: en
tags:
  - openbaar vervoer
comments:
  - comment:
    author: Michael
    text:
      - thank God for the bus [;)] this is exactly the reason why i've started taking the bus to work!! In the 2+ years that i've been travelling to Zoetermeer it's only been late twice, but i've never had to take another bus or go through another city to get where i need to go, unlike the NS....
      - I'd say go check if there's a bus from DHC/DHHS to Zoetermeer dude...
  - comment:
    author: ArSa
    text:
      - I like public transport... it always arrives on time, whatever that time is. So i can relax and do my own things... like thinking.
  - comment:
    author: Frank
    text:
      - Arsa, you're right. Normally I'm quite happy with the public transport, but last week it just was not nice.
  - comment:
    author: Kevin
    text:
      - The Sprinter travelling through Zoetermeer is in fact a real pain in the ass. I have to travel from Zoetermeer (Leidsewallen) to Rotterdam Hofplein, and I often suffer from delays. I've begun taking the bus instead, lately...
  - comment:
    author: Frank
    text:
      - Kevin, You're right. The last two weeks, the train back to The Hague was not one day on time!
---

I am so frustrated by the public transport.
First let me explain how my normal journey to the office is.
I go to the railway station on my bike and there I take the train to Zoetermeer and in a 2 minute walk I'm at the office.
On an average day this trip takes around 40 minutes.
Which is faster then the same journey by car in the rush hour.

But Thursday it was bad weather and I didn't go to the station on my bike, but with the tram.
I left home at 8:25, and I just missed the tram.
So I arrived at the station two minutes too late for my train.
I had to wait for the next train, which should go about 15 minutes later.
When it arrived it was announced that it would not travel, because there was a problem after Utrecht.
For those of you not familiar with Dutch topography, Utrecht is half way (60km) the train's journey and I only have to go to Zoetermeer (10km).
I don't understand they don't let the train go to Utrecht and back to The Hague.
I spoke a friend yesterday, who is a train driver and he was surprised as well but this.

But this meant I had to wait a total of 30 minutes for the next train that took me to Zoetermeer.
So finally at 9:45 I arrived at the office.
So a journey I normally do in 40 minutes, took me 80 minutes!

On the way back, the train was delayed for 20 minutes, and this also happened Friday.

And today a general power failure at the station in The Hague!
This happened when I just turned off the radio at home to get my bike and leave for the station.
At the station no one could tell how long it was gone take before the trains would ride again.
So after waiting a short time hoping trains would ride soon, but soon I decided to drive back home and work at home today, which I luckily can do.

When everything works as planned the public transport if ok, but the last...
