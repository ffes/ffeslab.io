---
layout: post
title: Entries got lost
date: 2004-09-22
lang: en
category: website
comments:
  - comment:
    author: Michael
    text:
      - Hmm doesn't sound good [:/] this means that they don't even run a backup everyday. I wonder how much time passes between their backups then actually if you've lost 2 entries... since you don't even update everyday 
---

Today I noticed that me last two blog entries were lost.
This is caused my a harddisk failure at my hosting provider.
They has to put back a tape backup which didn't have these entries.

Obviously comments added recently are lost as well.
Sorry, but there is not much I can do about.

I don't think I will re-enter these blog entries.
They both were about Firefox.
After switching from IE to Firefox, I noticed some things that I wrote about.
One was about the way the developers handled a feature request to add a Print item to the context menu.
The other one was about a page at mozilla.org the looked good in IE but not in Firefox.
