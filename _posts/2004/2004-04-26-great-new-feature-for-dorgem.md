---
layout: post
title: Great new feature for Dorgem
date: 2004-04-26
lang: en
category: software
tags:
- dorgem
---

Today I received an email of Petr Vacek.
He added [DevIL](http://openil.sourceforge.net/) to [Dorgem](https://dorgem.sourceforge.net/).
Currently I'm busy to integrate his changes into the current code base.

DevIL is a open source (LGPL) multi platform graphical library.
With this library is not too difficult to add support for all common used graphical file formats to Dorgem, for save and uploading as well as for the bitmap caption.
But this also opens possibilities to add effects to a bitmap like resizing, contrast and blurring.
But these effects won't be in the next release.
I first have to think of a way to handle this.
Maybe it will become just like the captions, unlimited effect that can be added to a store event or the bitmap caption.

I have to admit the FTP client problems has somewhat slipped my mind.
I accidentally ran into the freeware click-and-point adventure [Out Of Order](https://en.wikipedia.org/wiki/Out_of_Order_(video_game)) and have been playing it for the last week **a lot**.
It is a great adventure with a lot of humor.
I even think I will actually complete this game.
I'm not that much of gamer, but this game it fun!
Try it, it's free!
