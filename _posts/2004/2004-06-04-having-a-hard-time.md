---
layout: post
title: Having a hard time
date: 2004-06-04
lang: en
category: software
tags:
- dorgem
---

I'm having a hard time with the stability of the new [Dorgem](https://dorgem.sourceforge.net/) release.
One of Dorgems main (of you want to call it that way) features was its stability.
But for some reason the current version doesn't have it anymore.
I think, somewhere along the way a memory or resource leak has slipped in the program and I just can find it.
I even start to think the new FTP client wasn't necessary at all.

On a test machine at the office with a fresh installed WinXP, I installed the current beta.
I had a capture to a local file every 10 seconds.
After somewhere between 3 and 4 hours the program freezes.
When I debug this, it is always at a different point.

And I'm not the only with this problem.
Three bug reports on the [SourceForge project page](https://sourceforge.net/projects/dorgem) are related to this problem.

If someone has a program that can detect this kind of problem, please let me know.
At the office we don't have them and there currently is no need to buy such a program.
So if anyone can and wants to help me with this problem, please contact me!!!
See the help or [Dorgem](https://dorgem.sourceforge.net/) website.
