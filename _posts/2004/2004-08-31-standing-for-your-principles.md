---
layout: post
title: Standing for your principles
date: 2004-08-31
lang: en
comments:
  - comment:
    author: Erik
    text:
      - Good thing! You 'hit the nail on it's head'! Just recently I saw a student like the one you describe; really lame! He tried to tell us that Microsoft IE is shit (which is partly true) and e.g. Mozilla is about perfect, which, of course, is not the truth! But there was no way of convincing him otherwise...
      - A typical lamer..!
  - comment:
    author: Powersbridge
    text:
      - Agreed. If someone wants to talk about the evils of any particular software then turn around and work for someone using that same software they hardly seem convinced themselves. I've used PHP and mySQL from the start. But hey, just do it 'cause I love doin' it.
---

What happened to the students these days?
They use to be the ones that stood for principles, don't they?

But now a days students have a laptop with OpenOffice and Mozilla.
They highly prefer Linux to Windows.
They don't use Microsoft products like IE and Outlook because they suck and are unsafe.
I can live with that opinion.

But why do these type of students accept a graduation project to program a ASP.NET website?
What happened to the principles!?!
When you don't like MS and propagate it that way, you don't do that!
You only accept a PHP/MySQL/Apache based project!
