---
layout: post
title: Is it that difficult?
date: 2004-08-24
lang: en
comments:
  - comment:
    author: Erik
    text:
      - Now that's a thing that bugged me since the introduction of the Euro. As if the word 'cent' has a different interpretation when using Euro's. As if you'll mean '5 Euro and 47 Guilder cents'... And all these stupid lemmings that just keep saying it! Let's hunt them down, put them in a big hole and ... [:-S]
  - comment:
    author: Michael
    text:
      - Yeah i've noticed the "euro cent" as well. I've called the IND last friday and heard the same ".. 10 euro cent per minute.." lol what other kind of cent would it be then if not euro cent?! people are stupid :\
---

Why do people still need to say euro cent? As if the guilder cent still exists. But ok, it is not wrong to do so, only a bit useless.

But things can be worse. Look at this quote from the site of Vara Laat, my favorite Dutch late night show.

> Wil je een uitzending bijwonen van VARA Laat? Bel dan met de afdeling Publieksservice op nummer 0900 - 0123 (0,10 eurocent p.m.)

When you want to visit a show you can dial the number. And did you notice what it costs???
