---
title: My bike - part 2
date: 2004-08-13
lang: en
comments:
  - comment:
    author: Michael
    text:
      - "And this evening we will get the keys to our garden."
      - hahah is it just me or does this just sound a tad crooked? Anyway, good thing your bike is up and ... running (?) .. again! In the end replacing a tire was cheaper than buying a new bike huh?
  - comment:
    author: Frank
    text:
      - Read it!! [;)] It wasn't the tire, but the entire wheel. And I thought much more needed to be replaced!
  - comment:
    author: Michael
    text:
      - Sorry.. critical error in memory location 003xFFFFF [:D] i meant to say wheel [:P]
---

[I've told you]({% post_url 2004/2004-07-28-my-bike %}) about the problems I had with my bike recently.
Tuesday I've brought my bike to the bicycle shop to let them put a new wheel in it.
And I'm glad I did.
My bike rides better the it has even done before!
And this all for an acceptable price. [:)]
Maybe next year I have to let them do some other minor things (like the shifters) and then my bike should be fine for quite some time.

And this evening we will get the keys to our garden.
We are very excited about this and are really looking forward to it.
We have bought some plants and things.
But first need to see what is in the house and barns and probable cleanup the mess in the barns.
There is a lot of old rusty stuff in them that needs to dumped.
