---
layout: post
title: New Dorgem beta released
date: 2004-05-18
lang: en
category: software
tags:
- dorgem
---

Today I released [Dorgem](https://dorgem.sourceforge.net/) 2.1.0 beta 5.
This contains the new FTP client.
I've tested it today and it ran 7 hours without any problem, uploading to an FTP site every 15 seconds.
That is much better then with the WinINet FTP because that one stopped completely after about 30 minutes.

It has taken a bit longer then expected, but that is nothing new in software development.
But this time it has not much to do with major bugs or other software problems.
It was me :-)
I just didn't spend enough time on this.

I think this new FTP client is much better then the WinINet based that was used before, so I'm quite happy with it.
It also has a passive FTP mode so luckily this feature is still available.
The code even has support for proxies and firewalls that require a form of authentication, but I don't have a way to test this.
It anyone needs this feature, please contact me and I'll see what I can do.

The next thing I'm going to add to Dorgem is the integration with [DevIL](http://openil.sourceforge.net/).
With this Dorgem can read (for Bitmap captions) and write (for the Store events) most common used formats like.
DevIL also has support for basic effects like brightness adjustments, contrast, etc, etc.

And I really must finish the separate captions for the web server.
That feature is the only thing that holds back the official release of 2.1.0.
And after that I don't think any new 2.x versions will be released (but didn't I say the same when I released beta 4).

At this moment I'm thinking about the new things to add to Dorgem to make it a 3.0 release.
I really want to add DirectShow support.
I'm think about a video DLL so the user can select what video interface (VfW of DirectShow) he/she wants to use.
Another thing I really want is a streaming web server, but that probably require a complete new web server.
Anybody any suggestion, please ?!?
When done Dorgem would be the only free webcam program that has these feature, as far as I know.
