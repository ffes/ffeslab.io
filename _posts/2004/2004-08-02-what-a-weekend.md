---
layout: post
title: What a weekend!
date: 2004-08-02
lang: en


comments:
  - comment:
    author: Michael
    text:
      - Hey there Frank. Looks like you've had a filled weekend at least [:)] Can't really say that It's a shame that we won't have a garden in our house though.. Takes too much time to maintain [:P] But I guess it'd be different when you actually choose to get a garden somewhere apart from your house, than it is to be stuck with a garden because the house came with one [;)]
      - Thank God for airconditioning! Great invention [:D]



  - comment:
    author: Frank
    text:
      - I have never lived in a house with a garden, so I'm not sure I like gardening. But we will see. Once you got one, you need to maintain it.
  - comment:
    author: Bart
    text:
      - Ik was óók naar Varend Corso Westland en stond vlakbij De Uithof, leuk maar tam, zéér tam. Ik zal proberen volgend jaar op 5 en 6 augustus een carnavalsband/dweilband op één van die schuiten te krijgen. Ik vond de vele travo's niet bijster origineel, het percentage met heup- en andere lichaamsdelen wiegende dames/heren erg hoog. Wat ik écht storend vond was dat de schuit vaak/bijna altijd leuk opgetuigd was maar dat dan bijna altijd een dikbuikige schipper, meesrtal aan een sigaar lurkend daarbij to-tààl uit de toon viel en dus het geportretteerde echt heel nadelig beïnvloedde, leermoment voor de leiding zou ik zeggen.
      - ff iets anders. Mevr Fesevur-van Berkel dreef vroeger een modezaak aan De Passage in Den Haag, ik heb nog oude geweven labels van die tijd en bezocht De Passage. Deze Mevr Fesevur-van Berkel woont nu in Zd-Frankrijk. Is zij familie van je?
      - Greetzzzz!
      - Bart
  - comment:
    author: Frank
    text:
      - Ja, op een of andere manier moet dat familie zijn, alleen is de exacte link nog niet gelegd. Stuur even een e-mail als je meer info wilt.
---

Saturday we went to the [Varend Corso](https://varendcorso.nl/).
The procession was really nice, but the location we watched from was not that great.
The average age was 60+ and almost everybody just sat on their chairs and hardly ever moved.
No clapping, s(w)inging, waving, just sitting and watching.
So next year we will visit it again, but from another spot.

We visited it together with my brother, his wife, their son and my sister.
And after it, we went to my brother's house and where we barbecued.
I tried to connect the microphone to my nephew computer, but it seems the mike is broken.
So he has to invest again.
Now we still can not talk on MSN and we still have to type [:(] 

------

Yesterday we visited friends on their allotment garden.
We have been thinking about such a garden as well.
We have a very nice house, but it is on the 4th floor.
We asked if any garden was available, and they took us to someone who knew that kind of things.
And guess what, enough were for sale.
So we look at three of them and how knows, we will have a garden soon.
OK, we need to drive 15 minutes on the bike to get there, but it's much better then only a balcony.

------

It is quite hot in the Netherlands at the moment, at least to our standard (27° [:-)] ), so I didn't spend any time on the memory problems in Dorgem since I'm back from my vacation, sorry.
I also still have to answer some emails, so please be patient with me.
