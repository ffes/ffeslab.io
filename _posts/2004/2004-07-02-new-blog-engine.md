---
layout: post
title: Now Blog Engine
date: 2004-07-02
lang: en
category: website
comments:
  - comment:
    author: michael
    text:
      - It's looking very good man [:D] Again, thanks so much for the help on configuring that pc dude! Gracey was getting annoyed that I couldnt fix it [:P]
      - have a great time in Ommen [:)]
  - comment:
    author: Frank
    text:
      - Michael, we all have our specialties [:)]
  - comment:
    author: Grace
    text:
      - When I came here I almost jumped off my seat because it looks almost the same as Mikey's blog! Hahaha, but cool! Ik zal je ook in mijn website later linken als je klaar bent met het ontwerp. [:)]
      - Geniet van je vakantie!
---

Today I've put the new blog engine online.
I've decided to use the engine written by Michael.
He wrote it for his own blog.
All I have done at this stage is to make it work for me and optimized some code.
And I added a RSS feed.
If you have used the old RSS Feed you need to update your settings.
Michael, many thanks for letting me use your code.

When you look at Michael's blog and mine, you notice that they look very similar.
This will be adjusted after my vacation, which starts this weekend.
Then I will also look at the archives.
They don't work right now and therefore they are not visible.

This new blog has the possibility to add comments to a journal item.
There is no need to register.
Simply enter your name, if you wish your email address and/or your website and your comment.
You can also use the tagboard, if you want to leave a short message that is not related to an item.

As said, I'm leaving for a vacation so it can take a while before I respond to emails.
We are going to camp in Ommen, which is in the North of Netherlands.
Let's hope the weather will get better, the forecasts don't look good [:-(]
