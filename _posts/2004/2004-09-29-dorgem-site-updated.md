---
layout: post
title: Dorgem site updated
date: 2004-09-29
lang: en
category: software
tags:
- dorgem
comments:
  - comment:
    author: Jerry
    text:
      - I'm trying out Dorgem, starting today. What's the resource issue?
  - comment:
    author: Michael
    text:
      - Yeah I know about those lack of time/motivation problems [:(] if I didn't have any of those Legends of Rayne would have been finished by now lol
      - Good luck on solving these issues bud!
  - comment:
    author: Ash
    text:
      - Just thought I'd let you know of a great piece of webcam viewing software which people might like to use. It's a Java client designed to fit into your web site, found at http://xaoza.net/software/webcam/index.html. I'm using it at the moment, dunno if this is helpful for you or not. It's a great piece of software though. [:-D]
      - As for lack of motivation, I know the feeling. I've got three projects that I just need to get done, but don't have the motivation for any.
  - comment:
    author: jensemann
    text:
      - Hi,
      - using dorgem (latest beta) for some weeks now and didn´t have resource-probs ever since.. Any more details about the resource-issue?
      - Greetings from germany, Jens
  - comment:
    author: Brian
    text:
      - i have just done a ton of javascript for my website just because i decided to switch to dorgem from webcamXP(a big resource hog). You can use my javascript. It's working great for me.
      - if you want... you can post my javascript on your page.
      - You can find the code here http://bst82551.ath.cx:8181/cam/cam1.txt
      - Maybe to solve your resource problem, you should post the code to beta 5 on your sf.net. Maybe a few programmers would look at it and see something that you missed. I mean after looking through thousands of lines of code, you could be missing something small. I'd definitely look at the code a little(mostly to learn). I'm trying to get into C++ programming. Anyway, good luck on finding the bug and don't forget- use my javascript examples if you want!!!
      - Brian
  - comment:
    author: Kasper
    text:
      - I really love Dorgem, it is the best webcam solution i ever tried. But i would really like to see the ftpproblem solved.
      - I hope you can find time in the near future to work on it.
---

Today I update the Dorgem website.
I added some users to the Users page and added a roadmap for the next versions of [Dorgem](https://dorgem.sourceforge.net/).

There is not much development going on right now.
The reason for this is quite easy, a lack of time and motivation.
There is a nasty resource problem in the current version and I can't find it [:-(]
I thought I had fixed it, by adding the new FTP client, but that didn't help.
Also changing some things in the DoCapture() but that didn't help.
When everything I try doesn't help, it is not good for my motivation.

One thing I still need to add to the site is a description how to add Dorgem to your website.
Things like, publishing the source of my own webcam page and basic HTML and JavaScript samples to show how pages can be updated.
This has been requested my some users and I promised to publish it, but I just did come to it.
The basic HTML and my PHP (based upon an idea contributed by Marcus Shaw) solution are quite easy to publish, but I still need to make a proper JavaScript sample.
Once that is done, I will put them on-line.
