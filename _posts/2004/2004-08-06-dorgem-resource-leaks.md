---
layout: post
title: Dorgem Resource Leaks
date: 2004-08-06
lang: en
category: software
tags:
- dorgem
comments:
  - comment:
    author: Zac Bowling
    text:
      - Simliar problems experience in many different application lately. These libraries are still old that microsoft puts out.
---

Today I've finally setup a test system to find the resource leaks [Dorgem](https://dorgem.sourceforge.net/).
On the test machine at the office I installed Dorgem, the compiler and BoundsCheckers.
When I run Dorgem in BoundsChecker, it starts complain a couple of times.

I won't bother you with the technical details, but the real problem seems to be in `DoCapture()`.
I hope that when I have adjusted this function, it will increase the stability of Dorgem a lot.

So I have some work to do!
