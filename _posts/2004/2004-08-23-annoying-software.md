---
layout: post
title: Annoying software
date: 2004-08-23
lang: en
comments:
  - comment:
    author: Erik
    text:
      - Hey; that's a kinky feature! My name, e-mail and website are automatically filled in!
      - Anyway, I suppose the music-industry paid them a load of money to block the traffic from player to harddisk. It won't be an idea of iRiver self.
  - comment:
    author: Frank
    text:
      - I also think it is not an iRiver idea, but it's still very strange that the UMS does work.
---

For my birthday this year I bought a iRiver iFP-180T MP3 player.
A nice device that I use a lot.
It is a little battery consuming, but I use rechargeable so it is not a very big issue.

Saturday evening I updated the MP3's on my player.
I accidentally deleted two files from my harddisk that were on my MP3 player.
First thought was to put them back from my MP3 player to my harddisk.
But what do you think...

That was **not** possible.
The iRiver Manager has a anti-piracy thing built-in that prevents you from copying a MP3 from the player to the PC.
Except MP3 and WMA files, any other file types can be copied?!?
I just don't see the point of this.
Do they really think this will stop people from copying MP3s?

But then I found the **really** strange thing on this issue.
When I install the <acronym title="USB Mass Storage, this will turn my MP3 player into a memory pen/drive.">UMS</acronym> firmware you can copy **any** type of file, including MP3, from the player to the PC.

The downside of UMS of that it is much slower then the Manager firmware, which I'm using right now.
And since I only update the MP3 player at home, I don't think I will install the other type of firmware, but I really don't see the use of this so called piracy blocking.
