---
layout: post
title: Busy weekend
date: 2004-06-27
lang: en
comments:
  - comment:
    author: Erik
    text:
      - Hey, what's this?!?
      - All these posts weren't here this morning! WHAT IS GOING ON?
  - comment:
    author: Frank
    text:
      - I imported my old blog as you can see and will update the layout soon [:)] 
---

This weekend is quite a busy weekend.
Menno's 6th birthday was yesterday.
A lot of people came to visit us and he got a lot of presents.
It looks like we now have a Bart Smit in our home.

And yesterday also was a great day for Dutch soccer.
**Finally** we managed to win a penalty shout-out, in the match against Sweden.
We now are in the semi-finales of the [European Championships](https://en.wikipedia.org/wiki/UEFA_Euro_2004).
We gone have to play Portugal next Wednesday.
A though match!
Portugal as the hosts of the championship, so a lot of Portuguese are in the stadium and a few Dutch fans.
But I'm sure I will be in front of the TV to see the game and of course they have to win.
Yesterday you could see that they really are a team and that is a thing they need to go on and become champion.

Today we are going to visit [Parkpop](https://nl.wikipedia.org/wiki/Parkpop), in the Zuiderpark which is a 5 minute walk from our home.
There are not that much acts we are really need to see, but it is always a nice atmosphere.

Another I want to do this weekend is to switch to another blog engine.
Now I use [Thingamablog](https://www.thingamablog.com), which is a great engine when you don't have a permanent connection to the Internet.
But since a week or three we have ADSL at home, so I don't have a need for it anymore.
A real on-line blog would be much better.
Now I am bound to my home PC to update my blog.
And Thingamablog doesn't have support for comments, which is something I sort of miss.

I have not made a decision which engine to use, but I want to run it on me own site and not a blogger.com (or similar) entry.
I don't want the ads and need to subscribe to add a comment.
I've run into b2evolution, but I'm not sure yet if I want to use it.
I could also write my own or use code written by Michael a former colleague.

So, as said a busy weekend and not much time to look at the memory problems in Dorgem.
I'm afraid it won't until after the holiday season.
