---
layout: post
title: Test system almost up and running
date: 2004-06-17
lang: en
category: software
tags:
- dorgem
---

At the office we have a test PC with images of all Win32 operating systems.
This is very useful for testing new software under different versions of Windows and Internet Explorer.
Today I started to setup a test system based upon an XP image, so I can try to find the problems I have with the current beta of [Dorgem](https://dorgem.sourceforge.net/).

I've downloaded the evaluation version of [BoundsChecker](https://en.wikipedia.org/wiki/BoundsChecker).
BoundsChecker claims it can find memory and resource leaks.
I hope with it I can find and, even more important, solve this problem.

Today I also got a contribution by Marcus Shaw.
He contribute a PHP script to handle when the camera is off-line.
The script checks if the uploaded image is not too old.
If so, it will show the off-line image.
I will review this somewhere next week.

The coming weekend is very busy and I spend too much time watching the [European Championship Football](https://en.wikipedia.org/wiki/UEFA_Euro_2004) in Portugal.
I hope Holland will become champion, but as many people here, I don't think they will manage to do it.
I will have to miss the next game we (when they win it's we, when they loose it's they [:-)] will play against the Czechs, because Caroline gives a concert with [her choir](https://www.voicesunlimited.nl) this Saturday evening.
But they will do well, even when I don't watch he game on TV.
