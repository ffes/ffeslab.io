---
layout: post
title: Building a simple recipes website
lang: en
date: 2020-12-30 21:15:39 +0100
last_modified_at: 2024-10-20 15:31:58 +02:00
category: software
tags:
  - .NET Core
  - Recipes
  - Static website
---

During the last months we have been cooking a lot more.
From our niece we got a number of [Hello Fresh](https://www.hellofresh.nl/) recipes, and like them very much.
So I wanted to improve the recipe management.
And as a true developer, I decide to create something myself.

Until now we kept most of the recipes simply on paper.
But that makes finding back those recipes very cumbersome.
We also had a couple of ODT-documents with some recipes.
All in all not very organized.

I recently found out about the [recipe schema](https://schema.org/Recipe) and liked that as a simple way to store my recipes.
For a developer, they are relative easy to read structured data that can be processed.
It is also the format Google wants you to include when you [publish your recipes on the internet](https://developers.google.com/search/docs/data-types/recipe) so they can process them more easily in their search results.

A simplified version of the recipe found on schema.org

```json
{
	"@context": "https://schema.org",
	"@type": "Recipe",
	"name": "Mom's World Famous Banana Bread",
	"description": "This classic banana bread recipe comes from my mom -- the walnuts add a nice texture and flavor to the banana bread.",
	"cookTime": "PT1H",
	"datePublished": "2009-05-08",
	"recipeIngredient": [
		"3 or 4 ripe bananas, smashed",
		"1 egg",
		"3/4 cup of sugar"
	],
	"recipeInstructions": [
		"Preheat the oven to 350 degrees.",
		"Mix in the ingredients in a bowl. Add the flour last. Pour the mixture into a loaf pan and bake for one hour."
	],
	"recipeYield": {
		"value": 1,
		"unitText": "loaf"
	}
}
```

We had found the recipes for our Christmas diner on various sources and some of them were hard to read or print.
My primary goal at this moment was to create easy printable recipes.

Now that I had some data and a goal I could start to code.

First I converted those recipes to JSON files.
Since most recipes were found on-line, it was mostly a matter of copy and paste.

I created a .NET console app to do the work.
C# is a language I know and like and .NET has very rich ecosystem of add-ons.
For the very first version I created the models I needed and used the [`System.Text.Json`](https://docs.microsoft.com/en-us/dotnet/api/system.text.json) deserialiser to parse the recipes.
The first basics were there and worked as expected.

Now I needed something to print the recipes.
I first investigated some PDF nuget packages.
That was an unpleasant experience.
There is nothing that can be used in a simple hobby project.
Most of them are expensive or only for .NET Framework.
So creating PDF's on the fly is not gonna happen.

The next idea was to create HTML files and print them.
But how do you create a HTML file from .NET code?
Luckily our son Menno recently did a project at work to manipulate HTML, so he suggested to use [HTML Agility Pack](https://html-agility-pack.net/).
An evening of work was all it took to create simple HTML files from the JSON files.
All I needed to do was adding some CSS and I could print our Christmas recipes!

After a successful Christmas diner, I wanted to improve my generator and create a simple static website I can host on the NAS in our LAN.
So I added a `appsettings.json` to set the paths and let the tool generate an `index.html`.
Now we can view the recipes in the kitchen on our smartphone.

The website generator has a lot of points of improvements.
* The code is not very robust. There is no error handling, so a malformed JSON file will crash the app.
* The JSON parser should get polymorphic deserialization capabilities.
* In the JSON file is more information then is shown on the HTML.
* Searching is on the wishlist.
* So is processing the keywords.
* And showing the images. Maybe those images need to be resize before putting them on the site.
* Include the original JSON as JSON-LD tag in the HTML, so search engines can do smart things.
* I hope the [schema.org PR 2738](https://github.com/schemaorg/schemaorg/pull/2738) gets merged, which give the option to do smart things with the ingredients.
* The static texts are all in Dutch, apart from the timespan. They could become translatable. But that means that others would like to use this tool as well. In its current state I cannot imagine anyone wanting that.

Want to see how I did this all, take a look at the [repository on GitHub](https://github.com/ffes/recipes).

When dreaming about new features to do with the data I have, a next step could be to create an EPUB so we view the recipes on our e-reader.
Or even an Android app that contains the recipes or talks to a webservice.
Since we also have an iPad, maybe I could even create an app for that.
The possibilities are endless.

**Follow up 2024-10-20:**

Our family cookbook has improved a lot since this was posted and can be found [online](https://kookboek.fesevur.nl).
The sources of the recipes can be found at [GitLab](https://gitlab.com/ffes/kookboek).
