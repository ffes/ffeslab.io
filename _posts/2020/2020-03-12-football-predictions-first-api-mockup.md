---
layout: post
title: First API mockup
series: Football predictions in ASP.NET and Angular
lang: en
date: 2020-03-12 22:46:15 +0100
tags:
  - ASP.NET Core
  - Football predictions
---

It has been a while, but the last couple of days I finally made time to invest some time in the football predictions site.
Given the current situation with the COVID-19 virus it is unsure if the tournament will be played at all, but for me it much more about the learning process then the actual site itself.

## Groups API

The first thing I implemented is a mockup API to get all the groups.
There is no data retrieved from the database yet, so it turned out to be not too difficult.

I created the models for `Group` and `Country` and for now they should be fine.
After that, some instances were created and that is returned by the API.

That is all the endpoint does at this moment.
A really basic proof of concept.

## Using the API

The next step will be show the content of the API call in the client.
I'm looking into [NSwag](https://github.com/RicoSuter/NSwag) or [ng-swagger-gen](https://www.npmjs.com/package/ng-swagger-gen), to generate that part of the client for me.

For that the site needs a be able to output a `swagger.json`.
That was added as well.

### NSwag

All the tutorials on NSwag I found are Windows-based, using NSwagStudio.
Problem is that NSwagStudio is a Windows Desktop application.
At the moment most part of my development is done on a Linux machine that won't work for me.
There is a command line interface, but I haven't tried that out yet.

On the other hand, NSwag can generate its stuff based on compiled code of the server and doesn't need the output of `swagger.json`.
That could be a plus. No need to generate a json file.

### ng-swagger-gen

ng-swagger-gen is a node.js client and we have been using it at the office before.

It needs `swagger.json` but they advise not to store the generated code in your git repository.
You should generate the code during the build.
It is CI-friendly.

### Conclusion

Well, there is not conclusion yet.
I need to investigate these two tools and decide which one to use.
