---
layout: post
title: Back to the light theme, if you prefer
date: 2020-06-21 18:06:21 +02:00
lang: en
excerpt_separator: <!--more-->
category: website
tags:
  - website
  - dark theme
  - light theme
  - prefers-color-scheme
  - SASS
  - CSS variables
---

Back in September 2019 [I converted](https://gitlab.com/ffes/ffes.gitlab.io/-/commit/d57220931397ed7213ac850c89e0f0dbd0a90f01) the look of my website to dark theme.

A while ago I found out that most modern operating systems and their browsers now support [prefers-color-scheme](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme).
So I recently decided that I want to add support for that to my site and as a side effect bring back the light theme (if you prefer that).
<!--more-->
I know that not all operating systems support the feature, like Linux and older version of Android and iOS.
But since most of my visitors use the Windows desktop to view my site, I don't think that will be much of a problem.
Windows 10 and all the major browsers on that platform support this feature.
So the site will look the way you want: light or dark.

I choose to make the dark theme, the default theme.
So if suddenly the site is changed to a light theme and you prefer the dark theme, you might consider adjusting your system preferences.

## How I did it

Since I use [Jekyll](https://www.jekyllrb.com) to generate my site, I use SCSS to generate the CSS for my site.

The `prefers-color-scheme` setting is not too difficult to implement, but there are a couple of things you need to think of.

First of all since the site need to be able to dynamically change from light to dark and back you cannot simply hardcode the color in the CSS.
It is best to use CSS variables to define all the colors.
I gave the variables names based on their use like `--text-color` and `--bg-color`.

```css
/* The dark theme is the default theme */
:root {
	--bg-color: #293134;
	--text-color: #c8c8c8;
	--highlight-color: #fdfdfd;
	--nav-bg-color: #000000;
	--nav-text-color: #c8c8c8;
	--nav-highlight-color: #fdfdfd;
	--header-color: #fdfdfd;
	--link-color: #569cd6;
	--terminal-color: #619e57;
}

@media (prefers-color-scheme: light) {
	:root {
		--bg-color: #fdfdfd;
		--text-color: #000000;
		--highlight-color: #293134;
		--nav-bg-color: #000000;
		--nav-text-color: #c8c8c8;
		--nav-highlight-color: #fdfdfd;
		--header-color: #569cd6;
		--link-color: #569cd6;
		--terminal-color: #619e57;
	}
}
```

After that I had to replace all the colors with these CSS variables.

```css
body {
	background-color: var(--bg-color);
	color: var(--text-color);
	height: 100%;
	font-family: 'Open Sans', sans-serif;
}
```

Nice plus is that the new variables names make the SCSS easier to understand what will happen.

If you want to use SASS variables to define the RGB values you can use them, but know that [modern implementations of SASS](https://sass-lang.com/documentation/breaking-changes/css-vars) require to use the `#{$dark_grey}` syntax when defining the colors.

My website is quite simple, so that is all I had to do. Well, when it comes to SASS and CSS.

Back in September I also replaced the social media logos with SVGs on my [About page]({% link about.html %}).
So I only had black versions of the GitHub logo and of the Instagram logo.
For now I created two versions of the logo: a black one and a white one.
And based on the `prefers-color-scheme` the default image is overwritten using the `<picture>` tag.
Maybe one day I will include the SVGs as inline images and use CSS to replace the colors.

```html
<picture>
	<source srcset="assets/images/logos/github-black.svg" media="(prefers-color-scheme: light)">
	<img src="assets/images/logos/github-white.svg" alt="GitHub" title="GitHub" />
</picture>
```

With a little over an hour the site respected the preference of the user.
