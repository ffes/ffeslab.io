---
title: Getting started with Eleventy
date: 2020-10-26 21:47:52 +01:00
lang: en
category: website
tags:
  - 11ty
  - eleventy
  - website
---

The last couple of days I have been playing a bit with [Eleventy](https://www.11ty.dev/).

## Why Eleventy?

I really like the concept of [static website generators](https://jamstack.org/generators/), especially for the sites I build at this moment.
Thus far I have been creating a couple of website with [Jekyll](https://www.jekyllrb.com/), including this site.
At this moment I don't have plans to move away from Jekyll for my existing sites, but it is always a good idea to investigate other options.
One of the downsides of Jekyll is its build speed and the dependency of Ruby and the ecosystem like bundler.

Since I already use [Node.js](https://nodejs.org/en/) to get some of the dependencies in my Jekyll sites, like [Bootstrap](https://getbootstrap.com/) and [Font Awesome](https://fontawesome.com/), I thought I would investigate a JavaScript based static site generator.
According to [several](https://24ways.org/2018/turn-jekyll-up-to-eleventy/) [sources](https://www.ianfeather.co.uk/moving-this-blog-from-jekyll-to-11ty/) converting from Jekyll to Eleventy is not a huge step.
This makes it a great candidate to look into.

## What I need

My wife Caroline has been drawing and painting for quite some time and wants to create a portfolio website.
I decided to play a bit with Eleventy so I can make a substantiated decision when that site really needs to be build.

What I need to maintain such a website, apart from a decent design, are a couple of things:

* It must support data file, preferably in JSON, to generate various pages with the artwork.
  Adding a new image and description should be little work.
* It must support [SCSS](https://sass-lang.com/) to generate the CSS for me.
* It must run within a Docker image so I can use that for development and GitLab CI based building and releasing.
* The template system should be at least as powerful as [Liquid](https://jekyllrb.com/docs/liquid/) is in Jekyll.
* I really like the [`link` tag](https://jekyllrb.com/docs/liquid/tags/#link) of Jekyll.
  It would be great to be able to keep having a build time feature like that.

The new static site generator at best should have all these features, but I can live with not having the `link` tag.

## Getting started

The very first steps to play with Eleventy is to go through the [getting started steps](https://www.11ty.dev/docs/getting-started/) and create a Hello World! like site.
That worked within a couple of minutes.

The first thing I like is that they use `npx` and discourage the use of installing Eleventy as a global tool.
This makes it easier to install and allows you to simply run a `docker run` command using the `node:lts` image and you are ready to get started.
The first wish fulfilled.
Great!

Next step will be adding a template and some basic styling.
According to the [docs](https://www.11ty.dev/docs/layouts/) there are several options for templating.
Let's dive in that a next post.
