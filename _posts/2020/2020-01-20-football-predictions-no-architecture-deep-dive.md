---
layout: post
title: No architecture deep dive
series: Football predictions in ASP.NET and Angular
lang: en
date: 2020-01-20 22:06:53 +0100
tags:
  - ASP.NET Core
  - Football predictions
  - Domain Driven Design
  - Clean Architecture
---

In [my last post]({% post_url 2019/2019-12-16-football-predictions-create-the-database %}) I wrote that I wanted to start writing a API endpoint.
But that was a bit premature, at the time.

I first had to investigate about stuff like Domain Driven Design, Test Driven Design and Clean Architecture.
To put it mildly my knowledge of these architectures and best practices is a bit rusty.

Over the last period I watched a couple of YouTube clips and read various articles about those subjects.
Turns out it is quite a journey to learn these topics, and not necessarily that useful for this project.
Domain Driven Design is recommended for complex systems, but I don't consider the website I am going to build complex.
I definitely need to learn more about these things, but I'm not gonna combine that with this project.
I don't want to dive too deep.
The basics of a Web API and an Angular frontend will keep me busy for the coming period.
