---
title: Oplossen van een Rubiks kubus
lang: nl
date: 2023-01-22 17:46:00 +01:00
tags:
  - Rubiks kubus
  - tutorial
---

Al jaren wil ik een [Rubiks kubus](https://nl.wikipedia.org/wiki/Rubiks_kubus) kunnen oplossen, maar ik had er tot nu toe had ik er geen energie in gestopt.
Maar bij mijn "onderzoek" voor ons bezoek aan het [Nationale Videogame Museum]({% post_url 2023/2023-01-14-videogame-museum %}) was ik op een YouTube-kanaal terechtgekomen die een [playlist](https://www.youtube.com/playlist?list=PLCeCAjMpjky-AW6QcJYSthVva8HZyNgas) heeft over hoe je een Rubiks kubus kunt oplossen.
Een videoserie gericht op kinderen van de basisschool, maar als het werkt is dat natuurlijk geen probleem.

Menno heeft zijn kubussen nog niet meeverhuisd, zijn er diverse kubussen in huis.
Aan de slag dus met die video's, en het werkte.
Voor het eerst in mijn leven wist ik zelf een kubus op te lossen!

Omdat ik de algoritmes niet direct uit mijn hoofd weet, heb ik ze (vooral voor mezelf) uitgeschreven en gelijk deze tutorial van gemaakt.
Voor een uitleg over de [Cube Notation](https://nl.wikipedia.org/wiki/Rubiks_kubus#Notaties) kun je [deze video uit de serie](https://youtu.be/GRu9AcdtbRo) bekijken.


## Uitgangspunt

Aangezien ik al jaren weet hoe de eerste laag gedaan moet worden, gaat deze tutorial ervan uit dat de eerste witte laag compleet is.


## Middelste laag

Draai de witte laag naar onder en zoek in de bovenste laag een middenstuk dat geen geel bevat.
Draai dit boven het juiste middenblok.

Als de kleur bovenaan links zit, draai dan de kubus naar links en voeg het volgende patroon uit:

```
L'U'L'U'L'ULUL
```

Als de kleur bovenaan recht zit, draai dan de kubus naar rechts en voer het volgende patroon uit:

```
RURURU'R'U'R'
```

Als er geen juiste blokje bovenaan beschikbaar zijn, positioneer de kubus zo dat gewenste blokje rechts voor je zit en draai dan een blokje naar de bovenste laag.

```
RUR'U'F'U'F
```

## Bovenste laag


### Naar kruis

Eerst werken naar het "kruis".

Zorg dat het gele vlak naar boven is en voer het volgende patroon uit.

```
FRUR'U'F'
```

Voer dit patroon net zo lang uit tot je op het kruis komt.

### Van kruis naar vis

De "vis" is alleen de "vis" als alle middenstukken en één hoek geel zijn. Als er meer dan één hoek geel is, is dat gewoon het kruis.

```
RUR'URU2R'
```

### Van vis naar gele laag

Als je bij de "vis" uitkomt zorg dat dat je de gele hoek linksonder hebt en voer (met geel naar boven) het volgende patroon uit.

```
RUR'URU2R'
```

Als je na de eerste keer weer op de "vis" uitkomt, draai de kubus dan zodat de hoek weer linksonder zit en herhaal het bovenstaande patroon.

### Gele hoeken goed zetten

Mogelijk kom je gelijk op "koplampen" uit. Dit zijn twee hoeken die al goed staat gedraaid naar de kleur van het middenstuk.
Zorg ervoor dat je "koplampen" of een heel vlak hebt en draai die naar links.

Als je geen "koplampen" hebt, voer dan ook deze stap uit zodat je wel "koplampen" hebt.

```
RUR'U'R'FR2U'R'U'RUR'F'
```

Mogelijk moet je deze stap meerdere keren uitvoeren. Zorg er altijd voor dat de "koplampen" of het hele vlak aan de linkerkant zijn.

### Naar één zijde opgelost

Voer het volgende patroon uit.

```
R2URUR'U'R'U'R'UR'
```

### Naar helemaal opgelost

Zorg dat de opgeloste zijde aan de achterkant zit en voor nogmaals het volgende patroon uit.

```
R2URUR'U'R'U'R'UR'
```

Mogelijk moet je deze stap nogmaals uitvoeren. Zorg er altijd voor dat de opgeloste zijde aan de achterkant zit.
