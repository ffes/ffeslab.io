---
layout: post
title: Nationale Videogame Museum
date: 2023-01-14 19:36:48 +01:00
lang: nl
tags:
  - Nationale Videogame Museum
  - zoetermeer
  - retro gaming
---

Vandaag zijn Menno en ik naar het [Nationale Videogame Museum](https://www.nationaalvideogamemuseum.nl/) geweest in Zoetermeer. Tot voor kort wist ik niet eens van het bestaan van dit museum. Maar op Facebook kwamen we een post tegen dat er tot het einde van deze maand een pop-up museum in het centrum van Den Haag is, in de voormalige boekhandel Van Stockum.

Toen ik dat hoorde heb ik wat onderzoek gedaan en zo kwamen we er achter dat in het Stadshart in Zoetermeer het Nationale Videogame Museum is gevestigd. Het museum is [een uit de hand gelopen hobby](https://www.nationaalvideogamemuseum.nl/geschiedenis) en inmiddels een volwaardig museum. Online konden we alvast kaartjes kopen, voor het tijdslot van 14:00 tot 16:00. Op YouTube was ik een [filmpje](https://youtu.be/pWSp6pmaU_w) tegengekomen van iemand die het begin 2020 had bezocht, en zelf heeft het museum ook een [YouTube kanaal](https://www.youtube.com/@NationaalVideogameMuseum).

Aan het einde van de ochtend vertrokken we naar onze eerste stop, de pop-up in het centrum.

Hier stond een tiental kasten. Variërend van klassiekers als [Ms. Pac-Man](https://nl.wikipedia.org/wiki/Ms._Pac-Man), [Donkey Kong](https://nl.wikipedia.org/wiki/Donkey_Kong_(spel)) en [Galaga](https://nl.wikipedia.org/wiki/Galaga) tot het voor ons onbekende [Michael Jackson's Moonwalker](https://en.wikipedia.org/wiki/Michael_Jackson%27s_Moonwalker). Hier hebben we (bijna) alle kasten uitgeprobeerd en ons prima vermaakt.

Na een korte lunch gingen we door naar Zoetermeer, op weg naar het echte museum. We waren benieuwd wat ons te wachten stond.

Bij binnenkomst in het museum kregen we een armbandje wat ons tijdslot aangaf. Daarna konden we naar binnen. Maar eerst deden we onze jassen in een locker, waarvan de deurtjes een 8-bit plaatje van Pinky, een van de monsters van Pac-Man. Leuk detail zo gelijk bij het begin.

Het eerste deel van het museum heeft ook echt iets museaals. Een in 70-er-jaren stijl ingerichte woonkamer met een [Atari 2600](https://nl.wikipedia.org/wiki/Atari_2600), en een 80-er-jaren ingerichte slaapkamer met een [Commodore 64](https://nl.wikipedia.org/wiki/Commodore_64) en een [MSX](https://nl.wikipedia.org/wiki/MSX). Op de 2600 was Pitfall te spelen en ook op de homecomputers waren spellen te spelen. Bij de homecomputers lagen ook de listingboeken, waaruit je in BASIC geschreven programma's kon overtypen en dat spelen, als je uiteraard geen typefout had gemaakt.

In dit eerste stuk stond ook een collectie van Pac-Man kasten, de meest opvallende wat mij betreft een combinatie die ik nog nooit gezien had, namelijk "Baby Pac-Man". Dit is een combinatie van de videogame Pac-Man dat je in het scherm bovenaan speelt en als je een escape onderaan neemt kom je in flipperkast terecht waar je punten kunt scoren. Als de bal weggaat, kun je weer verder met Pac-Man. Erg leuk om te doen, al waren de spookjes van deze Pac-Man erg agressief en daardoor moeilijk te spelen.

Als je dit eerste stuk door was kwam je bij de kasten van de 80-er-jaren, al vonden wij dat aanbod een beetje tegenvallen. Een aantal van de kasten die we in Den Haag al hadden gezien, maar niet heel erg veel andere kasten.

Tot nu toe hadden we nog niet het idee gehad dat het heel erg druk was, maar toen we in de grote hal kwamen was het even acclimatiseren. Rij na rij met alle mogelijke videogames waar een hoop geluid uit kwam, maar wat een gaaf gezicht.

Een rij met flightsimulators en daar tegenover een rij met allemaal motorspellen. Daarachter een rij met aan beide kanten alleen maar autorace spellen. Er was een sectie met allemaal schietspellen, inclusief een pistool waarmee je op het scherm "schoot" maar ook een kleiduivenschietspel met een geweer.

Hier hebben we best wat tijd doorgebracht. Zelf had ik in mijn jeugd altijd van die racespellen willen maar dat was er nooit echt van gekomen. Dus deze kans heb ik niet voorbij laten gaan. De eerste keer op zo'n motorspel ging verrassend goed. Ik dacht nog dat ik een talent had ontdekt, maar het bleek toch echt beginnersgeluk want een tweede keer ging het compleet mis.

Achterin de hal was een hele sectie met allemaal Japanse ritmespellen. Denk hierbij aan allemaal variaties op Guitar Hero. Met trommels waar je met stokken in het juiste ritme op moest slaan, maar ook een DJ-booth waar je de plaatjes op het juiste moment moest instarten. Uiteraard ontbrak een dansspel waar je op het juiste moment op de juiste plek moest stappen niet.

Al met al waren kwamen we onze 2 uur prima door, maar tegen het einde was het ook mooi geweest. We hadden gedaan wat we wilden doen. Nou ja, bijna dan. Menno had graag nog zo'n drumspel gedaan, maar daar was het erg druk.

Met een tevreden gevoel, kijken we terug op het bezoek aan dit museum. Erg leuk om te zijn en om terug te komen. Herinneringen opgehaald, spelletjes gespeeld. Maar zaterdagmiddag tussen 14:00 en 16:00 is niet het ideale moment, want dan kan het er flink druk zijn.
