---
layout: post
title: Recovering my very old blog
date: 2023-08-16 21:05:42 +02:00
lang: en
category: website
---

I have been blogging quite a long time now.
The oldest blogpost on my site was from June 2011, but I have been blogging on and off since 2004.
Those old posts from 2004 until 2011 were lost for many years.
I wasn't sure what had happened exactly, so did some research and found out some things I completely forgot about.

## Some history

I registered `fesevur.com` in May 2000 and at first my site was handwritten HTML.

When I started [my blog in April 2004]({% post_url 2004/2004-04-09-it-is-started %}) I used an offline blog engine called [Thingamablog](https://www.thingamablog.com), that much to my surprise still exists.
In July 2004 [I converted my blog to a self written engine]({% post_url 2004/2004-07-02-new-blog-engine %}) after getting some code from a colleague.
Having a permanent internet connection and being able to blog from everywhere, and not being limited to my computer at home, were my main reason to do so.

Then CMSes became more and more a thing and [I chose]({% post_url 2007/2007-01-12-moving-weblogs-to-joomla %}) to use [Joomla 1.x](https://www.joomla.org/) for my website, but for my blog I kept on using that self written engine.
Eventually I converted the frontend part of the engine to a Joomla component so it integrated better in the site and used that for a while.
The backend to edit the posts always remained the same.

After a while my website didn't get the attention it needed and the blogging stopped for a couple of years.
When I [started blogging again]({% post_url 2011/2011-06-14-nieuwe-poging %}) in 2011 I used [Blogger](https://www.blogger.com) this time.
That was the hip way to go in those days.
Since there was no proper import for my self written blog, I never bothered about the old posts, a thing I regret.

I remember that updating Joomla back in the day was not too easy, so that got delayed and delayed.
In early 2015 I moved away from the CMS and created a new site, with much the same content, using [Jekyll](https://jekyllrb.com).
I was back at a static website, but this time generated from sources.
A step I still think was the right decision, much faster and safer and easier to maintain for me as a developer.
My blog remained on Blogger for a while, until I decide to use the [Jekyll importer for Blogger](https://import.jekyllrb.com/docs/blogger/) to [import my blog]({% post_url 2017/2017-10-15-blog-verhuisd %}).

## Finding back the old blog

Only problem was that my old blog, that used the self written engine, was still gone.
Somewhere in that period I must have lost the backup I surely made of the old posts.

Recently someone mentioned [The Internet Archive Wayback Machine](https://web.archive.org/).
Obviously I know that site, but for some reason this time I linked the two together,
and it turns out [they archived my blog better than I did](https://web.archive.org/web/20081201142624/http://www.fesevur.com/component/option,com_ffblog/user,frank/action,archive/Itemid,24/).
This was the only place that has the old blog and I wanted it back.

## Recovering the old blog

Recovering these blog posts turned out to be manual labor.
There was no efficient way to automate that.
I had to copy and paste every blogpost into a separate markdown file, and I blogged a lot in those early days.
It took a couple of evenings to convert the 119 posts I wrote in those 5 years.
Maybe I should have invested time in automating that process.
During this process I checked and (if possible) fixed the links, added some metadata, fixed some typos but didn't change anything else.

This obviously meant I read my entire old blog after many, many year.
It was, let's say, interesting to read about my life and opinions in those years.
Some quite personal stuff, some opinions I'm not sure I would still have today, some nice memories that came back.
Especially all the running posts about my first two years of running were very enjoyable to read.

The Wayback Machine don't have the pictures of the posts.
I checked my photo archive, but apparently those specific images for the blog were not saved.

My old blog had comments for some time.
For the record I converted those, even though my current blog doesn't have comments.
I've stored them in the metadata of the post they belonged to.
They are rendered very basically.
That could be improved.

The other thing that could to be fixed as well are the smileys.
I used them a lot back then.

Now I finally have my complete blogging history on my website.
If you are interested, all the entries from 2004 until 2008 can be found in [my blog archive]({% link blog/archive.html %}).
