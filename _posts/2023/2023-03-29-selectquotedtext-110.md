---
layout: post
title: SelectQuotedText 1.1.0 released
date: 2023-03-29 22:23:59 +01:00
lang: en
category: software
tags:
  - selectquotedtext
  - notepad++
---

Today I released version 1.1.0 of [SelectQuotedText]({% link selectquotedtext.md %}).

New features:

- Updated so it work again with recent versions of Notepad++.
- Removed changelog from the AboutDlg and created a CHANGELOG.md in the repository.

To download version 1.1.0, go to [download page on GitHub](https://github.com/ffes/selectquotedtext/releases/tag/v1.1.0).
