---
layout: post
title: Retro gaming on a very old laptop
date: 2023-04-29 19:32:27 +02:00
lang: en
tags:
  - Ubuntu
  - RetroPie
  - retro gaming
---

A while ago [I come across retro gaming once again]({% post_url 2023/2023-01-14-videogame-museum %}).
Although I am not a gamer, having grown up and started uses computers in the 1980's retro gaming is something I do occasionally.
The first computer I ever played with was a friend's [Sinclair ZX81](https://en.wikipedia.org/wiki/ZX81).
The first computer I owned was an [Atari 800XL](https://en.wikipedia.org/wiki/Atari_8-bit_family) followed by an [Philips](https://en.wikipedia.org/wiki/Philips_VG-8020) [MSX](https://en.wikipedia.org/wiki/MSX).
Friends of mine had a [Commodore 64](https://en.wikipedia.org/wiki/Commodore_64).
Apart from learning to [program in BASIC](https://en.wikipedia.org/wiki/BASIC) (what kick-started my life in IT), I also played games on it so I know a lot of the major games from that era.

If you want to play those games again, you need something like [RetroPie](https://retropie.org.uk), which is the best supported tool for retro gaming at the moment of writing.


## The hardware

I still had a very old Dell Latitude E5500 lying around.
This machine is a leftover from the office, saved from the bin.
It was registered in our system at the office in May 2009, so quite old.
It has a Intel Core2 Duo P8400 processor, 4 GB internal memory and 250 GB HDD and came originally with Windows XP.
When that was not supported anymore, I install Xubuntu on it and used it for some time, but it became obsolete a couple of years ago.
It should have left the house, but for some reason we never throw it away.
To play with RetroPie I decided to try it first on this laptop.
I had RetroPie installed on it for a couple of weeks and that worked quite well.
But for some reason I couldn't run system updates on it anymore, so I decided to reinstall Ubuntu and try to make it autologin into RetroPie.
This means I can turn it on and start playing.


## Reinstall the machine

First I downloaded the [Ubuntu Server 22.04](https://ubuntu.com/download/server) ISO and put that on a USB stick with that image using the [Universal USB Installer](https://www.pendrivelinux.com/universal-usb-installer-easy-as-1-2-3/).

I rebooted the laptop from that stick and installed Ubuntu Server, not the minimized version.
Used most of the defaults apart from the locale settings.
I connect the wifi and installed the SSH server.
This allows me to manage the system and use [SSH-FTP](https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol) to put stuff on it.


## Install RetroPie

To install RetroPie I followed the well documented steps that need to be done to [install RetroPie on Ubuntu](https://retropie.org.uk/docs/Debian).

When the script started I preformed the `Basic Install`.
This took just over an hour to complete on this old machine, because it builds all the software it needs from sources, including all the emulators.
Replacing the HDD with a SSD would definitely help, but I don't want to spend any money on this setup.
And this step doesn't have to be done too often so it isn't much of a problem.

Now I can run `emulationstation` from the command line and it starts.


## Playing the first game

To check if everything was working as expected, I had to put some images on the machine using FileZilla.
Since I had SSH installed, I could use SSH-FTP without any problem.
We still had a Logitech F310 gamepad lying around, so that serves as the main controller for now.
The first game I played was the [Tetris](https://en.wikipedia.org/wiki/Tetris) on the Game Boy.


## Futher setup tweaks

Now I wanted to improve the experience.

First I installed lightbox and [setup autologin](https://askubuntu.com/questions/51086/how-do-i-enable-auto-login-in-lightdm).

Secondly I followed some other on-line articles to stop `sudo` asking for a password.
This makes it easier to run setups and upgrade from within RetroPie.


## What's next

If I like this and use it enough, I might invest in a better pc or laptop to replace this laptop.
I'm even considering building a full size arcade cabinet, with real joysticks and buttons, to put it all in.
But first let's see how much it gets used.

This whole exercise was also done to see what it takes to have a nice experience when the hardware would be put in a cabinet.
I can buy a second hand Dell Optiplex desktop for just a bit more then a new Raspberry Pi 4 starter pack.
Since I'm considering to build a full size cabinet space is not an issue and putting a i5 (at least 4th gen, with a SSD) in seems better hardware then a Raspberry Pi.
Another advantage of an Intel based processor is that I can use DOSBOX to play old DOS games, and maybe even Wine to play some Windows games.
