---
title: Recovering Blogger comments
date: 2023-11-20 10:46:33 +01:00
lang: en
category: website
---

While reading my post about [recovering my very old blog]({% post_url 2023/2023-08-16-recovering-my-old-blog %}) again, I read the [Blogger Importer docs](https://import.jekyllrb.com/docs/blogger) again and noticed that it has an option to recover comments as well.

I'm not sure if it had that option when [I converted them originally]({% post_url 2017/2017-10-15-blog-verhuisd %}) or that I just ignored it because my Jekyll based static website and blog would not have comments anyway.
But since I found a way to display the old comments from the original (self written) blog, I decided to give it a go and see what the result of that conversion would be.

Luckily I had backuped the Blogger export better, so the XML was still there.
I cloned my repository in a separate temporary directory, installed the Ruby Gems that are needed to run the importer by putting them in `Gemfiles`.
Then I ran the command to import the blog as complete as possible.
And there were the comments, in a separate directory named `_comments`.
It was as easy as that!

It turned out the old Blogger blog had around 20 comments.
So another hour of manual labour and those comments were restored as well.
