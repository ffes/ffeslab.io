---
layout: post
title: Create the database scripts
series: Football predictions in ASP.NET and Angular
lang: en
date: 2021-03-23 23:23:15 +0100
excerpt_separator: <!--more-->
tags:
  - Football predictions
  - ASP.NET Core
  - webapi
  - Angular
  - SQLite
---

It has been a while since I spend some time on my Football Predictions Website.
Today I created the scripts to create the database.

For the previous version of this project [I decided]({% post_url 2019/2019-12-13-football-predictions-database-test %}) to start the development using a SQLite database.
I also [created the database schema]({% post_url 2019/2019-12-16-football-predictions-create-the-database %}) and filled it the fixture that where known at the time.
And then corona came and the tournament was moved to 2021.

<!--more-->

So I needed to update those scripts.
The dates of the matches needed to be updated and all the countries are known now.
Not the most difficult, but a bit boring, task that needs to be done.

Tonight I finally sat down and updated the scripts.
They are commited to the repository, both the English and the Dutch variant.

The Dutch variant I need for my own site.
The English variant I will use during development and is there for anybody to use.
If you can to translate them to another language, feel free to do so.

The next step will be to create an API endpoint that can retrieve the group data from the database.
