---
layout: post
title: NppSnippets 1.7.0 released
date: 2021-11-25 23:21:40 +01:00
lang: en
category: software
tags:
  - nppsnippets
  - notepad++
---

Today I released version 1.7.0 of [NppSnippets]({% link nppsnippets.html %}).

* Add box above the list of snippets to filter that list (issue [#32](https://github.com/ffes/nppsnippets/issues/32)).
* Add options dialog
* Modernize some snippets in the Template database (issue [#35](https://github.com/ffes/nppsnippets/issues/35)).
* Upgrade to SQLite version 3.36.0

To download version 1.7.0, go to the [download page on GitHub](https://github.com/ffes/nppsnippets/releases/tag/v1.7.0).

The [PR to update the Plugin Admin](https://github.com/notepad-plus-plus/nppPluginList/pull/385) of Notepad++ has been submitted.
When that PR is merged and a new version of Notepad++ has been released the plugin will be updated automatically.

Also know that I removed the libraries section from the [NppSnippets page]({% link nppsnippets.html %}).
I don't have a download site anymore and in the many years that I had the libraries section, I only received two contributions.
So it is not worth the effort to bring it back.
