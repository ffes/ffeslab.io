---
layout: post
title: A new start
series: Football predictions in ASP.NET and Angular
lang: en
date: 2021-02-06 21:03:38 +0100
excerpt_separator: <!--more-->
tags:
  - Football predictions
  - ASP.NET Core
  - webapi
  - Angular
---

A little over a year ago I started the development of a website for a selected group of people I personally know.
So see the full story behind this project I suggest you read at least the [introduction of my previous attempt]({% post_url 2019/2019-10-26-football-predictions-in-aspnet-and-angular %}).

Shortly after [my last post in the series]({% post_url 2020/2020-03-12-football-predictions-first-api-mockup %}) the European Championship Football was postponed due to the COVID-19 pandemic to 2021.
At this moment it looks like that it all will go on this summer.

That means that the development of the prediction site I want to write needs to be restarted.

<!--more-->

## Combined vs separate projects

The last time I touched the code I had started with a combined ASP.NET and Angular project.
At the office we recently developed a couple of new sites.
For those projects we choose to create a standard web API as backend and an Angular SPA as a frontend to talk to that API.

In general the combined project feels quite heavy, especially on my not too recent pc.

The major advantage is that I can run the separate projects in the environment I want.
So run the Angular project in a WSL2 environment editing with VS Code while running the API in full Visual Studio.
Or when I'm just working on the frontend I can simply use the Developer Command Prompt for VS 2019 to run the API as lightweight as possible.

Since development for the previous prediction site didn't really start, I decided to start from scratch.

## Creating the two projects

First I create the .NET API project and add a VS solution as well.
The solution file will come handy when for instance I want to add unit tests.

Using the "Developer Command Prompt for VS 2019" in a directory on my C drive, I entered these commands.

```bash
mkdir API
cd API
dotnet new webapi
cd ..
dotnet new sln
```

After that I opened the newly created solution in Visual Studio and used "Add existing project" to add the newly created webapi to that solution.

The very basic of the new webapi project as now done. Next to create a new Angular project.

I already have Node.js installed in my WSL2 environment.
I don't use the Windows version of Node.js.
To create the Angular site I start a docker image.
Using docker lets me create the project without the need to have all that Angular stuff install globally on my machine.
And I use the Angular CLI commands very little when not within an Angular project, so this works just fine for me.
Within the project I can use the various `npm run` commands or use `npx ng` to execute any Angular CLI command.
This means one less piece of software to maintain on my machine.

First I create a directory next to the API directory named `SPA`.

```bash
mkdir SPA
cd SPA
docker run --rm -ti -v $PWD:/app -w /app node:lts /bin/bash
```

Within that running container execute these commands.

```bash
npm install -g @angular/cli
ng new football
```

Now I have a new Angular site.

I deleted the `.git` and `node_modules` directories.
I don't need them at this moment.

## Adding EditorConfig

I am a big fan of [EditorConfig](https://editorconfig.org).
One of the first I always do when I start a new project is to create an `.editorconfig` file for the project to set my personal preferences and adjust the newly generated files to match these preferences.
Doing it now may seem a bit of a hassle, but it saves you from large git diff later.
For me, really worth the effort.

## Building for the first time

With that done, I build both projects for the first time.

First I build the API using Visual Studio.
That just works.

Now build the SPA for the first time.
Since it is run on a Windows directory, so it is not very fast (to put it mildly).
When these basics are done, I will create a copy in a WSL2 directory which makes it much faster.

```bash
npm install
npm build
```

Since these are both just newly created projects and there isn't changed anything to them, running should work but isn't that useful.
To let them know about each other is one of the first steps to be coded.

One thing I could easily take [from the previous implementation]({% post_url 2020/2020-03-12-football-predictions-first-api-mockup %}), is the proof of concept of the `groups` endpoint.
This gives me a very simple endpoint that I can use to make swagger work and implement basic frontend for.

## Putting it on GitLab

All this is commited and pushed to [GitLab](https://gitlab.com/ffes/football).
The old repository had the same name and has been renamed.
It will be deleted any time soon.
