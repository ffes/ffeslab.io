---
layout: post
title: Changing the background color of my dark theme
lang: en
date: 2021-02-04 21:28:49 +0100
category: website
tags:
  - website
  - dark theme
---

The more I looked at my own website, the more I started to dislike the background color of my dark theme.
So today I decided to change it.
But what should it become?
That was the big question.

Earlier today I read a [tweet by F1 journalist Will Buxton](https://twitter.com/wbuxtonofficial/status/1356556410872619009) about the new Red Bull cars in the 2021 season of DTM.
As always Red Bull did a terrific job in designing their liveries.
I am a big fan of the AlphaTauri F1 livery for the 2020 season.
I just love that deep dark blue color.

Seeing it again the idea was born to use that color.
Not too difficult for a developer to find what the exact color is.
I just had to go to [their site](https://www.scuderiaalphatauri.com/) and use the developer tools to see the color: `#011321`

Luckily I use SCSS and CSS variables to define the colors in my themes, so changing it was just editing that color definition and all was done.
Commit, set the tag and the website is updated.

The site looks so much better with this background.
