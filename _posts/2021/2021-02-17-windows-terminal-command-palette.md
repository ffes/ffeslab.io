---
layout: post
title: Add profiles to the Command Pallette of Windows Terminal
lang: en
date: 2021-02-17 23:23:49 +0100
tags:
  - Windows Terminal
  - Command Palette
---

I use the [Windows Terminal](https://github.com/microsoft/terminal) since it was first released and I like it very much.
Last August [version 1.3 was released](https://devblogs.microsoft.com/commandline/windows-terminal-preview-1-3-release/) and it added the command palette, similar to the command palette that can be found in VS Code.
Nice feature but the only reason I would use that is to open a new tab with a specific profile.
But that was a bit cumbersome, but thankfully you can fix this yourself.

It bugged me quite some time that it was not possible to press `Ctrl+Shift+P`, start typing a part of the profile name and press `Enter` to open a new tab with that profile.
So I searched the issues on GitHub, but couldn't find any issue about this.
When I opened [an issue on GitHub](https://github.com/microsoft/terminal/issues/9180) to suggest this, I got the answer that the current behavior was by design, but that you can change this.
All you need to do is to add some objects to the `settings.json` to do the magic for you.

Press `Ctrl+,` to open the `setting.json` and locate the `actions` section.
If you have an older version of the settings, this section might be called `keybindings`.
I suggest to rename that section to `actions` because that is the new name.

In the `actions` section, add these lines.

```json
{
	"iterateOn": "profiles",
	"icon": "${profile.icon}",
	"name": "${profile.name}: New tab",
	"command": {
		"action": "newTab",
		"profile": "${profile.name}"
	}
},
{
	"iterateOn": "profiles",
	"icon": "${profile.icon}",
	"name": "${profile.name}: Split vertically",
	"command": {
		"action": "splitPane",
		"split": "vertical",
		"profile": "${profile.name}"
	}
},
{
	"iterateOn": "profiles",
	"icon": "${profile.icon}",
	"name": "${profile.name}: Split horizontally",
	"command": {
		"action": "splitPane",
		"split": "horizontal",
		"profile": "${profile.name}"
	}
}
```

As you can see per block, it iterates all the profile and add an entry to the list of available commands.
There are three blocks, to open a a new tab and to split vertically and horizontally.
Splitting horizontally I often use to build an Angular site on WSL2 on top and a pane below that running the C# API in a VS2019 Developers CMD.

Now when you press `Ctrl+Shift+P` all the profiles are iterated into the Command Palette.
Type a few characters of the name of the profile you want to open.
Three entries per profile should show up, one to open a new tab and two to split either horizontally or vertically.

With this adjustment to the settings, the Command Palette of the Windows Terminal finally has become something I actually use.


Update March 2024:  
This adjustment needs to done by editing `settings.json` yourself.
Back in the day there was no UI for the settings and `Ctrl+,` opened the `settings.json`.
Now you need to press `Ctrl+Shift+,` to open that file in the your default JSON-editor.
