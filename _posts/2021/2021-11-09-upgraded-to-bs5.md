---
layout: post
title: Upgraded to Bootstrap 5
date: 2021-11-09 21:02:24 +0100
lang: en
category: website
tags:
  - bootstrap
---

Today I upgraded from Bootstrap 4 to Bootstrap 5.
Although that is an upgrade to the next major version, the changes I needed were minimal.
Just some classes in the `navbar` needed to be adjusted and the `data`-fields needed the namespace.
It just took half an hour to complete the entire upgrade.
Only the mobile menu isn't aligned yet to the right end of the screen.

Along the way I also converted from fontawesome to [bootstrap icons](https://icons.getbootstrap.com).
I only use icons in my blog pages for tags and categories, so there was not much reason not to do it.
I needed to wait for [v1.6](https://blog.getbootstrap.com/2021/10/13/bootstrap-icons-1-6-0/) before the scss integration was possible.
All in all it wasn't much work and worth the effort.
