---
title: Vim, not as hard as it seems
layout: default
lang: en
---


# Vim, not as hard as it seems

Editing files on Linux using Vim is not as hard as it seems.
This Vim tutorial will learn you to get comfortable with Vim.

Contents:

<ul>
{% for vim_page in site.vim %}
    <li><a href="{{ vim_page.url }}">{{ vim_page.name }}</a></li>
{% endfor %}
</ul>

If you want to know how this tutorial came about, [read my blog post]({% post_url 2022/2022-02-13-vim-tutorial %}).
This tutorial is very much work in progress, other chapters will follow but progress will not be really fast.


This work is licensed under the [Creative Commons Attribution-NonCommercial 4.0 International License](https://creativecommons.org/licenses/by-nc/4.0/).
