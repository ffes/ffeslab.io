---
layout: default
title: SelectQuotedText
description: SelectQuotedText is a plug-in for Notepad++ that selects the text in quotes
---

# SelectQuotedText plug-in for Notepad++


## Introduction

SelectQuotedText is a plug-in for [Notepad++](https://notepad-plus-plus.org/) that selects the text in quotes.


## How to install

SelectQuotedText is available in the Plugin Manager of Notepad++. That is the easiest way to install it.

To manually install the plug-in, copy `SelectQuotedText.dll` to the `plugins/SelectQuotedText` directory. Then (re)start Notepad++.


## Download

You can download version 1.1.0 [here](https://github.com/ffes/selectquotedtext/releases/tag/v1.1.0/).

The source code of SelectQuotedText can be found at [GitHub](https://github.com/ffes/selectquotedtext/).


## How to use

When the cursor is in a quoted text, press `Alt+'` to select that text.
You can adjust the hotkey in `Settings`, `Shortcut Mapper`, `Plugin commands`.
